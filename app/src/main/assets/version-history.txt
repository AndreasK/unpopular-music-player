(see fastlane changelogs for newer versions)

Version 2.80.3

- Play Store version (limited functionality)
- Some warnings localised.

Version 2.80.2

- Tile mode repaired.
- New SDK and libraries.

Version 2.80.1

- Solved crash after "show album duration" setting had been changed.

Version 2.80

- F-Droid version.
- ogg/opus support added.

Version 2.72

- F-Droid version.
- Bug removed: missing album images were shown as empty, not with default image (only Android 11).
- Handle Uri instead of path, whenever possible, and skip conversion to path, if not necessary.
- As JaudioTagger does not accepts Uris, only paths, it might be removed from the program in near future.

Version 2.71

- Various problems with Android 11 avoided.
- Play Store version unfortunately forced to be functionally limited with Android 11.
- Error message for database open failure.
- Tile reflects "SAF" mode or "DB" mode, if active.
- API 31.

Version 2.70

- Use own Java tag reader library instead of Jaudiotagger (configurable).
- Target API 30 (Android 11)

Version 2.62

- Allow opening of SAF paths also if "use own database" is configured in settings.
- Avoid unnecessary redraws of album pictures, happened mostly in SAF mode (sllooooooow).

Version 2.61

- Vorbis (.ogg or .flac files) movement tags adapted to the "MusicBrainz Picard" tagger behaviour.
- Crash in "open with..." and "share" avoided.

Version 2.60.1

- For "open with..." or "share" derive title from file name and album name from path in case of missing tags.

Version 2.60

- For smoother scrolling, cover images are created in a background thread.
- Libraries and tools updated.

Version 2.52

- No wrong bookmarks for audio files opened via "open with" or "share".
- Suppress "[00:00]" for audio files of unknown duration.
- Correctly handle files from NewPipe's downloads when passed via "share".
- Read metadata from shared files and sort them by album and track number to enable grouping.

Version 2.51

- More vertical size for element in grid view.
- Adaptive application icon (vector graphics and background).

Version 2.50.1

- Added dialogue to avoid violation of Google's "Deceptive Ads" policy.

Version 2.50

- Built for Android 10 (SDK 29)
- Icons in settings overview

Version 2.40.1

- SAF paths can be opened without explicitly activating "use own database"

Version 2.40

- SAF support (in combination with SAF Media Scanner)
- grid view is no longer experimental
- replace '\n' with semicolon in composer, performer and conductor lists

Version 2.30

- Automatic bookmark handling, stored per album.
- Playing can be resumed wherever it had been stopped before.
- Robust bookmark creation, even works when process is killed by the system.
- When continuing a track, the last five seconds played are repeated.
- Longpress to floating button stops playing and removes bookmark.

Version 2.20

- Optional album grid view (experimental)

Version 2.11.3

- Crash on process restart avoided.

Version 2.11.2

- Playing of more tracks repaired again.

Version 2.11.1

- A missing artist no longer needs to "null" in album header.
- Workaround for Android 9 bug in MediaPlayer.
- Crash in MediaController avoided.

Version 2.11

- Scrollbar added to tracks view.
- If subtitles are enabled, but not present, the wrong comma will now be suppressed.
- Trying to avoid some mysterious and seldom crashes.

Version 2.10

- Circular progress indicator (only for Android 5 and newer) for album (inner ring) and track (outer ring)
- Crash with Android 9 avoided.
- Play/Pause state of controller and floating button preserved after device rotation
- Play/Pause state of controller synchronised with floating button
- Workaround for Android bug causing album picture on lockscreen to be suppressed after track change

Version 2.01

- Short press to header makes it collapsing.
- Long press to header sends whole album to tagger.
- Coloured navigation bar on tracks view.
- Crash avoided that occurred when long pressing an empty space.
- Redundant setting removed.
- Minor localisation changes.

Version 2.00

- Collapsing toolbar with floating button for tracks view
- Try to determine the media controller height in order to prevent information from being hidden by it.

Version 1.70

- Sophisticated colour theme handling
- Three new themes, the old one is accessible via settings.
- Theme change is instantly visible, even in settings
- Round launcher icon added
- Preferences rearranged for convenience
- Notification priority especially adapted to Samsung. Why is this necessary?!?
- Prevent rapid redraws on Samsung always-on-screen
- Repaired play state sync of media controller
- Alert dialog is now light, not dark.
- Preference changes are immediately applied to album list.

Version 1.60

- "gapless" playback, as far as supported by Android
- "fast scrolling" and "section indexer" for album list. Can be deactivated.
- album sort order is case insensitive
- "various" performers is translated
- built with SDK 28

Version 1.51

- Crash with Android 8.1 avoided (with channel-less notifications)

Version 1.50.2

- New SDK 27.
- Crash avoided.

Version 1.50.1

- Crash avoided that was caused by inconsistent database.
- Help text extended

Version 1.50

- Supports the media database created by <i>Classical Music Scanner</i>.
- In case of using that database, the internal metadata tag reader is not needed.
- New menu entry: Start tagger in browser mode
- New menu entry: Start scanner
- New menu entry: Reload album list

Version 1.44.2 and 1.44.3

- Avoid crashes caused by process restart on Samsung.

Version 1.44.1

- Album header will no longer be corrupted with header size > 120. The Problem existed with v1.43 and v1.44.
- Workaround for Android bug: System does not read album artist tag.
- Workaround for Android bug: System does not read year tag from FLAC files.
- Album header in track view rearranged, with composer above title.
- Avoid crash caused by process restart.

Version 1.43
- Automatic size calculation for images in album list (from Opus 1 Music Player) in [mm] instead of [pixels].
- For albums without multi-part works and more than one track: If there is an album composer, an album year or album interpreter, the corresponding data are displayed in header and not before or after each individual track.
- Album header is rearranged to make text scrolling obsolete.
- Suppress "00:00" for not yet calculated album duration.

Version 1.42
- Long clicking a track opens the Classical Music Tagger or, if not installed, opens the Play Store.
- Stability improvements.

Version 1.41

- Missing gendered language form added: The "*" variant is obligatory for member*ess*s of the party "Die Grünen" in Germany (-> https://www.gruene.de/fileadmin/user_upload/Dokumente/BDK_2015_Halle/BDK15_Geschlechtergerechte_Sprache.pdf).
- IMPORTANT: The phrase "unknown composer“ (in notifications) is finally gendered. The German version for Bündnis90/DieGrünen is e.g. „unbekannte*r Komponist*in“.
- Help text includes technical information about audio file metadata ("tags").
- Built with SDK 26 (previously: 25).

Version 1.40

- Bluetooth car radio support extended to Android 5 (was Android 6).
- Additional Notification type (Android 6 and newer): System with Media Style. Note that due to a system bug this will be gray on Android 6, but fine on Android 7.
- Album cover as background image on lock screen (Android 5 and newer).
- Internal: compatibility layer removed from media session and notifications put to separate module.

Version 1.34

- Additional Bluetooth car radio mode: Use album display for composer and work (untested, as no test device...).
- Bug removed that had been introduced with version 1.30: Albums without cover image must be shown with generic image.
- Track numbers sent to Bluetooth car radio must start with 1, not 0. Note that due to a system bug this feature needs Android 7.
- Possible crashes avoided that could be caused by corrupt audio files.

Version 1.33

- possible crash with corrupt audio files avoided.

Version 1.32

- possible crash with corrupt audio files avoided.
- code cleanup

Version 1.31

- Bluetooth metadata style setting (currently only two styles)
- Bluetooth car radio play state and position update corrected.
- Bluetooth car radio can send "play", "pause", "next track" and "previous track" commands to device.
- Bluetooth car radio support restricted to Android 6 and newer.
- automatic dialogue with list of changes
- in "About" dialogue show "DEBUG" for a debug build

Version 1.30

- meta data passed to Bluetooth car radio: album, track title and performer
- media play "Service" rewritten for stability improvement, especially when using notifications
- autoplay modes removed (too complicated with new service)
- volume key in track view changes music volume, not alarm volume
- 1.30.1: avoid crash in Android 4.4 (->https://stackoverflow.com/questions/44408617/android-crash-on-boot-mediabuttonreceiver-may-not-be-null)
- 1.30.1: added some null pointer checks as attempt to avoid sporadic reflection crashes (->https://stackoverflow.com/questions/44073861/play-store-crash-report-illegalstateexception-on-android-view-viewdeclaredoncl/46435279#46435279)

Version 1.21

- stability improvement
- track list view should scroll more smoothly, at least Google promised that...

Version 1.20

- most parts of jaudiotagger 2.2.6 merged in order to support proprietary tags introduced with iTunes 12.6.
- iTunes behaviour (German GUI):

  "Werk und Satz verwenden: AUS":

    MP3: "Gruppierung" -> GRP1          <<== THIS IS NONSENSE, SHOULD BE TIT1
    MP3: "Titel" -> TIT2

    MP4: "Gruppierung" -> ©grp
    MP4: "Titel" -> ©nam

  "Werk und Satz verwenden: EIN":

    MP3: "Werk" -> TIT1          <<== THIS IS NONSENSE, should be new tag WORK
    MP3: "Satzname" -> MVNM
    MP3: "Satznummer"/"von" -> MVIN

    MP4: "Werk" -> ©wrk
    MP4: "Satzname" -> ©mvn
    MP4: "Satznummer" -> ©mvi
    MP4: "Anzahl Sätze" -> ©mvc
    MP4: shwm := 1

  "Werk und Satz verwenden: EIN->AUS":
    MP3: no changes in file      <<== THIS IS NONSENSE, either set flag or remove movement tags

  "Werk und Satz verwenden: EIN->AUS":
    MP4: shwm removed

- Unpopular Music Player behaviour:

  if movement name exists (either ©mvn or MVNM):
    get movement number (either ©mvi or MVIN) and place it in front of movement name (in roman numerals) and use this as title
    get work (©wrk)
    if work does not exist or it's not mp4, get "©grp" or TIT1
    if these also do not exist, get GRP1
  else
    use title as usual
    get "©grp" or TIT1
    if these do not exist, get GRP1

- if track numbers shall be displayed and album consists of more than one CD, split track numbers like "2001" to "2/1".
- if track numbers shall displayed, ignore all invalid ones (<= 0)

Version 1.11

- stability improvement in message handling, zero pointer checks added

Version 1.10

- Volume keys per default change media volume (copied from Opus 1 Music Player)
- Generic album icon changed to standard icon, makes app significantly smaller
- Custom notifications added, with expanded view
- built with newer toolchain

Version 1.04

- Japanese translation added, provided by https://gitlab.com/naofumi. Thanks!
- some null pointer tests added to make code more robust
- new method to get build time in "About" dialogue. See http://stackoverflow.com/questions/39610451/why-zipentry-shows-incorrect-compilation-time-of-apk-file
- some lint warnings suppressed
- Word "free" removed from app name because it's open source anyway.

Version 1.03

- First version to be added to F-Droid
- In case the device is rotated while playback is paused, the media controller will now correctly show the play symbol to resume playback.
- In case an album is erroneously split in multiple ones with same album title the albums are treated separately, following the Android bug.
TODO:
- Find album composer. In case he or she is not preceded the album name, precede it there and omit it in the tracks.
- Create colour chooser for texts.
- In case the whole album consists of single-title-opera: auto play album, not opus.

Version 1.02

- In order to belong to the same opus, both grouping and composer tag must match.
- Playing continues when device is rotated and media controls remain visible, if configured.

Version 1.01

- Adapted to permission mechanism of Android 6

Version 1.0

- Crash situation repaired that happened when audio file was removed or renamed while the Player is active. Now the file will behave like an empty one.
- Status bar colour of album list view and track list view no longer blue in case the gray theme was chosen
- Help Text Dialogue
- Allow screen rotation

Version 0.2

- Crash situation repaired that happened when mp3 file with album art was found

Version 0.1

- First version released
