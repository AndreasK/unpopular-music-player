package org.jaudiotagger.audio.exceptions;

/**
 * Created by and on 30.11.16.
 */
public class UnknownFilenameExtensionException extends Exception
{
    /**
    * Creates an instance.
    */
    public UnknownFilenameExtensionException()
    {
        super();
    }

    public UnknownFilenameExtensionException(Throwable ex)
    {
        super(ex);
    }

    /**
     * Creates an instance.
     *
     * @param message The message.
     */
    public UnknownFilenameExtensionException(String message)
    {
        super(message);
    }

    /**
     * Creates an instance.
     *
     * @param message The error message.
     * @param cause   The throwable causing this exception.
     */
    public UnknownFilenameExtensionException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
