/**
 *  @author : Paul Taylor
 *
 *  Version @version:$Id$
 *
 *  MusicTag Copyright (C)2003,2004
 *
 *  This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
 *  General Public  License as published by the Free Software Foundation; either version 2.1 of the License,
 *  or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License along with this library; if not,
 *  you can get a copy from http://www.opensource.org/licenses/lgpl-license.php or write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
package org.jaudiotagger.audio.aac;

import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.logging.ErrorMessage;
import org.jaudiotagger.logging.Hex;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents the audio header of an Aac File
 *
 * This is a dummy, because we only want to extract ID3 tag from some file
 */
public class AacAudioHeader implements AudioHeader
{
    private Long    audioDataStartPosition;
    private Long    audioDataEndPosition;

    private long fileSize;
    private long startByte;
    private double timePerFrame;
    private double trackLength;
    private long numberOfFrames;
    private long numberOfFramesEstimate;
    private long bitrate;
    private String encoder = "";

    //Logger
    public static Logger logger = Logger.getLogger("org.jaudiotagger.audio.aac");

    /**
     * After testing the average location of the first MP3Header bit was at 5000 bytes so this is
     * why chosen as a default.
     */

    public AacAudioHeader()
    {
    }

    /**
     * Search for the first MP3Header in the file
     *
     * The search starts from the start of the file, it is usually safer to use the alternative constructor that
     * allows you to provide the length of the tag header as a parameter so the tag can be skipped over.
     *
     * @param seekFile
     * @throws IOException
     * @throws InvalidAudioFrameException
     */
    public AacAudioHeader(final File seekFile) throws IOException, InvalidAudioFrameException
    {
        if (!seek(seekFile, 0))
        {
            throw new InvalidAudioFrameException("No audio header found within" + seekFile.getName());
        }
    }

    /**
     * Search for the first MP3Header in the file
     *
     * Starts searching from location startByte, this is because there is likely to be an ID3TagHeader
     * before the start of the audio. If this tagHeader contains unsynchronized information there is a
     * possibility that it might be inaccurately identified as the start of the Audio data. Various checks
     * are done in this code to prevent this happening but it cannot be guaranteed.
     *
     * Of course if the startByte provided overstates the length of the tag header, this could mean the
     * start of the MP3AudioHeader is missed, further checks are done within the MP3 class to recognize
     * if this has occurred and take appropriate action.
     *
     * @param seekFile
     * @param startByte
     * @throws IOException
     * @throws InvalidAudioFrameException
     */
    public AacAudioHeader(final File seekFile, long startByte) throws IOException, InvalidAudioFrameException
    {
        if (!seek(seekFile, startByte))
        {
            throw new InvalidAudioFrameException(ErrorMessage.NO_AUDIO_HEADER_FOUND.getMsg(seekFile.getName()));
        }
    }

    /**
     * Returns true if the first MP3 frame can be found for the MP3 file
     *
     * This is the first byte of  music data and not the ID3 Tag Frame.     *
     *
     * @param seekFile  MP3 file to seek
     * @param startByte if there is an ID3v2tag we dont want to start reading from the start of the tag
     * @return true if the first MP3 frame can be found
     * @throws IOException on any I/O error
     */
    public boolean seek(final File seekFile, long startByte) throws IOException
    {
        setFileSize(seekFile.length());
        setAacStartByte(startByte);
        setTimePerFrame();
        setNumberOfFrames();
        setTrackLength();
        setBitRate();
        setEncoder();
        return true;    // this is a lie, but we do not care
    }


    /**
     * Set the location of where the Audio file begins in the file
     *
     * @param startByte
     */
    protected void setAacStartByte(final long startByte)
    {
        this.startByte = startByte;
    }


    /**
     * Returns the byte position of the first MP3 Frame that the
     * <code>file</code> arguement refers to. This is the first byte of music
     * data and not the ID3 Tag Frame.
     *
     * @return the byte position of the first MP3 Frame
     */
    public long getAacStartByte()
    {
        return startByte;
    }


    /**
     * Set number of frames in this file, use Xing if exists otherwise ((File Size - Non Audio Part)/Frame Size)
     */
    protected void setNumberOfFrames()
    {
    }

    /**
     * @return The number of frames within the Audio File, calculated as accurately as possible
     */
    public long getNumberOfFrames()
    {
        return numberOfFrames;
    }

    @Override
    public Long getNoOfSamples()
    {
        return numberOfFrames;
    }
    /**
     * @return The number of frames within the Audio File, calculated by dividing the filesize by
     *         the number of frames, this may not be the most accurate method available.
     */
    public long getNumberOfFramesEstimate()
    {
        return numberOfFramesEstimate;
    }

    /**
     * Set the time each frame contributes to the audio in fractions of seconds, the higher
     * the sampling rate the shorter the audio segment provided by the frame,
     * the number of samples is fixed by the MPEG Version and Layer
     */
    protected void setTimePerFrame()
    {
    }

    /**
     * @return the the time each frame contributes to the audio in fractions of seconds
     */
    private double getTimePerFrame()
    {
        return timePerFrame;
    }

    /**
     * Estimate the length of the audio track in seconds
     * Calculation is Number of frames multiplied by the Time Per Frame using the first frame as a prototype
     * Time Per Frame is the number of samples in the frame (which is defined by the MPEGVersion/Layer combination)
     * divided by the sampling rate, i.e the higher the sampling rate the shorter the audio represented by the frame is going
     * to be.
     */
    protected void setTrackLength()
    {
        trackLength = numberOfFrames * getTimePerFrame();
    }


    /**
     * @return Track Length in seconds
     */
    public double getPreciseTrackLength()
    {
        return trackLength;
    }

    public int getTrackLength()
    {
        return 0;
    }

    /**
     * Return the length in user friendly format
     */
    public String getTrackLengthAsString()
    {
        return "0";
    }

    /**
     * @return the audio file type
     */
    public String getEncodingType()
    {
        return "";
    }

    /**
     * Set bitrate in kbps, if Vbr use Xingheader if possible
     */
    protected void setBitRate()
    {
    }

    protected void setEncoder()
    {
    }

    /**
     * @return bitrate in kbps, no indicator is provided as to whether or not it is vbr
     */
    public long getBitRateAsNumber()
    {
        return 0;
    }

    /**
     * @return the BitRate of the Audio, to distinguish cbr from vbr we add a '~'
     *         for vbr.
     */
    public String getBitRate()
    {
        return "0";
    }


    /**
     * @return the sampling rate in Hz
     */
    public int getSampleRateAsNumber()
    {
        return 0;
    }
    
    /**
     * @return the number of bits per sample
     */
    public int getBitsPerSample()
    {
        return 0;
    }

    /**
     * @return the sampling rate as string
     */
    public String getSampleRate()
    {
        return "0";
    }

    /**
     * @return MPEG Version (1-3)
     */
    public String getMpegVersion()
    {
        return "";
    }

    /**
     * @return MPEG Layer (1-3)
     */
    public String getMpegLayer()
    {
        return "";
    }

    /**
     * @return the format of the audio (i.e. MPEG-1 Layer3)
     */
    public String getFormat()
    {
        return "unknown AAC";
    }

    /**
     * @return the Channel Mode such as Stero or Mono
     */
    public String getChannels()
    {
        return "";
    }

    /**
     * @return Emphasis
     */
    public String getEmphasis()
    {
        return "";
    }

    /**
     * @return if the bitrate is variable, Xing header takes precedence if we have one
     */
    public boolean isVariableBitRate()
    {
        return false;
    }

    public boolean isProtected()
    {
        return false;
    }

    public boolean isPrivate()
    {
        return false;
    }

    public boolean isCopyrighted()
    {
        return false;
    }

    public boolean isOriginal()
    {
        return false;
    }

    public boolean isPadding()
    {
        return false;
    }

    public boolean isLossless()
    {
        return false;
    }

    /**
     */
    public String getEncoder()
    {
        return encoder;
    }

    /**
     */
    protected void setFileSize(long fileSize)
    {
        this.fileSize = fileSize;
    }


    /**
     * @return a string representation
     */
    public String toString()
    {
        return "";
    }

    /**
     */
    public Integer getByteRate()
    {
        return null;
    }

    /**
     */
    public Long getAudioDataLength()
    {
        return new Long(0);
    }

    @Override
    public Long getAudioDataStartPosition()
    {
        return audioDataStartPosition;
    }

    public void setAudioDataStartPosition(Long audioDataStartPosition)
    {
        this.audioDataStartPosition = audioDataStartPosition;
    }

    @Override
    public Long getAudioDataEndPosition()
    {
        return audioDataEndPosition;
    }

    public void setAudioDataEndPosition(Long audioDataEndPosition)
    {
        this.audioDataEndPosition = audioDataEndPosition;
    }
}
