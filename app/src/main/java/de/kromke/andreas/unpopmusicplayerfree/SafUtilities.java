package de.kromke.andreas.unpopmusicplayerfree;

import android.content.Context;
import android.content.UriPermission;
import android.net.Uri;
import androidx.documentfile.provider.DocumentFile;
import android.util.Log;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

@SuppressWarnings("SpellCheckingInspection")
class SafUtilities
{
    private final static String LOG_TAG = "SafUtilities";

    /*
    private static void prtList(List<String> theList)
    {
        int i = 0;
        for (String s : theList)
        {
            Log.d(LOG_TAG, "" + i + ": " + s);
            i++;
        }
    }
    */


    /************************************************************************************
     *
     * get DocumentFile from Uri string
     *
     * The Uri string might look like this:
     *
     *  "content:/com.android.externalstorage.documents/tree/42B5-1A05%3AMusic%2FSafTest/document/42B5-1A05%3AMusic%2FSafTest%2FHaydn%20-%20Cellokonzerte%2Ffolder.jpg"
     *
     * The permission list contains Uris like:
     *
     *  "content://com.android.providers.downloads.documents/tree/downloads"
     *  "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic"
     *  "content://com.android.externalstorage.documents/tree/primary%3AClassicalMusicDb"
     *  "content://com.android.externalstorage.documents/tree/42B5-1A05%3A"
     *
     * Scheme is "content"
     * Host is "com.android.externalstorage.documents"
     * Authority is the same as Host
     *
     ***********************************************************************************/
    static DocumentFile getDocumentFileFromUriString(Context context, final String theUriString)
    {
        List<UriPermission> thePermissionList = context.getContentResolver().getPersistedUriPermissions();
        Uri theUri = Uri.parse(theUriString);
        if (theUri == null)
        {
            Log.d(LOG_TAG, "getDocumentFileFromUriString(): cannot parse " + theUriString);
            return null;
        }
        String theAuth = theUri.getAuthority();
        if (theAuth == null)
        {
            Log.d(LOG_TAG, "getDocumentFileFromUriString(): cannot get auth " + theUriString);
            return null;
        }
        String lastPart = theUri.getLastPathSegment();
        if (lastPart == null)
        {
            Log.d(LOG_TAG, "getDocumentFileFromUriString(): cannot get lat part " + theUriString);
            return null;
        }

        //Log.d(LOG_TAG, "getDocumentFileFromUriString(): search " + theUri);
        //Log.d(LOG_TAG, "getDocumentFileFromUriString(): auth " + theUri.getAuthority());
        //Log.d(LOG_TAG, "getDocumentFileFromUriString(): host " + theUri.getHost());
        //Log.d(LOG_TAG, "getDocumentFileFromUriString(): path " + theUri.getPath());
        //prtList(theUri.getPathSegments());

        for (UriPermission perm : thePermissionList)
        {
            Uri uri = perm.getUri();
            String auth = uri.getAuthority();
            //Log.d(LOG_TAG, "getDocumentFileFromUriString(): auth = " + auth);
            if ((auth != null) && auth.equals(theAuth))
            {
                //Log.d(LOG_TAG, "getDocumentFileFromUriString(): found with matching authority: " + uri.toString());
                //Log.d(LOG_TAG, "getDocumentFileFromUriString(): host " + uri.getHost());
                //Log.d(LOG_TAG, "getDocumentFileFromUriString(): path " + uri.getPath());
                //Log.d(LOG_TAG, "getDocumentFileFromUriString(): scheme " + uri.getScheme());
                //Log.d(LOG_TAG, "getDocumentFileFromUriString(): last part " + uri.getLastPathSegment());
                //prtList(uri.getPathSegments());
                String lastPartFound = uri.getLastPathSegment();
                if ((lastPartFound != null) && lastPart.startsWith(lastPartFound))
                {
                    Log.d(LOG_TAG, "getDocumentFileFromUriString(): possible match: " + lastPartFound);
                    Log.d(LOG_TAG, "to parse: " + lastPart.substring(lastPartFound.length()));

                    String rpath = lastPart.substring(lastPartFound.length());
                    DocumentFile d = DocumentFile.fromTreeUri(context, uri);      // get a tree document file
                    if (d != null)
                    {
                        while (rpath.length() > 0)
                        {
                            if (rpath.startsWith("/"))
                            {
                                rpath = rpath.substring(1);
                            }

                            int index = rpath.indexOf('/');
                            if (index < 0)
                            {
                                index = rpath.length();
                            }

                            String name = rpath.substring(0, index);
                            String name2 = Uri.decode(name);
                            DocumentFile ndir = d.findFile(name2);
                            if (ndir == null)
                            {
                                Log.w(LOG_TAG, "getDocumentFileFromUriString(): cannot find element \"" + name2 + "\" in Uri " + d.getUri());
                                return null;
                            }
                            d = ndir;
                            rpath = rpath.substring(name.length());
                        }

                        return d;
                    }
                }
            }
        }

        Log.w(LOG_TAG, "getDocumentFileFromUriString(): could not resolve in trees, trying single Uri " + theUriString);
        return DocumentFile.fromSingleUri(context, theUri);
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    static void closeStream(Closeable s)
    {
        try
        {
            s.close();
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "I/O exception");
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    static boolean copyFileFromTo(InputStream is, OutputStream os)
    {
        boolean result = true;
        try
        {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "file not found");
            result = false;
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "I/O exception");
            result = false;
        } finally
        {
            if (is != null)
                closeStream(is);
            if (os != null)
                closeStream(os);
        }

        return result;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    static boolean isSafPath(final String path)
    {
        return path.startsWith("content://");
    }
}