/*
 * Copyright (C) 2019-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;


/** @noinspection JavadocBlankLines*/
public class TracksRecyclerAdapter extends RecyclerView.Adapter<TracksRecyclerAdapter.ViewHolder>
{
    public interface OnLongClickListener
    {
        void onLongClick(View view);
    }
    private OnLongClickListener mOnLongClickListener;

    private final String LOG_TAG = "UMPL : TracksRecAdpt";
    private final LayoutInflater mInflater;
    // settings
    private final boolean mShowTrackNumbers;
    private final int mShowConductorMode;
    private final boolean mShowSubtitle;
    private final int mBgColourNormal;
    private final int mBgColourSelected;
    private final String mStrConductor;

    // track list and layout
    private final ArrayList<AudioTrack> mAudioTracks;
    private final int mNumTracks;
    private int currTrack = -1;
    private int progressPercentage = -1;
    private int progressPercentageSecondary = -1;
    private final boolean mAddDummyTrack;
    private int mDummyTrackHeight = 120;        // value from Nexus-S, Android 4.4.4


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    TracksRecyclerAdapter
    (
        Activity activity,
        ArrayList<AudioTrack> theAudioTracks,
        boolean addDummyTrack,
        boolean showTrackNumbers,
        int showConductorMode,
        boolean showSubtitle
    )
    {
        mInflater          = activity.getLayoutInflater();
        mAudioTracks       = theAudioTracks;
        mNumTracks         = (mAudioTracks != null) ? mAudioTracks.size() : 0;
        mAddDummyTrack     = addDummyTrack;
        mShowTrackNumbers  = showTrackNumbers;
        mShowConductorMode = showConductorMode;
        mShowSubtitle = showSubtitle;
        mStrConductor      = AppGlobals.getGenderedConductor(activity);
        mBgColourNormal    = UserSettings.getThemedColourFromResId(activity, R.attr.track_list_background_normal);
        mBgColourSelected  = UserSettings.getThemedColourFromResId(activity, R.attr.track_list_background_selected);
    }


    /**************************************************************************
     *
     * create a "view holder" which contains our layout and custom meta information
     *
     * Note that this call does not yet handle a special row, but only handles
     * information that is common to all rows.
     *
     *************************************************************************/
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        //Log.d(LOG_TAG, "onCreateViewHolder(typ = " + viewType +")");
        View view = mInflater.inflate(R.layout.audio_track, viewGroup, false);
        return new ViewHolder(view, viewType);
    }


    /**************************************************************************
     *
     * bind a "view holder" with data for a given row
     *
     * This takes a common object and individualises it for a given row.
     *
     *************************************************************************/
    @Override
    public void onBindViewHolder(@NonNull TracksRecyclerAdapter.ViewHolder viewHolder, int position)
    {
        //Log.d(LOG_TAG, "onBindViewHolder(pos = " + position +")");
        viewHolder.setData(position);
    }


    /**************************************************************************
     *
     * tell the number of rows
     *
     *************************************************************************/
    @Override
    public int getItemCount()
    {
        //Log.d(LOG_TAG, "getItemCount() => " + ((mAudioAlbums != null) ? mAudioAlbums.size() : 0));
        return (mAddDummyTrack) ? mNumTracks + 1 : mNumTracks;
    }


    /**************************************************************************
     *
     * if the list has a header or footer with different view type, this
     * function must return different values
     *
     *************************************************************************/
    @Override
    public int getItemViewType(int position)
    {
        int size = (mAudioTracks == null) ? 0 : mAudioTracks.size();
        if (position >= size)
        {
            return 1;   // dummy track
        }
        else
        {
            return 0;   // track
        }
    }


    /**************************************************************************
     *
     * object representing a given row
     *
     *************************************************************************/
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener
    {
        boolean mIsDummyView;
        TextView composerAndWorkView;
        TextView titleView;
        TextView artistView;
        ProgressBar progressView;

        //
        // constructor, common data for each row, not individual, yet
        //
        ViewHolder(View audioTrackLay, int viewType)
        {
            super(audioTrackLay);       // this sets the basic class attribute itemView := audioTrackLay
            composerAndWorkView = audioTrackLay.findViewById(R.id.track_composer_and_work);
            titleView = audioTrackLay.findViewById(R.id.track_title);
            artistView = audioTrackLay.findViewById(R.id.track_interpreter);
            progressView = audioTrackLay.findViewById(R.id.track_progress);
            mIsDummyView = (viewType == 1);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view)
        {
            Log.d(LOG_TAG, "onLongClick()");
            if (mOnLongClickListener != null)
            {
                mOnLongClickListener.onLongClick(view);
            }
            return true;
        }

        //
        // helper
        //

        private String durationStr(final AudioTrack currAudioTrack)
        {
            if (currAudioTrack.duration >= 0)
            {
                return " [" + AppGlobals.convertMsToHMmSs(currAudioTrack.duration) + "]";
            }
            else
            {
                return "";
            }
        }

        //
        // individualise the object for a given row
        //
        void setData(int position)
        {
            // This happened once, reported by Google Developer Crash reporter,
            // called from getViewByPosition(), with unknown reason
            if ((!mIsDummyView) && ((position < 0) || (position >= mNumTracks)))
            {
                Log.e(LOG_TAG, "getView(position = " + position + ") : out of bounds: 0.." + (mNumTracks - 1));
                return;
            }

            // apply colour theme

            if (mIsDummyView)
            {
                // the dummy entry
                titleView.setHeight(mDummyTrackHeight);
                titleView.setEnabled(false);        // shall make it non-clickable
                //composerAndWorkView.setHeight(0);
                //artistView.setHeight(0);
                composerAndWorkView.setVisibility(View.GONE);
                artistView.setVisibility(View.GONE);
                progressView.setVisibility(View.GONE);
                itemView.setTag(-1);
                return;
            }

            //get track using position
            AudioTrack currAudioTrack = mAudioTracks.get(position);

            // get information from track
            String trackComposer    = currAudioTrack.composer;
            String trackGroup       = currAudioTrack.grouping;
            String trackTitle       = currAudioTrack.title;
            String trackInterpreter = currAudioTrack.performer;
            long   trackYear        = currAudioTrack.year;

            if (!currAudioTrack.album.bHasGroups)
            {
                if (currAudioTrack.album.hasCommonComposer())
                {
                    // suppress composer in case there is a common "album composer"
                    trackComposer = "";
                }
                if (currAudioTrack.album.hasCommonPerformer())
                {
                    // suppress performer in case there is a common "album performer"
                    trackInterpreter = "";
                }
                if (currAudioTrack.album.common_year > 0)
                {
                    // suppress year in case there is a common "album year"
                    trackYear = 0;
                }
            }

            // precede track number, if set in preferences, split into CD/track, if >= 1000
            if (mShowTrackNumbers)
            {
                if (currAudioTrack.trackno > 0)
                {
                    if (currAudioTrack.discno > 0)
                    {
                        trackTitle = "[" + currAudioTrack.discno + "/" + currAudioTrack.trackno + "] " + trackTitle;
                    } else
                    {
                        trackTitle = "[" + currAudioTrack.trackno + "] " + trackTitle;
                    }
                }
            }

            // add subtitle, if set in preferences
            if (mShowSubtitle && !currAudioTrack.subtitle.isEmpty())
            {
                trackTitle += ", " + currAudioTrack.subtitle;
            }

            // add conductor, if set in preferences
            if ((mShowConductorMode != 0) && !currAudioTrack.conductor.isEmpty())
            {
                if (!trackInterpreter.isEmpty())
                {
                    if (mShowConductorMode == 2)
                    {
                        trackInterpreter += "\n";
                    } else
                    {
                        trackInterpreter += ", ";
                    }
                }

                trackInterpreter += mStrConductor + ": " + currAudioTrack.conductor;
            }

            // if there is no grouping, use title instead
            if (trackGroup.isEmpty())
            {
                trackGroup = trackTitle;
                trackTitle = "";
            }

            // if it's not the first one of the group, omit composer, work and performer
            if (currAudioTrack.group_no != 0)
            {
                trackComposer = "";
                trackGroup = "";
            }

            if (!currAudioTrack.group_last)
            {
                // for a group, add the performer only to the last element
                trackInterpreter = "";
                trackYear = 0;
            }

            // composer and work resp. title
            if (!trackGroup.equals(""))
            {
                String textShown;
                if (trackComposer.equals(""))
                {
                    textShown = trackGroup;
                }
                else
                {
                    textShown = trackComposer + ":\n" + trackGroup;
                }
                // work without movements/pieces: add duration here
                if (trackTitle.isEmpty())
                {
                    textShown = textShown + durationStr(currAudioTrack);
                }
                //composerAndWorkView.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                composerAndWorkView.setText(textShown);
                composerAndWorkView.setVisibility(View.VISIBLE);
            }
            else
            {
                // neither composer nor work
                //composerAndWorkView.setHeight(0);
                composerAndWorkView.setVisibility(View.GONE);
            }

            // title and duration
            if (!trackTitle.isEmpty())
            {
                String titleShown = trackTitle + durationStr(currAudioTrack);
                //titleView.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                titleView.setText(titleShown);
                titleView.setVisibility(View.VISIBLE);

            }
            else
            {
                // work without movements/pieces
                //titleView.setHeight(0);
                titleView.setVisibility(View.GONE);
            }

            // performer and year
            if (!trackInterpreter.isEmpty() || (trackYear != 0))
            {
                String textShown = trackInterpreter;
                if (!trackInterpreter.isEmpty() && (trackYear != 0))
                {
                    textShown = textShown + " ";
                }
                if (trackYear != 0)
                {
                    textShown = textShown + "(" + trackYear + ")";
                }
                //artistView.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                artistView.setText(textShown);
                artistView.setVisibility(View.VISIBLE);
            }
            else
            {
                // not first movement of the work, or there is no performer
                //artistView.setHeight(0);
                artistView.setVisibility(View.GONE);
            }

            if (position == currTrack)
            {
                //Log.d("TracksAdapter", "getView(position = " + position + ") -- selection colour");
                itemView.setBackgroundColor(mBgColourSelected);
                progressView.setProgress(progressPercentage);
                progressView.setSecondaryProgress(progressPercentageSecondary);
                progressView.setVisibility(View.VISIBLE);
            }
            else
            {
                itemView.setBackgroundColor(mBgColourNormal);
                progressView.setVisibility(View.GONE);
            }

            //set position as tag
            itemView.setTag(position);     //TODO: this is unnecessary, as no longer used
        }
    }


    /**************************************************************************
     *
     * additional function
     *
     *************************************************************************/
    void setOnLongClickListener(OnLongClickListener listener)
    {
        mOnLongClickListener = listener;
    }


    /**************************************************************************
     *
     * additional function
     *
     *************************************************************************/
    void setCurrTrack(int curr)
    {
        Log.d(LOG_TAG, "setCurrTrack(" + curr + ")");
        currTrack = curr;
        progressPercentage = -1;
    }


    /**************************************************************************
     *
     * additional function
     *
     *************************************************************************/
    boolean setProgressPercentage(int percentage, int percentageSecondary)
    {
        if ((percentage != progressPercentage) || (percentageSecondary != progressPercentageSecondary))
        {
            progressPercentage = percentage;
            progressPercentageSecondary = percentageSecondary;
            return true;    // change
        }
        else
        {
            return false;   // no change
        }
    }


    /**************************************************************************
     *
     * additional function
     *
     *************************************************************************/
    void setDummyTrackHeight(int height)
    {
        mDummyTrackHeight = height;
    }
}