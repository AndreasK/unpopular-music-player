/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Size;

import java.io.IOException;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.documentfile.provider.DocumentFile;

/** @noinspection JavadocBlankLines*/ // ImageView for deferred image loading in background
public class ImageViewDeferred
        extends androidx.appcompat.widget.AppCompatImageView
{
    private final static String LOG_TAG = "UMPL : ImgViewDeferred";

    //
    // class attributes
    //

    private static final boolean sbDeferred = true;     // debug helper

    private static final int MSG_CREATE_COVER = 61;     // UI thread -> worker thread
    private static final int MSG_DRAW_COVER   = 62;     // worker thread -> UI thread

    private static final WorkerHandlerCallback theWorkerHandlerCallback = new WorkerHandlerCallback();
    private static final UiHandlerCallback theUiHandlerCallback = new UiHandlerCallback();
    private static Handler sUiHandler;                  // UI Thread handler
    private static Handler sWorkerThreadHandler;        // worker thread handler
    private static int sImageSize = 100;

    //
    // object attributes
    //

    private final Context mContext;         // context of constructor
    private int mPosition;                  // corresponding database key

    private static class CoverMsg
    {
        public Context context;
        public int key;                 // LRU key
        public String path;             // picture path or Uri
        public ImageViewDeferred view;  // associated view
        CoverMsg(Context context, int position, String path, ImageViewDeferred view)
        {
            this.context = context;
            this.key = position;
            this.view = view;
            this.path = path;
        }
    }

    // callback for messages from UI thread to worker thread
    static private class WorkerHandlerCallback implements Handler.Callback
    {
        @Override
        public boolean handleMessage(Message message)
        {
            if (message.what != MSG_CREATE_COVER)
            {
                return false;
            }

            CoverMsg payload = (CoverMsg) message.obj;

            // check if the message is outdated, i.e. the view has been reused in the meantime
            ImageViewDeferred theView = payload.view;
            if (theView.mPosition != payload.key)
            {
                Log.d(LOG_TAG, "handleMessage() : outdated message to BG discarded");
                return true;
            }

            Bitmap bitmap = null;
            if (BitmapUtils.LruCacheContainsKey(payload.key))
            {
                // note that bitmap maybe null for invalid album art files
                bitmap = BitmapUtils.LruCacheGet(payload.key);
                //Log.d(LOG_TAG, "handleMessage() : got bitmap from cache");
            }
            if (bitmap == null)
            {
                bitmap = createBitmap(payload.context, payload.path, sImageSize);
            }
            if (bitmap != null)
            {
                BitmapUtils.LruCachePut(payload.key, bitmap);
                // send message to draw image in UI thread, just pass the same payload
                sUiHandler.sendMessage(sUiHandler.obtainMessage(MSG_DRAW_COVER, payload));
            }
            return true;
        }
    }

    // callback for messages from worker thread to UI thread
    static private class UiHandlerCallback implements Handler.Callback
    {
        @Override
        public boolean handleMessage(Message message)
        {
            if (message.what != MSG_DRAW_COVER)
            {
                return false;
            }

            CoverMsg payload = (CoverMsg) message.obj;

            // check if the message is outdated, i.e. the view has been reused in the meantime
            ImageViewDeferred theView = payload.view;
            if (theView.mPosition != payload.key)
            {
                Log.d(LOG_TAG, "handleMessage() : outdated message to UI discarded");
                return true;
            }

            //Log.d(LOG_TAG, "handleMessage(MSG_DRAW_COVER) -- for position " + payload.key);
            Bitmap bitmap = BitmapUtils.LruCacheGet(payload.key);
            if (bitmap != null)
            {
                theView.setImageBitmap(bitmap);
            }
            return true;
        }
    }


    /************************************************************************************
     *
     * constructor
     *
     ***********************************************************************************/
    public ImageViewDeferred(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        mContext = context;
        mPosition = -1;         // no image key associated, yet

        //Log.d(LOG_TAG, "ImageViewDeferred() : object created");

        // This call is supposed to register a callback run in the UI thread.
        if (sUiHandler == null)
        {
            sUiHandler = new Handler(theUiHandlerCallback);
        }

        // Note that the worker thread is static, i.e. exists only once for the class,
        // and so is the callback.
        if (sWorkerThreadHandler == null)
        {
            HandlerThread thread = new HandlerThread("DeferredCoverImageThread");
            thread.start();
            sWorkerThreadHandler = new Handler(thread.getLooper(), theWorkerHandlerCallback);
        }
    }


    /************************************************************************************
     *
     * pass picture file path (called in UI thread)
     *
     * Note that the Views are reused. Thus we have to remember the current (!)
     * corresponding database key, so that the message handlers can detect
     * outdated messages and discard them.
     *
     ***********************************************************************************/
    public void setPicturePath
    (
        int position,
        final String path,
        final int imageSize,
        final Drawable defaultBitmap
    )
    {
        sImageSize = imageSize;     // may be updated here

        // detect reuse
        /*
        //Log.w(LOG_TAG, "setPicturePath(" + position + ", \"" + path + "\")");
        if ((mPosition >= 0) && (mPosition != position))
        {
            Log.w(LOG_TAG, "setPicturePath() : object reused, position was " + mPosition + ", now " + position);
        }
        */
        mPosition = position;

        Bitmap bitmap = null;
        if (!path.isEmpty())
        {
            //Log.d(LOG_TAG, "getView() : art path \"" + theAlbum.album_picture_path + "\"");
            // cache mechanism
            if (BitmapUtils.LruCacheContainsKey(position))
            {
                // note that bitmap maybe null for invalid album art files
                bitmap = BitmapUtils.LruCacheGet(position);
                //Log.d(LOG_TAG, "setPicturePath() : got bitmap from cache");
            }
            else
            if (sbDeferred)
            {
                // create image in background thread
                //Log.d(LOG_TAG, "setPicturePath() : send request for position " + position + " to worker thread");
                CoverMsg payload = new CoverMsg(mContext, position, path, this);
                sWorkerThreadHandler.sendMessage(sWorkerThreadHandler.obtainMessage(MSG_CREATE_COVER, payload));
            }
            else
            {
                bitmap = createBitmap(mContext, path, imageSize);
                if (bitmap != null)
                {
                    BitmapUtils.LruCachePut(position, bitmap);
                }
            }
        }

        if (bitmap != null)
        {
            setImageBitmap(bitmap);
        }
        else
        {
            setImageDrawable(defaultBitmap);
            // causes out of memory error:
            //  albumCoverImageView.setImageResource(R.drawable.no_album_art);
        }
    }


    /************************************************************************************
     *
     * create bitmap from media Uri (API 29 or newer)
     *
     ***********************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.Q)
    static Bitmap createBitmapFromMediaContentUri
    (
        Context context,
        final String path,
        final int imageSize
    )
    {
        Bitmap bitmap = null;

        // Android 11 media path from:
        //          ContentUris.withAppendedId(
        //                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
        //                        thisAlbumId);
        ContentResolver resolver = context.getContentResolver();
        Uri contentUri = Uri.parse(path);
        Size wh = new Size(imageSize, imageSize);
        try
        {
            bitmap = resolver.loadThumbnail(contentUri, wh, null);
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "createBitmapFromMediaContentUri() : invalid uri \"" + path + "\": " + e);
        }

        return bitmap;
    }


    /************************************************************************************
     *
     * create bitmap from SAF Uri
     *
     * @noinspection RedundantSuppression
     ***********************************************************************************/
    @SuppressWarnings("deprecation")
    private static Bitmap createBitmapFromSafPath
    (
        Context context,
        final String path
    )
    {
        Bitmap bitmap = null;

        DocumentFile d = SafUtilities.getDocumentFileFromUriString(context, path);
        if (d != null)
        {
            try
            {
                ContentResolver resolver = context.getContentResolver();
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P)
                {
                    ImageDecoder.Source source = ImageDecoder.createSource(resolver, d.getUri());
                    bitmap = ImageDecoder.decodeBitmap(source);
                }
                else
                {
                    // deprecated with Android 9
                    bitmap = MediaStore.Images.Media.getBitmap(resolver, d.getUri());
                }
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "createBitmap() : invalid art path \"" + path + "\": " + e);
            } catch (SecurityException e)
            {
                Log.e(LOG_TAG, "createBitmap() : not allowed to access art path \"" + path + "\": " + e);
                Log.e(LOG_TAG, "last path segment is " + d.getUri().getLastPathSegment());
            } catch (Exception e)
            {
                Log.e(LOG_TAG, "createBitmap() : exception on path \"" + path + "\": " + e);
            }
        }

        return bitmap;
    }


    /************************************************************************************
     *
     * create bitmap from path
     *
     ***********************************************************************************/
    private static Bitmap createBitmap
    (
        Context context,
        final String path,
        final int imageSize
    )
    {
        Bitmap bitmap = null;
        if (path.startsWith("content://media/"))
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            {
                bitmap = createBitmapFromMediaContentUri(context, path, imageSize);
            }
        }
        else
        if (path.startsWith("content://"))
        {
            bitmap = createBitmapFromSafPath(context, path);
        } else
        {
            // traditional path
            bitmap = BitmapUtils.decodeSampledBitmapFromFile(path, imageSize, imageSize);
        }

        return bitmap;
    }

}
