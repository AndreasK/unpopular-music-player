/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * fills the album list with data
 * @noinspection JavadocBlankLines, RedundantSuppression
 */
class AudioAlbumAdapter extends BaseAdapter implements SectionIndexer
{
    //private final static String LOG_TAG = "UMPL : AudioAlbumAdapter";
    private final Context mContext;
    private final ArrayList<AudioAlbum> mAudioAlbums;
    private final LayoutInflater mTheInflater;
    // section handling for fast scroll
    private String[] mSections;
    private int[] mSectionForPosition;
    private int[] mPositionForSection;
    //private ArrayList<Drawable> theCoverBitmaps;
    private final Drawable mDefaultBitmap;
    private int mCurrAlbum = -1;
    private final int mAlbumImageSize;
    private final boolean mShowAlbumDuration;
    private final boolean mUseGridView;


    /**************************************************************************
     *
     * Section Indexer function
     *
     *************************************************************************/
    @Override
    public Object[] getSections()
    {
        return mSections;
    }


    /**************************************************************************
     *
     * Section Indexer function
     *
     *************************************************************************/
    @Override
    public int getPositionForSection(int sectionIndex)
    {
        return mPositionForSection[sectionIndex];
    }


    /**************************************************************************
     *
     * Section Indexer function
     *
     *************************************************************************/
    @Override
    public int getSectionForPosition(int position)
    {
        return mSectionForPosition[position];
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private boolean createSectionsInternal()
    {
        int size = (mAudioAlbums == null) ? 0 : mAudioAlbums.size();
        if (size <= 0)
        {
            return false;
        }

        mSectionForPosition = new int[size];
        int[] tempPositionForSection = new int[size];   // to be truncated later
        String[] tempSections = new String[size];   // to be truncated later

        //boolean bIsSorted = true;

        int num_sections = 0;
        char cp = ' ';
        int i_pos = 0;

        for (AudioAlbum theObject: mAudioAlbums)
        {
            final String theSectionName = theObject.getSection();
            char c0;

            if ((theSectionName != null) && !theSectionName.isEmpty())
            {
                c0 = Character.toUpperCase(theSectionName.charAt(0));
                if (c0 < '0')
                {
                    c0 = ' ';   // treat all control characters like space
                }
                else
                if (Character.isDigit(c0))
                {
                    c0 = '0';   // treat all digits as same section
                }
            }
            else
            {
                c0 = cp;
            }

            if ((i_pos == 0) || (c0 != cp))
            {
                tempPositionForSection[num_sections] = i_pos;
                switch(c0)
                {
                    case ' ': tempSections[num_sections] = "#"; break;
                    case '0': tempSections[num_sections] = "0-9"; break;
                    default:  tempSections[num_sections] = Character.toString(c0); break;
                }
                num_sections++;
            }

            //Log.d(LOG_TAG, "createSections() : album \"" + theSectionName + "\" is section " + (num_sections - 1));
            mSectionForPosition[i_pos++] = num_sections - 1;
            cp = c0;
        }

        // truncate arrays

        mPositionForSection = new int[num_sections];
        mSections = new String[num_sections];
        // truncate array by copying
        System.arraycopy(tempPositionForSection, 0, mPositionForSection, 0, num_sections);
        System.arraycopy(tempSections, 0, mSections, 0, num_sections);

        return true;
    }


    //constructor
    // BUG in Android, see: http://stackoverflow.com/questions/13685275/outofmemory-error-in-custom-listview-adapter-mono-android
    AudioAlbumAdapter
    (
        Context c,
        ArrayList<AudioAlbum> theAudioAlbums,
        int albumImageSize,
        boolean showAlbumDuration,
        boolean useSectionIndexer,
        boolean useGridView
    )
    {
        mContext = c;
        mAudioAlbums = theAudioAlbums;
        mTheInflater = LayoutInflater.from(c);
        mAlbumImageSize = albumImageSize;
        mShowAlbumDuration = showAlbumDuration;
        mUseGridView = useGridView;

        /*
        // due to Android bug create all bitmaps here
        theCoverBitmaps = new ArrayList<Drawable>();
        for (int i = 0; i < theAudioAlbums.size(); i++)
        {
            AudioAlbum theAlbum = theAudioAlbums.get(i);
            Drawable d;
            if (!theAlbum.album_picture_path.isEmpty())
            {
                d = Drawable.createFromPath(theAlbum.album_picture_path);
            }
            else
            {
                d = null;
            }
            theCoverBitmaps.add(d);
        }
        */
        // default bitmap
        //        mDefaultBitmap = c.getResources().getDrawable(R.drawable.no_album_art);  // deprecated
        mDefaultBitmap = ContextCompat.getDrawable(c, R.drawable.no_album_art);

        // create bitmap cache
        BitmapUtils.LruCacheCreate(20);
        createSections(useSectionIndexer);
    }

    // can be called from outside to add indexer later
    void createSections(boolean bUseSectionIndexer)
    {
        if ((!bUseSectionIndexer) || (!createSectionsInternal()))
        {
            // no sections
            mSectionForPosition = new int[0];
            mPositionForSection = new int[0];
            mSections = new String[0];
        }
        // this is not necessary in constructor, but needed if section indexer is changed or created later
        notifyDataSetChanged();     // omitting this will crash the SectionIndexer!!!
    }

    void setCurrAlbum(int curr)
    {
        mCurrAlbum = curr;
    }

    @Override
    public int getCount()
    {
        return (mAudioAlbums != null) ? mAudioAlbums.size() : 0;
    }

    @Override
    public Object getItem(int arg0)
    {
        return null;
    }

    @Override
    public long getItemId(int arg0)
    {
        return 0;
    }


    /**************************************************************************
     *
     * convertView can be reused, if not null and has correct type, see Adapter.java
     *
     *************************************************************************/
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        RelativeLayout audioAlbumLay;

        if (convertView != null)
        {
            // just recycle the View. Can be sure it's the correct type
            //Log.d(LOG_TAG, "getView(" + position + ") : recycle");
            audioAlbumLay = (RelativeLayout) convertView;
        }
        else
        {
            // inflate a new hierarchy from the xml layout, containing all text and image objects
            //Log.d(LOG_TAG, "getView(" + position + ") : create");
            int res = (mUseGridView) ? R.layout.audio_album_grid : R.layout.audio_album;
            audioAlbumLay = (RelativeLayout) mTheInflater.inflate(res, parent, false);
        }

        if (audioAlbumLay != null)
        {
            //get title and artist sub-views from the just created hierarchy
            TextView albumView = audioAlbumLay.findViewById(R.id.album_title);
            TextView albumInfoView = audioAlbumLay.findViewById(R.id.album_info);
            ImageViewDeferred albumCoverImageView = audioAlbumLay.findViewById(R.id.album_cover_image);
            //noinspection StatementWithEmptyBody
            if (mUseGridView)
            {
                //ViewGroup.LayoutParams params;
                //params = albumCoverImageView.getLayoutParams();
                //mAlbumImageSize = params.width;
                //mAlbumImageSize = 512;
            }
            else
            {
                // set cover image view size from Preferences
                ViewGroup.LayoutParams params;
                params = albumCoverImageView.getLayoutParams();
                params.width = mAlbumImageSize;
                params.height = mAlbumImageSize;
                albumCoverImageView.setLayoutParams(params);
            }

            //get album using position
            AudioAlbum theAlbum = mAudioAlbums.get(position);

            //show album title in large, bold text
            albumView.setText(theAlbum.album_name);

            // show info below, in smaller text
            String infoText = "";
            if (theAlbum.hasCommonComposer())
            {
                infoText = theAlbum.getComposer() + "\n";
            }
            final String artist = theAlbum.getArtist();
            if (artist != null)
            {
                infoText += artist + "\n";
            }
            String trackText = (theAlbum.album_no_of_tracks == 1) ? mContext.getString(R.string.str_track) : mContext.getString(R.string.str_tracks);
            infoText += "(" + theAlbum.album_no_of_tracks + " " + trackText;
            if ((mShowAlbumDuration) && (theAlbum.album_duration != 0))
            {
                infoText += ", " + AppGlobals.convertMsToHMmSs(theAlbum.album_duration);
            }
            infoText += ")";
            albumInfoView.setText(infoText);

            // pass information how to retrieve album picture
            albumCoverImageView.setPicturePath(position, theAlbum.album_picture_path, mAlbumImageSize, mDefaultBitmap);

            if (position == mCurrAlbum)
            {
                audioAlbumLay.setBackgroundColor(UserSettings.getThemedColourFromResId(mContext, R.attr.album_list_background_selected));
            }
            else
            {
                audioAlbumLay.setBackgroundColor(UserSettings.getThemedColourFromResId(mContext, R.attr.album_list_background_normal));
            }
            //set position as tag
            audioAlbumLay.setTag(position);
        }
        return audioAlbumLay;
    }

}
