/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.documentfile.provider.DocumentFile;

import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * global variables, shared by all activities
 * @noinspection JavadocBlankLines
 */
@SuppressWarnings("SpellCheckingInspection")
class AppGlobals
{
    private static final String LOG_TAG = "UMPL : AppGlobals";

    // public lists of all albums and current album
    static ArrayList<AudioAlbum> sAudioAlbumList;         // all albums
    static AudioAlbum sCurrAlbum = null;
    static long sCurrAlbumId = -1;
    static ArrayList<AudioTrack> sCurrAlbumTrackList = null;    // all tracks for currAlbum
    static Drawable sCurrAlbumImageDrawable;
    // hack to know the size of an album header picture
    static boolean sbAlbumHeaderPictureSizeKnown = false;
    static int sAlbumHeaderPictureSize = 512;       // default value

    // media database
    static private DatabaseManager sDatabaseManager = null;
    static private DatabaseManagerAndroid sDatabaseManagerAndroid = null;
    public static boolean sbAndroidDbIsActive = true;   // default
    public static boolean sbOwnDatabaseIsSaf = false;

    // various
    static int sGenderismMode = 0;


    /**************************************************************************
     *
     * initialisation
     *
     * reads album list and opens album, if album id is not -1
     *
     *************************************************************************/
    static int initAppGlobals
    (
        Context context,
        long currAlbumId,
        final String ownDatabasePath,
        boolean bIsSafPath,
        final boolean bUseJaudioTaglib
    )
    {
        boolean bUseOwnDatabase = (ownDatabasePath != null);

        if ((bUseOwnDatabase) && (sDatabaseManager == null))
        {
            sDatabaseManager = new DatabaseManager();
            Log.d(LOG_TAG, "initAppGlobals() : open own database: " + ownDatabasePath);
            int rc = sDatabaseManager.open(ownDatabasePath);
            /* TEST CODE
            if (rc != 0)
            {
                Log.w(LOG_TAG, "initAppGlobals() : Android-11-hack-open own database: " + ownDatabasePath + ".jpg");
                rc = mDatabaseManager.open(ownDatabasePath + ".jpg");
            }
             */

            if (rc == 0)
            {
                sbAndroidDbIsActive = false;
                sbOwnDatabaseIsSaf = bIsSafPath;
            }
            else
            {
                Log.e(LOG_TAG, "initAppGlobals() : cannot open own database, falling back to Android DB.");
                bUseOwnDatabase = false;
                sbAndroidDbIsActive = true;
                sbOwnDatabaseIsSaf = false;
                sDatabaseManager = null;
            }
        }

        if ((!bUseOwnDatabase) && (sDatabaseManagerAndroid == null))
        {
            sDatabaseManagerAndroid = new DatabaseManagerAndroid(bUseJaudioTaglib);
            sDatabaseManagerAndroid.open(context.getContentResolver());
        }

        int currAlbumNo = -1;

        if (sbAndroidDbIsActive != AudioTrack.sbUsingAndroidDb)
        {
            Log.d(LOG_TAG, "initAppGlobals() : database mode changed, invalidate album list");
            sAudioAlbumList = null;
        }

        if (sAudioAlbumList == null)
        {
            if (sDatabaseManager != null)
            {
                sAudioAlbumList = sDatabaseManager.queryAlbums();
                AudioTrack.sbUsingAndroidDb = false;
            }
            else
            {
                sAudioAlbumList = sDatabaseManagerAndroid.queryAlbums();
                AudioTrack.sbUsingAndroidDb = true;
            }
        }

        if (currAlbumId != -1)
        {
            currAlbumNo = findAudioAlbum(currAlbumId);
            if (currAlbumNo >= 0)
            {
                Log.d(LOG_TAG, "initAppGlobals() : restore album index " + currAlbumNo);
                AudioAlbum currAlbum = sAudioAlbumList.get(currAlbumNo);
                if (currAlbum != null)
                {
                    openAudioAlbum(context, currAlbum);
                }
            }
        }

        return currAlbumNo;
    }


    /**************************************************************************
     *
     * helper function avoiding null pointer violation
     *
     *************************************************************************/
    static int getAudioAlbumTrackListSize()
    {
        if (sCurrAlbumTrackList == null)
            return 0;
        else
            return sCurrAlbumTrackList.size();
    }


    /**************************************************************************
     *
     * find albumId in list and return index
     *
     *************************************************************************/
    static private int findAudioAlbum(long albumId)
    {
        if (sAudioAlbumList != null)
        {
            int i;
            int size = sAudioAlbumList.size();
            for (i = 0; i < size; i++)
            {
                if (sAudioAlbumList.get(i).album_id == albumId)
                {
                    return i;
                }
            }
        }

        return -1;
    }


    /**************************************************************************
     *
     * init grouping and album image
     *
     *************************************************************************/
    private static void initAudioAlbum(Context context)
    {
        AudioTrack prevTrack = null;
        int noInGroup = 0;
        long albumDuration = 0;
        int size = getAudioAlbumTrackListSize();
        for (int i = 0; i < size; i++)
        {
            AudioTrack theTrack = sCurrAlbumTrackList.get(i);

            if (theTrack.album == null)
            {
                theTrack.album = sCurrAlbum;     // for pseudo album
            }

            if (theTrack.isSameGroup(prevTrack))
            {
                sCurrAlbum.bHasGroups = true;
            }
            else
            {
                // this is the first group or a new one in the sorted list or there is no group
                noInGroup = 0;
                if (prevTrack != null)
                {
                    prevTrack.group_last = true;
                }
            }

            theTrack.group_no = noInGroup++;
            theTrack.group_last = false;		// may be changed later
            albumDuration += theTrack.duration;
            prevTrack = theTrack;
        }

        if (prevTrack != null)
        {
            prevTrack.group_last = true;
        }

        sCurrAlbum.album_duration = albumDuration;
        sCurrAlbumImageDrawable = null;  // default: no picture for this album

        if ((sCurrAlbum.album_picture_path != null) && !sCurrAlbum.album_picture_path.isEmpty())
        {
            if (sCurrAlbum.album_picture_path.startsWith("content://media/"))
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                {
                    Bitmap bitmap = ImageViewDeferred.createBitmapFromMediaContentUri(context, sCurrAlbum.album_picture_path, sAlbumHeaderPictureSize);
                    if (bitmap != null)
                    {
                        sCurrAlbumImageDrawable = new BitmapDrawable(context.getResources(), bitmap);
                    }
                }
            }
            else
            if (SafUtilities.isSafPath(sCurrAlbum.album_picture_path))
            {
                DocumentFile df = SafUtilities.getDocumentFileFromUriString(context, sCurrAlbum.album_picture_path);
                if (df != null)
                {
                    InputStream is;
                    try
                    {
                        is = context.getContentResolver().openInputStream(df.getUri());
                        sCurrAlbumImageDrawable = Drawable.createFromStream(is, df.getName());
                    } catch (FileNotFoundException e)
                    {
                        Log.e(LOG_TAG, "image file not found: " + e);
                    } catch (Exception e)
                    {
                        Log.e(LOG_TAG, "image file access denied: " + e);
                    }
                }
            }
            else
            {
                sCurrAlbumImageDrawable = Drawable.createFromPath(sCurrAlbum.album_picture_path);
            }
        }
    }


    /**************************************************************************
     *
     * select album and extract list of corresponding tracks to currAlbumTrackList
     * and get album cover image
     *
     *************************************************************************/
    static void openAudioAlbum(Context context, AudioAlbum theAlbum)
    {
        Log.v(LOG_TAG, "openAudioAlbum() -- albumId == " + theAlbum.album_id);
        sCurrAlbumId = theAlbum.album_id;
        if ((sCurrAlbum == null) || (sCurrAlbum.album_id != theAlbum.album_id))
        {
            sCurrAlbum = theAlbum;
            if (sDatabaseManager != null)
                sCurrAlbumTrackList = sDatabaseManager.queryTracksOfAlbum(theAlbum);
            else
                sCurrAlbumTrackList = sDatabaseManagerAndroid.queryTracksOfAlbum(theAlbum);
        }

        initAudioAlbum(context);
    }


    /**************************************************************************
     *
     * open pseudo album for "share" and "open with"
     *
     *************************************************************************/
    @SuppressWarnings({"unused", "RedundantSuppression"})
    static void openAudioAlbumPseudo
    (
        Context context,
        ArrayList<AudioTrack> trackList,
        String albumName
    )
    {
        Log.v(LOG_TAG, "openAudioAlbumPseudo()");
        sCurrAlbumId = -1;
        sCurrAlbum = new AudioAlbum(sCurrAlbumId, albumName, null, trackList.size(), 0, 0, null);
        sCurrAlbumImageDrawable = null;
        sCurrAlbumTrackList = trackList;

        initAudioAlbum(context);
    }


    /* helper function */
    // see http://tools.android.com/tips/lint/suppressing-lint-warnings
    // see http://tools.android.com/tips/lint-checks
    @SuppressLint("DefaultLocale")
    static String convertMsToHMmSs(long ms)
    {
        long seconds = (ms + 999) / 1000;       // round up
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        if (h > 0)
            return String.format("%d:%02d:%02d", h, m, s);
        else
            return String.format("%02d:%02d", m, s);
    }


    /**************************************************************************
     *
     * get gendered version of "conductor" according to genderism mode
     *
     *************************************************************************/
    static String getGenderedConductor(Context context)
    {
        final int[] resTable =
        {
            R.string.str_conductor_gen,         // 0
            R.string.str_conductor_abbrev,
            R.string.str_conductor_ess,
            R.string.str_conductor_fm,
            R.string.str_conductor_mf,          // 4
            R.string.str_conductor_capI,
            R.string.str_conductor_underscore,
            R.string.str_conductor_x,           // 7
            R.string.str_conductor_f,
            R.string.str_conductor_star         // 9
        };

        return context.getString(resTable[sGenderismMode]);
    }


    /**************************************************************************
     *
     * get gendered version of "unknown composer" according to genderism mode
     *
     *************************************************************************/
    static String getGenderedUnknownComposer(Context context)
    {
        final int[] resTable =
        {
            R.string.str_unknown_composer_gen,         // 0
            R.string.str_unknown_composer_abbrev,
            R.string.str_unknown_composer_ess,
            R.string.str_unknown_composer_fm,
            R.string.str_unknown_composer_mf,          // 4
            R.string.str_unknown_composer_capI,
            R.string.str_unknown_composer_underscore,
            R.string.str_unknown_composer_x,           // 7
            R.string.str_unknown_composer_f,
            R.string.str_unknown_composer_star         // 9
        };

        return context.getString(resTable[sGenderismMode]);
    }


    /**************************************************************************
     *
     * names may be empty, but not null, and we replace linefeed with semicolon (for name lists)
     *
     *************************************************************************/
    static String beautifyName(String theName)
    {
        return (theName != null) ? theName.replace("\n", "; ") : "";
    }


    /**************************************************************************
     *
     * safe way to check if string differ
     *
     *************************************************************************/
    static boolean stringsDiffer(final String s1, final String s2)
    {
        if (s1 == null)
        {
            return s2 != null;
        }
        // now s1 is not null
        return !s1.equals(s2);
    }


    /**************************************************************************
     *
     * debug helper
     *
     *************************************************************************/
    static boolean pathIsReadable(String path)
    {
        File f = new File(path);
        if (f.exists())
        {
            if (f.canRead())
            {
                Log.d(LOG_TAG, "file exists and is readable: " + f.getPath());
                return true;
            }
            else
            {
                Log.e(LOG_TAG, "file exists, but is not readable: " + f.getPath());
            }
        }
        else
        {
            Log.e(LOG_TAG, "file does not exist: " + f.getPath());
        }
        return false;
    }

}
