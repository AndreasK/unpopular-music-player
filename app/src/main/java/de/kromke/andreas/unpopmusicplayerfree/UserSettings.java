/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
//import android.preference.PreferenceManager;  // fwr one shall use androidx ..
import androidx.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * helper class for user settings (preferences)
 */
class UserSettings
{
    private static final String LOG_TAG = "UMPL : UserSettings";
    static final String PREF_BEHAVIOUR_OF_BACK_KEY = "prefBehaviourOfBackKey";
    static final String PREF_AUTOHIDE_CONTROLLER = "prefAutoHideController";
    static final String PREF_DELTA_SIZE_OF_ALBUM_HEADER = "prefDeltaSizeOfAlbumHeader";
    static final String PREF_SIZE_ALBUM_LIST_ALBUM_ART = "prefSizeOfAlbumArtInAlbumList";
    static final String PREF_SIZE_ALBUM_GRID_ALBUM_ART = "prefSizeOfAlbumArtInAlbumGrid";
    static final String PREF_SHOW_TRACK_NUMBER = "prefShowTracknumber";
    static final String PREF_SHOW_ALBUM_DURATION = "prefShowAlbumDuration";
    static final String PREF_SHOW_CONDUCTOR = "prefShowConductor";
    static final String PREF_SHOW_SUBTITLE = "prefShowSubtitle";
    static final String PREF_GENDERISM_NONSENSE = "prefGenderismNonsense";
    static final String PREF_NOTIFICATION_STYLE = "prefNotificationStyle";
    static final String PREF_INSTALLED_APP_VERSION = "prefInstalledAppVersion";
    static final String PREF_BLUETOOTH_METADATA_STYLE = "prefBluetoothMetadataStyle";
    static final String PREF_USE_OWN_DATABASE = "prefUseOwnDatabase";
    static final String PREF_USE_JAUDIOTAGGER_LIB = "prefUseJaudiotaggerLib";
    static final String PREF_OWN_DATABASE_PATH = "prefOwnDatabasePath";
    static final String PREF_ALBUM_LIST_FAST_SCROLL = "prefAlbumListFastScroll";
    static final String PREF_ALBUM_LIST_SECTION_INDEXER = "prefAlbumListSectionIndexer";
    static final String PREF_SHOW_ALBUM_PROGRESS = "prefShowAlbumProgress";
    static final String PREF_THEME = "prefTheme";
    static final String PREF_GRIDVIEW = "prefGridView";
    private static final String SAVED_BOOKMARKS = "savedBookmarks";
    static final String PREF_LATEST_SAF_URI = "prefLatestSafUri";

    private static SharedPreferences mSharedPrefs;

    static class AppVersionInfo
    {
        String versionName = "";
        int versionCode = 0;
        String strCreationTime = "";
        boolean isDebug;
    }


    /** @noinspection RedundantSuppression
     *
     * Get version information. Note that we cannot avoid using deprecated
     * calls because they are not available on older Android versions.
     */
    @SuppressWarnings("deprecation")
    static AppVersionInfo getVersionInfo(Context context)
    {
        AppVersionInfo ret = new AppVersionInfo();
        PackageInfo packageinfo = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU)
        {
            PackageManager.PackageInfoFlags flags = PackageManager.PackageInfoFlags.of(0);
            try
            {
                packageinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), flags);
            }
            catch (PackageManager.NameNotFoundException e)
            {
                Log.d(LOG_TAG, "exception: " + e.getMessage());
            }
        }
        else
        {
            int flags = 0;
            try
            {
                // deprecated in API 33
                packageinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), flags);
            }
            catch (PackageManager.NameNotFoundException e)
            {
                Log.d(LOG_TAG, "exception: " + e.getMessage());
            }
        }


        if (packageinfo != null)
        {
            ret.versionName = packageinfo.versionName;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            {
                ret.versionCode = (int) packageinfo.getLongVersionCode();
            }
            else
            {
                // deprecated in API 29
                ret.versionCode = packageinfo.versionCode;
            }
        }

        // get ISO8601 date instead of dumb US format (Z = time zone) ...
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        ret.strCreationTime = df.format(buildDate);
        ret.isDebug = BuildConfig.DEBUG;

        return ret;
    }

    /*
     * Initialises the module, if necessary. Note that re-initialisation may be necessary
     * if process was restarted.
     */
    static void init(Context context)
    {
        if (mSharedPrefs == null)
        {
            mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    /*
    * updates a preference value and returns previous value or defVal, if none
     */
    @SuppressWarnings("SameParameterValue")
    @SuppressLint("ApplySharedPref")
    static int updateVal(final String key, int newVal, int defVal)
    {
        int prevVal = defVal;

        if (mSharedPrefs.contains(key))
        {
            prevVal = getVal(key, defVal);
        }

        if (prevVal != newVal)
        {
            String vds = Integer.toString(newVal);
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.commit();
        }

        return prevVal;
    }

    /*
    * get a numerical value from the preferences and repair it, if necessary
     */
    @SuppressLint("ApplySharedPref")
    static int getVal(final String key, int defaultVal)
    {
        String vds = Integer.toString(defaultVal);
        String vs = mSharedPrefs.getString(key, vds);
        boolean bWriteBack = true;      // being pessimistic
        int v;

        try
        {
            v = Integer.parseInt(vs);
            bWriteBack = false;             // success
        } catch (NumberFormatException e)
        {
            v = defaultVal;
        }

        if (bWriteBack)
        {
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.commit();
        }

        return v;
    }

    /*
     * put and commit
     */
    static void putValAndCommit(@SuppressWarnings("SameParameterValue") final String key, boolean theVal)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putBoolean(key, theVal);
        prefEditor.apply();
        prefEditor.commit();    // make sure datum is written to flash now
    }


    /*
     * same as getVal(), but save it in case it does not exist, yet
     */
    @SuppressWarnings("WeakerAccess")
    public static int getAndPutVal(final String key, int defaultVal)
    {
        String vds = Integer.toString(defaultVal);
        boolean bWriteBack = !mSharedPrefs.contains(key);   // not yet saved => bWriteBack = true
        String vs = mSharedPrefs.getString(key, vds);
        int v;
        try
        {
            v = Integer.parseInt(vs);
        } catch (NumberFormatException e)
        {
            v = defaultVal;
            bWriteBack = true;
        }

        // write it in case it has not been written or in case it's malformed
        if (bWriteBack)
        {
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.apply();
        }

        return v;
    }

    /*
     * get a text value from the preferences
     */
    public static String getString(final String key)
    {
        if (mSharedPrefs.contains(key))
        {
            return mSharedPrefs.getString(key, null);
        }
        else
        {
            return null;
        }
    }

    /*
     * put a text value to the preferences
     */
    @SuppressWarnings("WeakerAccess")
    public static void putString(final String key, String value)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putString(key, value);
        prefEditor.apply();
    }

    /*
     * put and commit. Necessary before a restart(), otherwise values will not be written.
     */
    static void putStringAndCommit(@SuppressWarnings("SameParameterValue") final String key, String value)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putString(key, value);
        prefEditor.apply();
        prefEditor.commit();    // make sure datum is written to flash now
    }

    /*
     * write a default text value to the preferences
     */
    @SuppressWarnings("WeakerAccess")
    public static void initString(final String key, String defaultVal)
    {
        if (!mSharedPrefs.contains(key))
        {
            // not set, yet: write it
            putString(key, defaultVal);
        }
    }

    /*
    * get a boolean value from the preferences and repair it, if necessary
     */
    @SuppressWarnings("SameParameterValue")
    static boolean getBool(final String key, boolean defaultVal)
    {
        return mSharedPrefs.getBoolean(key, defaultVal);
    }

    /*
     * get a themed colour value
     */
    static int getThemedColourFromResId(Context context, int id)
    {
        // strange stuff colour is a reference:
        // http://stackoverflow.com/questions/17277618/get-color-value-programmatically-when-its-a-reference-theme
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(id, typedValue, true);
        return typedValue.data;
    }

    static class Bookmark
    {
        String album;   // album name
        int track;      // relative track number, starting with 0
        long pos;       // playback position
        long time;      // creation time
    }

    /*
     * Decode bookmark from string.
     * Bookmarks are stored as four text lines.
     */
    private static Bookmark getBookmarkFromString(String entry)
    {
        Bookmark bookmark = new Bookmark();
        String[] parts = entry.split("\n");
        if (parts.length == 4)
        {
            try
            {
                bookmark.album = parts[0];
                bookmark.track = Integer.parseInt(parts[1]);
                bookmark.pos = Long.parseLong(parts[2]);
                bookmark.time = Long.parseLong(parts[3]);
                return bookmark;
            }
            catch(NumberFormatException e)
            {
                // ignore
            }
        }

        Log.e(LOG_TAG, "getBookmark() : malformed string \"" + entry + "\"");
        return null;
    }

    /*
     * Save bookmark.
     * Bookmarks are saved as string set in the preferences, and each
     * string consists of four lines.
     */
    static void putBookmark(String album, int track, long pos)
    {
        if (album == null)
        {
            return;     // nothing to do
        }

        //
        // Get the set of the stored bookmarks.
        // It's forbidden to modify the set. Instead we better create a new one by copying it.
        //

        Set<String> bookmarks = mSharedPrefs.getStringSet(SAVED_BOOKMARKS, null);
        Set<String> newBookmarks;

        //
        // Check if there is already an entry for our album. If yes, remove it.
        // Also remember the first element to be able to remove it to limit its size.
        //  This might be a hack, in case the order of the strings was changed.
        //

        if (bookmarks != null)
        {
            String albumEntry = null;       // remove this, if exists
            String oldestEntry = null;      // remove that, if necessary
            String malformedEntry = null;
            long oldestTime = 0;

            newBookmarks = new HashSet<>(bookmarks);        // make a copy, then modify that
            for (String entry : newBookmarks)
            {
                Bookmark bookmark = getBookmarkFromString(entry);
                if (bookmark != null)
                {
                    //if (entry.startsWith(album + "\n"))
                    if (bookmark.album.equals(album))
                    {
                        if ((bookmark.track == track) && (bookmark.pos == pos))
                        {
                            // nothing to do, bookmark already exists
                            return;
                        }
                        albumEntry = entry;
                    }
                    else
                    if ((oldestEntry == null) || (bookmark.time < oldestTime))
                    {
                        oldestEntry = entry;
                        oldestTime = bookmark.time;
                    }
                }
                else
                {
                    // remember malformed entry
                    malformedEntry = entry;
                }
            }

            // remove malformed entry
            if (malformedEntry != null)
            {
                newBookmarks.remove(malformedEntry);
            }

            // remove entry for our album
            if (albumEntry != null)
            {
                newBookmarks.remove(albumEntry);
            }

            // remove oldest (hopefully!) entry if size limit is exceeded
            if ((newBookmarks.size() > 100) && (oldestEntry != null))
            {
                Log.d(LOG_TAG, "putBookmark() : remove old entry \"" + oldestEntry + "\"");
                newBookmarks.remove(oldestEntry);
            }
        }
        else
        {
            newBookmarks = new HashSet<>();
        }

        //
        // Then add the new or updated entry.
        // Also store system time to be later able to remove the oldest entry
        //

        if (track >= 0)
        {
            newBookmarks.add(album + "\n" + track + "\n" + pos + "\n" + System.currentTimeMillis());
        }

        // Store the updated list also in case we removed a bookmark.
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putStringSet(SAVED_BOOKMARKS, newBookmarks);
        prefEditor.apply();
        //prefEditor.commit();
    }

    /*
     * Retrieve saved bookmark for given album
     */
    static Bookmark getBookmark(String album)
    {
        Set<String> bookmarks = mSharedPrefs.getStringSet(SAVED_BOOKMARKS, null);
        if (bookmarks != null)
        {
            for (String entry : bookmarks)
            {
                if (entry.startsWith(album + "\n"))
                {
                    return getBookmarkFromString(entry);
                }
            }
        }

        return null;
    }

    /*
     * get number of pixels for given millimeters
     */
    private static int pixelsForMillimeters(Context context, int mm)
    {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mm,
                context.getResources().getDisplayMetrics());
        return (int) px;
    }

    // get number of pixels or millimeters
    @SuppressWarnings("WeakerAccess")
    public static int pixelsOrMillimeters(Context context, int pixels, int mm)
    {
        int mpixels = pixelsForMillimeters(context, mm);
        return Math.max(pixels, mpixels);
    }
}
