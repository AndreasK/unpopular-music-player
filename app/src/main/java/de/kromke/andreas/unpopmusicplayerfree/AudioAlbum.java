/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.annotation.SuppressLint;
import android.content.Context;

/**
 * all necessary information about an album
 * @noinspection JavadocBlankLines, RedundantSuppression
 */
class AudioAlbum
{
    //private static String str_various_composers = null;
    private static String str_various_performers = null;
    private static final String sVarious = "≠";


    long album_id;                      // from MediaStore.Audio.Albums._ID, not "ALBUM_ID" (Android BUG!)
    String album_name;                  // from MediaStore.Audio.Albums.ALBUM)
    private final String fake_album_artist;   // from MediaStore.Audio.Albums.ARTIST (ignores album artist metadata, Android BUG!)
    long album_no_of_tracks;            // from MediaStore.Audio.Albums.NUMBER_OF_SONGS ("songs" is nonsense...)
//  String album_key;                   // for future use
//  int album_no_of_discs;              // for future use
    long album_duration = 0;            // 0: unknown resp. not yet calculated
    private String album_artist = null; // null: not set
    private String common_composer = null;      // null: not set, "": there is none
    private String common_performer = null;     // null: not set, "": there is none
    int common_year = -1;               // -1: not set, 0: there is none
    boolean bHasGroups = false;         // false: every work of album consists of exactly one track
//  String album_genre;             // for future use
    // see http://tools.android.com/tips/lint/suppressing-lint-warnings
    // see http://tools.android.com/tips/lint-checks
    @SuppressLint({"UnusedAttribute"})
    @SuppressWarnings({"unused", "RedundantSuppression"})
    private final long album_first_year;   // for future use
    @SuppressLint("UnusedAttribute")
    @SuppressWarnings({"unused", "RedundantSuppression"})
    private final long album_last_year;    // for future use
    String album_picture_path;       // cover art (path of jpg or png file)


    @SuppressWarnings("WeakerAccess")
    public static void SetContext(Context theContext)
    {
        //str_various_composers = theContext.getString(R.string.str_various_composers);
        str_various_performers = theContext.getString(R.string.str_various_performers);
    }


    /**************************************************************************
     *
     * initialising constructor
     *
     *************************************************************************/
    AudioAlbum(
            long id,
            String name,
            String artist,
//          String audioTrackAlbumKey,
            long no_of_tracks,
            long first_year,
            long last_year,
            String picture_path)
    {
        album_id           = id;
        album_name         = name;
        fake_album_artist  = AppGlobals.beautifyName(artist);
//      album_key          = audioTrackAlbumKey;
        album_no_of_tracks = no_of_tracks;
        album_first_year   = first_year;
        album_last_year    = last_year;
        album_picture_path = (picture_path != null) ? picture_path : "";
    }


    /**************************************************************************
     *
     * Used to investigate if there is a common "album composer"
     *
     * If composer differs from that of other tracks in same album, there is
     * none, indicated by "".
     *
     *************************************************************************/
    void setAlbumComposer(String composer)
    {
        if (composer != null)
        {
            if (common_composer == null)
            {
                common_composer = AppGlobals.beautifyName(composer);
            } else
            {
                if (!common_composer.equals(AppGlobals.beautifyName(composer)))
                {
                    common_composer = "";
                }
            }
        }
    }


    /**************************************************************************
     *
     * Used to investigate if there is a common "album performer"
     *
     * If performer differs from that of other tracks in same album, there is
     * none, indicated by "".
     *
     *************************************************************************/
    void setAlbumPerformer(String performer)
    {
        if (performer != null)
        {
            if (common_performer == null)
            {
                common_performer = AppGlobals.beautifyName(performer);
            } else
            {
                if (!common_performer.equals(AppGlobals.beautifyName(performer)))
                {
                    common_performer = "";
                }
            }
        }
    }


    /**************************************************************************
     *
     * pass album_artist tag from file
     *
     * We take the first string that is not null
     *
     *************************************************************************/
    void setAlbumArtist(String artist)
    {
        if (artist != null)
        {
            if (album_artist == null)
            {
                if (artist.equalsIgnoreCase("various"))
                {
                    album_artist = str_various_performers;
                }
                else
                {
                    album_artist = AppGlobals.beautifyName(artist);
                }
            }
        }
    }


    boolean hasCommonComposer()
    {
        return (common_composer != null) && (!common_composer.isEmpty()) && !(common_composer.equals(sVarious));
    }


    boolean hasCommonPerformer()
    {
        return (common_performer != null) && (!common_performer.isEmpty()) && !(common_performer.equals(sVarious));
    }


    /**************************************************************************
     *
     * There are three methods to get the album artist. Choose the best one
     * that is available.
     *
     * returns either null or a non-empty string
     *
     *************************************************************************/
    public final String getComposer()
    {
        if ((common_composer != null) && !common_composer.isEmpty())
        {
            if (common_composer.equals(sVarious))
            {
                return "";  // str_various_composers
            }
            else
            {
                return common_composer;
            }
        }

        return null;
    }


    /**************************************************************************
     *
     * There are three methods to get the album artist. Choose the best one
     * that is available.
     *
     * returns either null or a non-empty string
     *
     *************************************************************************/
    public final String getArtist()
    {
        if ((album_artist != null) && !album_artist.isEmpty())
        {
            return album_artist;            // explicitly read from audio file tag ALBUM_ARTIST
        }
        if ((common_performer != null) && !common_performer.isEmpty())
        {
            if (common_performer.equals(sVarious))
            {
                return str_various_performers;
            }
            else
            {
                return common_performer;        // derived from audio file tag ARTIST
            }
        }
        if ((fake_album_artist != null) && !fake_album_artist.isEmpty())
        {
            return fake_album_artist;      // fake info from Android
        }

        return null;
    }


    /**************************************************************************
     *
     * returns either "" or a non-empty string
     *
     *************************************************************************/
    final String getArtistAndYear()
    {
        String artist = getArtist();
        if (artist == null)
        {
            if (common_year <= 0)
            {
                return "";              // neither artist nor year
            }
            artist = "";
        }
        else
        {
            if (common_year <= 0)
            {
                return artist;          // artist, but not year
            }
            artist += " ";              // space between artist and year
        }

        return artist + "(" + common_year + ")";
    }


    /**************************************************************************
     *
     * Used to investigate if there is a common "album year"
     *
     * If year differs from that of other tracks in same album, there is
     * none, indicated by 1.
     *
     *************************************************************************/
    void setAlbumYear(int year)
    {
        if (common_year == -1)
        {
            common_year = year;
        }
        else
        {
            if (common_year != year)
            {
                common_year = 0;
            }
        }
    }

    public String getSection() { return album_name; }
}
