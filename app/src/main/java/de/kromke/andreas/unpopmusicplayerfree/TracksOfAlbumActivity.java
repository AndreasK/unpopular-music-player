/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
//import android.widget.ListView;
//import android.graphics.drawable.Drawable;
//import android.widget.AdapterView;
//import android.widget.RelativeLayout;
//import android.view.ViewGroup;

import android.os.IBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static de.kromke.andreas.unpopmusicplayerfree.AppGlobals.convertMsToHMmSs;
import static de.kromke.andreas.unpopmusicplayerfree.MainActivity.EXTRA_MESSAGE_ALBUM_ID;
import static de.kromke.andreas.unpopmusicplayerfree.UserSettings.PREF_THEME;
import static de.kromke.andreas.unpopmusicplayerfree.UserSettings.getVal;

import de.kromke.andreas.utilities.PrivateLog;


/**
 * activity screen that presents all the tracks belonging to one album
 * @noinspection JavadocBlankLines, RedundantSuppression, JavadocLinkAsPlainText
 */
@SuppressWarnings({"Convert2Lambda", "SpellCheckingInspection"})
public class TracksOfAlbumActivity extends AppCompatActivity
        implements ServiceConnection, MediaPlayerControl,
        MediaPlayService.MediaPlayServiceControl/*,
        MyListView.OnNoItemClickListener,
        MyListView.OnItemClickListener,
        MyListView.OnItemLongClickListener*/
{
    private static final String LOG_TAG = "UMPL : TracksOfAlbAct";
    private static final String STATE_CURR_TRACK_NO = "currTrackNo";
    private static final String STATE_BUFFERED_PLAY_STATE = "bufferedPlayState";
    private static final String STATE_PENDING_BOOKMARK_TRACKNO = "pendingBookmarkTrackno";
    private static final String STATE_PENDING_BOOKMARK_POS = "pendingBookmarkPos";
    private static final String STATE_SAVED_TRACK_NO = "savedTrackNo";
    private static final String STATE_SAVED_TRACK_POSITION = "savedTrackPosition";

    private int mColourTheme = -1;
    //private MyListView mTracksView;            // custom list view!
    private RecyclerView mTracksRecyclerView;
    private int mTracksRecyclerBottom = 0;      // needed for visibility calculations

    private int mCurrTrackNo = -1;
    private long mCurrAlbumId = -1;
    //private TracksAdapter mTracksAdapter;
    private TracksRecyclerAdapter mTracksRecyclerAdapter;
    private FloatingActionButton mFloatingButton;
    private boolean mbFloatingButtonIsPlaying;

    //bookmarks
    private static final long bookmarkRepeatMs = 5000;      // repeat the last seconds when continuing the track
    private int mPendingBookmarkTrackNo = -1;
    private long mPendingBookmarkPos = 0;
    private int mSavedTrackNo = -1;
    private long mSavedTrackPosition = 0;

    // Service
    private static boolean mServiceStarted = false;
    private MediaPlayService mMediaPlayService = null;
    private Intent mPlayIntent;

    // timer
    Timer mTimer;
    MyTimerTask mTimerTask;
    private static final int timerFrequency = 1000;         // milliseconds

    /* TEST CODE
    private static final String ACTION_STRING_SERVICE = "ToService";
    private static final String ACTION_STRING_ACTIVITY = "ToActivity";
    private BroadcastReceiver activityReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Log.i(LOG_TAG, "BroadcastReceiver::onReceive() message from Service");
        }
    };
    */

    //mMediaController (showing the prev/next/play/pause buttons inside the activity window)
    private MyMediaController mMediaController;
    private static int mMediaControllerHeight = -1;    // hack, unfortunately, shall survive activity recreation, thus make it static
    private AlertDialog mPendingDialog = null;       // test code

    //activity and playback pause flags
    private boolean mbActivityPaused = false;
    private boolean mBufferedPlayState;
    private int mBufferedPosition = 0;
    private int mBufferedDuration = 0;
    //private int headerClicked = 0;
    //private int origHeaderHeight, origTitleHeight, origImageWidth;

    //preferences
    //private int mAutoplayMode;
    private int mAutohideModeMs;
    private int mBehaviourOfBackKeyMode;
    private boolean mbShowAlbumProgress;
    //static boolean mbUseCollapsingLayout = true;

    private String getBookmarkInfo()
    {
        return "Track " +
        (mPendingBookmarkTrackNo + 1) + " (" +
                convertMsToHMmSs(mPendingBookmarkPos) + ")";
    }


    /************************************************************************************
     *
     * Activity method: create the activity
     *
     *  Note that this is also called in case the device had been turned, or
     *  in case the device had been shut off for a while. In this case we only have
     *  to re-initialise the GUI, but not the tables, database etc.
     *
     * Some recommend that onStart() binds to the service, onStop() unbinds,
     * but due to a bug in Samsung's Android "interpretation" we MUST do it
     * in create/destroy, otherwise our service will be killed by Samsung after unbind.
     * There is no problem with Google, Motorola, Nokia and Huawei. On the other
     * hand, Google's example also uses onCreate()/onDestroy() for bind/unbind.
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        PrivateLog.d(LOG_TAG, "onCreate()");
        super.onCreate(savedInstanceState);

        //
        // restore state and get global variables
        //

        // get parameters from caller
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            mCurrAlbumId = extras.getLong(EXTRA_MESSAGE_ALBUM_ID);
            PrivateLog.d(LOG_TAG, "onCreate() : got album id " + mCurrAlbumId + " from intent extras");
        }

        UserSettings.init(this);

        // restore state, if given
        if (savedInstanceState != null)
        {
            PrivateLog.d(LOG_TAG, "onCreate() : restore from saved instance");
            restoreInstanceState(savedInstanceState);
        }
        else
        {
            mCurrTrackNo = -1;
            mBufferedPlayState = false;

            mPendingBookmarkTrackNo = -1;
            mPendingBookmarkPos = 0;
            mSavedTrackNo = -1;

            if (!isCurrAlbumPseudo())
            {
                UserSettings.Bookmark bookmark = UserSettings.getBookmark(getCurrAlbumName());
                if ((bookmark != null) && (bookmark.track < AppGlobals.getAudioAlbumTrackListSize()))
                {
                    mPendingBookmarkTrackNo = bookmark.track;
                    mPendingBookmarkPos = bookmark.pos;
                    Toast.makeText(this, "Bookmark found: " + getBookmarkInfo(),
                            Toast.LENGTH_LONG).show();
                    PrivateLog.d(LOG_TAG, "onCreate() : bookmark track=" + bookmark.track + ", pos=" + mPendingBookmarkPos);
                }
            }
        }

        updateSettings();

        // init and restore album and track lists
        boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
        String ownDatabasePath;
        if (bUseOwnDatabase)
        {
            ownDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);  // might be null
        }
        else
        {
            ownDatabasePath = null;
        }
        boolean bUseJaudioTaglib = UserSettings.getBool(UserSettings.PREF_USE_JAUDIOTAGGER_LIB, false);
        AppGlobals.initAppGlobals(this, mCurrAlbumId, ownDatabasePath, false, bUseJaudioTaglib);
        mAutohideModeMs = UserSettings.getVal(UserSettings.PREF_AUTOHIDE_CONTROLLER, 0);
        if (mAutohideModeMs < 10)
        {
            mAutohideModeMs *= 1000;   // HACK: old settings
        }
        mBehaviourOfBackKeyMode = UserSettings.getVal(UserSettings.PREF_BEHAVIOUR_OF_BACK_KEY, 0);
        mbShowAlbumProgress = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_PROGRESS, false);

        //
        // Create screen, i.e. album header and list of tracks
        //

        //if (mbUseCollapsingLayout)
            initUiCollapsing();
        //else
        //    initUi();

        // this is used for communication with the service
        mPlayIntent = new Intent(this, MediaPlayService.class);

        //setup mMediaController
        // after screen rotation the media controller should be made visible,
        // but this is not possible even in resume, because of bad Android design
        createMusicController();

        /*
        // register the broadcast receiver so that we can receive messages from Service
        if (activityReceiver != null)
        {
            //Create an intent filter to listen to the broadcast sent with the action "ACTION_STRING_ACTIVITY"
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            //Map the intent filter to the receiver
            registerReceiver(activityReceiver, intentFilter);
        }
        */

        // Make sure Volume up/down keys change music volume, not ringer volume,
        // this has already been done for the other activity, but it seems the setting is activity bound.
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // we should bind at Start and unbind at Stop, but try to do with create/destroy
        bindToService();
    }


    /************************************************************************************
     *
     * Activity method: save settings
     *
     ***********************************************************************************/
    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState)
    {
        PrivateLog.d(LOG_TAG, "onSaveInstanceState()");
        if (mCurrTrackNo != -1)
        {
            savedInstanceState.putBoolean(STATE_BUFFERED_PLAY_STATE, mBufferedPlayState);
            savedInstanceState.putInt(STATE_CURR_TRACK_NO, mCurrTrackNo);
            PrivateLog.d(LOG_TAG, "onSaveInstanceState() : save track no " + mCurrTrackNo);
            PrivateLog.d(LOG_TAG, "onSaveInstanceState() : save play state " + mBufferedPlayState);
        }

        savedInstanceState.putInt(STATE_PENDING_BOOKMARK_TRACKNO, mPendingBookmarkTrackNo);
        savedInstanceState.putLong(STATE_PENDING_BOOKMARK_POS, mPendingBookmarkPos);
        savedInstanceState.putInt(STATE_SAVED_TRACK_NO, mSavedTrackNo);
        savedInstanceState.putLong(STATE_SAVED_TRACK_POSITION, mSavedTrackPosition);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    /************************************************************************************
     *
     * helper: restore saved settings
     *
     * Called only from onCreate()
     *
     ***********************************************************************************/
    private void restoreInstanceState(Bundle savedInstanceState)
    {
        PrivateLog.d(LOG_TAG, "restoreInstanceState()");

        mCurrTrackNo = savedInstanceState.getInt(STATE_CURR_TRACK_NO, -1);
        mBufferedPlayState = savedInstanceState.getBoolean(STATE_BUFFERED_PLAY_STATE, false);
        mPendingBookmarkTrackNo = savedInstanceState.getInt(STATE_PENDING_BOOKMARK_TRACKNO, -1);
        mPendingBookmarkPos = savedInstanceState.getLong(STATE_PENDING_BOOKMARK_POS, 0);
        mSavedTrackNo = savedInstanceState.getInt(STATE_SAVED_TRACK_NO, -1);
        mSavedTrackPosition = savedInstanceState.getLong(STATE_SAVED_TRACK_POSITION, 0);
        PrivateLog.d(LOG_TAG, "onCreate() : restore track no " + mCurrTrackNo + " from saved instance state");
        PrivateLog.d(LOG_TAG, "onCreate() : restore play state " + mBufferedPlayState + " from saved instance state");
    }


    /**************************************************************************
     *
     * Activity method, pendant to onStop()
     *
     * called after onCreate() or after the application has been put back to
     * the foreground.
     * After onStart() the method onResume() is called.
     *
     *************************************************************************/
    @Override
    protected void onStart()
    {
        PrivateLog.d(LOG_TAG, "onStart()");

        // manage progress bar via timer and avoid "jumps"
        updateProgress();
        mTimerTask = new MyTimerTask();

        //delay 1000ms, repeat in <timerFrequency>ms
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 1000, timerFrequency);

        super.onStart();
        updateFloatingButton();
    }


    /**************************************************************************
     *
     * Activity method, pendant to onPause()
     *
     *************************************************************************/
    @Override
    protected void onResume()
    {
        PrivateLog.d(LOG_TAG, "onResume()");
        super.onResume();
        if (mbActivityPaused)
        {
            mbActivityPaused = false;
        }
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    public void onAttachedToWindow()
    {
        PrivateLog.d(LOG_TAG, "onAttachedToWindow()");
        super.onAttachedToWindow();
        showMediaControllerIfNecessary();
    }


    /**************************************************************************
     *
     * Activity method, pendant is onResume()
     *
     * Called before the application is put to the background.
     * After onPause() the method onStop() is called.
     *
     *************************************************************************/
    @Override
    protected void onPause()
    {
        PrivateLog.d(LOG_TAG, "onPause()");
        super.onPause();
        mbActivityPaused = true;
    }


    /**************************************************************************
     *
     * Activity method, pendant is onStart()
     *
     *************************************************************************/
    @Override
    protected void onStop()
    {
        PrivateLog.d(LOG_TAG, "onStop()");

        mTimer.cancel();    // note that a cancelled timer cannot be re-scheduled. Why not?
        mTimer = null;
        mTimerTask = null;

        //noinspection StatementWithEmptyBody
        if (mMediaController != null)
        {
            // TODO: it might be wise to hide the controller here in order to save energy.
            // But this solved not the activity-has-leaked-media-controller-window problem,
            // which seems to be an Android bug.
        }

        super.onStop();
    }


    /**************************************************************************
     *
     * Activity method, pendant is onCreate()
     *
     *************************************************************************/
    @Override
    protected void onDestroy()
    {
        PrivateLog.d(LOG_TAG, "onDestroy()");

        if (mPendingDialog != null)
        {
            if (mPendingDialog.isShowing())
            {
                mPendingDialog.dismiss();
            }
            mPendingDialog = null;
        }

        //saveStateForBookmark(true);

        /*
        unregisterReceiver(activityReceiver);
        */

        unbindFromService();
        if (isFinishing())
        {
            if (mServiceStarted)
            {
                // only stop service in case app is finishing
                stopService(mPlayIntent);
                mServiceStarted = false;
            }

            mCurrTrackNo = -1;
        }

        if (mMediaController != null)
        {
            //PrivateLog.d(LOG_TAG, "onDestroy() : hide media controller");
            mMediaController.reallyHide();
            mMediaController.setEnabled(false);
            mMediaController = null;
        }

        super.onDestroy();
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        PrivateLog.d(LOG_TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.tracks_of_album_action_bar, menu);

        return true;
    }
/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        //menu item selected
        switch (item.getItemId())
        {
            case R.id.action_end:
                PrivateLog.d(LOG_TAG, "onOptionsItemSelected() : end");
                if (mCurrTrackNo >= 0)
                {
                    StopPlaying();
                }
                //unbindService(musicConnection);
                //mMediaPlayService = null;
                //finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
*/


    /* *************************************************************************
     *
     * Activity method: we want to handle BACK key longpress
     *
     *************************************************************************/
    /*
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event)
    {
        PrivateLog.d(LOG_TAG, "onKeyLongPress() : keyCode = " + keyCode);
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            dialogSetBookmark();
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }
    */


    /**************************************************************************
     *
     * Activity method: we want to handle the BACK key
     *
     *************************************************************************/
    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        int keyCode = event.getKeyCode();
        int actionCode = event.getAction();
        PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : keyCode = " + keyCode + ", actionCode = " + actionCode);
        //final boolean uniqueDown = (event.getRepeatCount() == 0) && (event.getAction() == KeyEvent.ACTION_DOWN);
        //if (uniqueDown)
        {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && (mCurrTrackNo >= 0))
            {
                // currently playing. Stop on key release, but also consume key down event
                if (actionCode == KeyEvent.ACTION_UP)
                {
                    if (mBehaviourOfBackKeyMode == 0)
                    {
                        if ((mAutohideModeMs != 0) && (mMediaController != null) && (!mMediaController.isShowing()))
                        {
                            // show hidden controls
                            mMediaController.show(mAutohideModeMs);
                            Toast.makeText(this, "Press BACK again to stop playback!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            // controls are visible
                            stopPlaying();
                        }
                    }
                    else
                    if (mBehaviourOfBackKeyMode == 1)
                    {
                        if (mBufferedPlayState)
                        {
                            pause();
                        }
                        else
                        {
                            stopPlaying();
                        }
                    }
                    else
                    if (mBehaviourOfBackKeyMode == 2)
                    {
                        stopPlaying();
                    }
                }
                return true;    // event was consumed
            }
            else
            //noinspection StatementWithEmptyBody
            if (keyCode == KeyEvent.KEYCODE_MENU)
            {
                // reserved for future use
            }
        }

        return super.dispatchKeyEvent(event);
    }


    /**********************************************************************************************
     *
     * update the progress circle here
     *
     *********************************************************************************************/
    public void TimerCallback()
    {
        updateProgress();
    }


    /**************************************************************************
     *
     * helper to re-show the music controller if necessary and possible
     *
     *************************************************************************/
    private void showMediaControllerIfNecessary()
    {
        if (mMediaPlayService != null)
        {
            // without service, we cannot know if status is playing or not
            if ((mCurrTrackNo >= 0) && (mMediaController != null) && !(mMediaController.isShowing()))
            {
                // media controller should be visible, but is not. Show it.
                mMediaController.show(mAutohideModeMs);
            }
        }
    }


    /**************************************************************************
     *
     * helper to update the play/pause state of the music controller if necessary and possible
     *
     *************************************************************************/
    private void updateMediaController()
    {
        if (mMediaPlayService != null)
        {
            // without service, we cannot know if status is playing or not
            if ((mCurrTrackNo >= 0) && (mMediaController != null))
            {
                // workaround for one of the media controller's bugs
                mMediaController.show(mAutohideModeMs);
            }
        }
    }


    /* *************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    /*
    private static int changeViewWidthAbsOrRelative(View v, int wabs, int wdelta)
    {
        int wprev;
        ViewGroup.LayoutParams params = v.getLayoutParams();
        wprev = params.width;
        if (wabs >= 0)
            params.width = wabs;
        else
            params.width += wdelta;
        v.setLayoutParams(params);
        return wprev;
    }
    */


    /* *************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    /*
    private static int changeViewHeightAbsOrRelative(View v, int habs, int hdelta)
    {
        int hprev;
        ViewGroup.LayoutParams params = v.getLayoutParams();
        hprev = params.height;
        if (habs >= 0)
            params.height = habs;
        else
            params.height += hdelta;
        v.setLayoutParams(params);
        return hprev;
    }
    */


    /* *************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    /*
    private static int changeViewGroupHeightAbsOrRelative(ViewGroup v, int habs, int hdelta)
    {
        int hprev;
        ViewGroup.LayoutParams params = v.getLayoutParams();
        hprev = params.height;
        if (habs >= 0)
            params.height = habs;
        else
            params.height += hdelta;
        v.setLayoutParams(params);
        return hprev;
    }
    */


    /**************************************************************************
     *
     * MediaPlayerControl callbacks
     *
     *************************************************************************/

    @Override
    public boolean canPause()
    {
        return true;
    }

    @Override
    public boolean canSeekBackward()
    {
        return true;
    }

    @Override
    public boolean canSeekForward()
    {
        return true;
    }

    @Override
    public int getAudioSessionId()
    {
        return 0;
    }

    @Override
    public int getBufferPercentage()
    {
        return 0;
    }

    @Override
    public int getCurrentPosition()
    {
        // use this hack to get the media controller height. TODO: find a better method
        if ((mMediaControllerHeight <= 0) && (mMediaController != null))
        {
            // not fetched, yet
            mMediaControllerHeight = mMediaController.getHeight();
            if (mMediaControllerHeight > 0)
            {
                PrivateLog.d(LOG_TAG, "media controller height is " + mMediaControllerHeight);
                if (mTracksRecyclerAdapter != null)
                {
                    mTracksRecyclerAdapter.setDummyTrackHeight(mMediaControllerHeight);
                }
                /*
                else
                if (mTracksAdapter != null)
                {
                    mTracksAdapter.setDummyTrackHeight(mMediaControllerHeight);
                }
                */
            }
        }

        //Log.v(LOG_TAG, "getCurrentPosition()");
        if ((mMediaPlayService != null) && (mMediaPlayService.isPlaying()))
        {
            mBufferedPosition = mMediaPlayService.getPosition();
        }
        updateControllerAndList();

        return mBufferedPosition;
    }

    @Override
    public int getDuration()
    {
        //Log.v(LOG_TAG, "getDuration()");
        if ((mMediaPlayService != null) && (mMediaPlayService.isPlaying()))
        {
            mBufferedDuration = mMediaPlayService.getDuration();
        }
//        updateControllerAndList();

        return mBufferedDuration;
    }

    @Override
    public boolean isPlaying()
    {
        mBufferedPlayState = audioIsPlaying();
        //Log.v(LOG_TAG, "isPlaying() => " + buffered_play_state);
        return mBufferedPlayState;
    }

    // called when the media mMediaController PAUSE button had been pressed
    @Override
    public void pause()
    {
        PrivateLog.d(LOG_TAG, "pause()");
        mBufferedPlayState = false;   // HACK
        if (mMediaPlayService != null)
        {
            saveStateForBookmark();     // also update buffered position
            mPendingBookmarkTrackNo = mSavedTrackNo;        // experiment to avoid losing the bookmark
            mPendingBookmarkPos = mSavedTrackPosition;      // experimental
            mMediaPlayService.pausePlayer();
        }
        updateFloatingButton();
    }

    @Override
    public void seekTo(int pos)
    {
        PrivateLog.d(LOG_TAG, "seekTo()");
        mBufferedPosition = pos;
        if (mMediaPlayService != null)
        {
            mMediaPlayService.seek(pos);
        }

        updateProgress();
    }

    // called when the media mMediaController PLAY button had been pressed
    @Override
    public void start()
    {
        PrivateLog.d(LOG_TAG, "start()");
        if (mMediaPlayService != null)
        {
            mMediaPlayService.continuePlayer();
        }
        updateFloatingButton();
    }

    // called when the media mMediaController PLAY_NEXT button had been pressed
    private void playNext()
    {
        PrivateLog.d(LOG_TAG, "playNext()");
        gotoTrackRel(1);
        updateFloatingButton();
    }

    // called when the media mMediaController PLAY_PREV button had been pressed
    private void playPrev()
    {
        PrivateLog.d(LOG_TAG, "playPrev()");
        gotoTrackRel(-1);
        updateFloatingButton();
    }


    /**************************************************************************
     *
     * informational callback from MediaPlayService, called after it has been
     * handled by the service
     *
     *************************************************************************/
    @Override
    public void onNotificationPlayPauseChanged()
    {
        mBufferedPlayState = isPlaying();
        PrivateLog.d(LOG_TAG, "onNotificationPlayPauseChanged() : play = " + mBufferedPlayState);

        updateMediaController();
        updateFloatingButton();
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private boolean audioIsPaused()
    {
        return ((mMediaPlayService != null) && (mMediaPlayService.isPaused()));
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private boolean audioIsPlaying()
    {
        //PrivateLog.d(LOG_TAG, "audioIsPlaying() : media play service is " + ((mMediaPlayService != null) ? "available" : "null"));
        return (mMediaPlayService != null) && (mMediaPlayService.isPlaying());
    }


    /**************************************************************************
     *
     * informational callback from MediaPlayService, called after it has been
     * handled by the service
     *
     * newTrackNo == -1 means STOP
     *
     *************************************************************************/
    @Override
    public void onNotificationTrackChanged(int newTrackNo)
    {
        PrivateLog.d(LOG_TAG, "onNotificationPlayNextOrPrevPressed(" + newTrackNo + ")");
        mBufferedPosition = 0;
        mBufferedDuration = 0;
        if ((mCurrTrackNo >= 0) && (newTrackNo < 0))
        {
            if (mMediaController != null)
            {
                // hide the controller overlay view
                mMediaController.reallyHide();
            }
        }

        updateList(newTrackNo);

        // now mCurrTrackNo has been updated with newTrackNo
        if (mCurrTrackNo >= 0)
        {
            mBufferedPlayState = true;
            PrivateLog.d(LOG_TAG, "gotoTrack() : buffered_play_state set to true");
            if (mMediaController != null)
            {
                mMediaController.show(mAutohideModeMs);
            }
        }

        // This shall remove the album bookmark, if the last track has just stopped.
        saveStateForBookmark();
        updateFloatingButton();
    }


    /**************************************************************************
     *
     * Service methods
     *
     *************************************************************************/

    private void bindToService()
    {
        if (!mServiceStarted)
        {
            // This is the first start. Start background service.
            // Make sure service keeps running when activity is unbound.
            // With Android S and above this can cause a security exception
            try
            {
                startService(mPlayIntent);
                mServiceStarted = true;
            }
            catch(Exception e)
            {
                PrivateLog.e(LOG_TAG, "bindToService() : " + e);
                Toast.makeText(this, "FATAL: Background Service start denied.", Toast.LENGTH_LONG).show();
                return;
            }
        }

        // bind in order to be able to control the service
        bindService(mPlayIntent, this, Context.BIND_AUTO_CREATE);
    }

    private void unbindFromService()
    {
        unbindService(this);
        mMediaPlayService = null;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service)
    {
        PrivateLog.d(LOG_TAG, "onServiceConnected()");
        MediaPlayService.MediaPlayServiceBinder binder = (MediaPlayService.MediaPlayServiceBinder) service;
        //get service
        mMediaPlayService = binder.getService();
        // register for callbacks
        mMediaPlayService.setController(this);
        updateFloatingButton();             // on turning the device, we lost the service and have to reconnect
        // Attention: This call will set mCurrTrackNo to 0, because it asks the service
        showMediaControllerIfNecessary();   // we cannot do this without the service
    }

    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        PrivateLog.d(LOG_TAG, "onServiceDisconnected()");
        mMediaPlayService = null;
    }


    /**************************************************************************
     *
     * setup Media Controller
     *
     *************************************************************************/
    private void createMusicController()
    {
        PrivateLog.d(LOG_TAG, "createMusicController()");
        mMediaController = new MyMediaController(this, mAutohideModeMs != 0);
        //set previous and next button listeners
        mMediaController.setPrevNextListeners(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playNext();
            }
        }, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playPrev();
            }
        });
        //set and show
        mMediaController.setMediaPlayer(this);
        View v;
        /*
        v = findViewById(R.id.tracks_album_header);
        PrivateLog.d(LOG_TAG, "createMusicController() : header w = " + v.getMeasuredWidth() + " h = " + v.getMeasuredHeight());
        v = findViewById(R.id.audioTrack_all);
        PrivateLog.d(LOG_TAG, "createMusicController() : space  w = " + v.getMeasuredWidth() + " h = " + v.getMeasuredHeight());
        */
        //if (mbUseCollapsingLayout)
        {
            //v = mTracksRecyclerView;      // DOES NOT WORK AS CONTROLLER WILL BE PARTIALLY HIDDEN BELOW NAVIGATION BAR
            v = findViewById(R.id.total);
        }
        /*
        else
        {
            v = mTracksView;
        }
        */
        //PrivateLog.d(LOG_TAG, "createMusicController() : list   w = " + v.getMeasuredWidth() + " h = " + v.getMeasuredHeight());
        mMediaController.setAnchorView(v);
        mMediaController.setEnabled(true);
    }


    /* *************************************************************************
     *
     * ListView helper
     *
     * Not used for RecyclerView
     *
     *************************************************************************/
    /*
    private View getViewByPosition(int pos, ListView listView)
    {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition)
        {
            return listView.getAdapter().getView(pos, null, listView);
        } else
        {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }
    */


    /* ************************************************************************
     *
     * helper
     *
     *************************************************************************/
    /*
    private View getViewByPosition(int pos, RecyclerView v)
    {
        RecyclerView.ViewHolder vh = v.findViewHolderForAdapterPosition(pos);
        if (vh != null)
        {
            return vh.itemView;
        }
        return null;
    }
    */


    /* *************************************************************************
     *
     * set the background colour for a listview item given by position
     *
     * Not used for RecyclerView
     *
     *************************************************************************/
    /*
    private void setBgColourForPos(int pos, int colour)
    {
        if (pos >= 0)
        {
            View v;
            if (mTracksView != null)
            {
                v = getViewByPosition(pos, mTracksView);
                if (v != null)
                {
                    v.setBackgroundColor(colour);
                }
            }

        }
    }
    */


    /* *************************************************************************
     *
     * helper
     *
     *************************************************************************/
    /*
    private void getRecyclerViewShowingRange()
    {
        if (mTracksRecyclerLayoutManager != null)
        {
            PrivateLog.d("TAG", "findFirstVisibleItemPosition: " + mTracksRecyclerLayoutManager.findFirstVisibleItemPosition());
            PrivateLog.d("TAG", "findFirstCompletelyVisibleItemPosition: " + mTracksRecyclerLayoutManager.findFirstCompletelyVisibleItemPosition());
            PrivateLog.d("TAG", "findLastVisibleItemPosition: " + mTracksRecyclerLayoutManager.findLastVisibleItemPosition());
            PrivateLog.d("TAG", "findLastCompletelyVisibleItemPosition: " + mTracksRecyclerLayoutManager.findLastCompletelyVisibleItemPosition());
            PrivateLog.d("TAG", "------------------------");

            //int index = mTracksRecyclerLayoutManager.findFirstVisibleItemPosition();
            //View v = mTracksRecyclerLayoutManager.getChildAt(0);
            //int top = (v == null) ? 0 : (v.getTop() - mTracksRecyclerLayoutManager.getPaddingTop());
            //PrivateLog.d("TAG", "visible position: " + index + "/" + top);
        }
    }
    */


    /**************************************************************************
     *
     * check if list item is visible
     *
     *************************************************************************/
    private boolean isListItemVisible(int pos)
    {
        if (mTracksRecyclerView != null)
        {
            RecyclerView.ViewHolder vh = mTracksRecyclerView.findViewHolderForAdapterPosition(pos);
            if (vh != null)
            {
                int[] xy = new int[2];

                if (mTracksRecyclerBottom == 0)
                {
                    // Bottom not yet calculated. We cannot get bottom from recycler view itself,
                    // because it gets scrolled outside of the screen while header is expanded
                    View vt = findViewById(R.id.total);     // use the recycler's parent to get the coordinates
                    if (vt != null)
                    {
                        vt.getLocationOnScreen(xy);
                        mTracksRecyclerBottom = xy[1] + vt.getHeight();
                    }
                }

                mTracksRecyclerView.getLocationOnScreen(xy);
                int recyclerY = xy[1];      // note that recycler Y position changes with header expanding and collapsing

                View v = vh.itemView;
                //if (v != null)
                {
                    v.getLocationOnScreen(xy);
                    //int w = v.getWidth();
                    int h = v.getHeight();
                    //PrivateLog.d(LOG_TAG, "item pos: y = " + xy[1] + ", h = " + h);

                    if (xy[1] < recyclerY)
                    {
                        return false;   // item is at least partially above its parent
                    }
                    //noinspection RedundantIfStatement
                    if (xy[1] + h > mTracksRecyclerBottom)
                    {
                        return false;   // item is at least partially below its parent
                    }
                    return true;
                }
            }
        }
        return false;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void collapseToolbar()
    {
        AppBarLayout l = findViewById(R.id.appbar);
        if (l != null)
        {
            l.setExpanded(false, true);
        }
    }
    /*
    private void expandToolbar()
    {
        AppBarLayout l = findViewById(R.id.appbar);
        if (l != null)
        {
            l.setExpanded(true, true);
        }
    }
    */


    /**************************************************************************
     *
     * update list item with progress
     *
     *************************************************************************/
    private void updateProgressView(int trackNo, int percentage, int albumPercentage)
    {
        if ((mTracksRecyclerAdapter != null) && (mTracksRecyclerAdapter.setProgressPercentage(percentage, albumPercentage)))
        {
            // redraw row
            //mTracksRecyclerAdapter.notifyItemChanged(trackNo);
            RecyclerView.ViewHolder vh = mTracksRecyclerView.findViewHolderForAdapterPosition(trackNo);
            if (vh != null)
            {
                if (vh instanceof TracksRecyclerAdapter.ViewHolder)
                {
                    TracksRecyclerAdapter.ViewHolder mvh = (TracksRecyclerAdapter.ViewHolder) vh;
                    ProgressBar progressView = mvh.progressView;
                    if (progressView != null)
                    {
                        progressView.setProgress(percentage);
                        progressView.setSecondaryProgress(albumPercentage);
                    }
                }
            }
        }
    }


    /**********************************************************************************************
     *
     * update the progress circle
     *
     *********************************************************************************************/
    public void updateProgress()
    {
        AudioTrack currTrack = getTrackFromIndex(mCurrTrackNo);
        if ((mMediaPlayService != null) && (currTrack != null))
        {
            int position = mMediaPlayService.getPosition();
            int album_percentage = -1;

            if (mbShowAlbumProgress)
            {
                // get album progress
                long album_position = 0;
                long album_duration = 0;
                for (int i = 0; i < mCurrTrackNo; i++)
                {
                    AudioTrack theTrack = getTrackFromIndex(i);
                    if ((theTrack != null) && (theTrack.duration > 0))
                    {
                        album_position += theTrack.duration;
                        album_duration += theTrack.duration;
                    }
                }
                for (int i = mCurrTrackNo; i < AppGlobals.sCurrAlbumTrackList.size(); i++)
                {
                    AudioTrack theTrack = getTrackFromIndex(i);
                    if ((theTrack != null) && (theTrack.duration > 0))
                    {
                        album_duration += theTrack.duration;
                    }
                }

                if (album_duration > 0)
                {
                    album_position += position;
                    if (album_position > album_duration)
                    {
                        album_percentage = 100;
                    }
                    else
                    {
                        album_percentage = (int) ((100 * album_position) / album_duration);
                    }
                }
            }

            int percentage = 0;
            int duration = (int) currTrack.duration;
            if (duration > 0)
            {
                if (position > duration)
                {
                    percentage = 100;
                } else
                {
                    percentage = (100 * position) / duration;
                }
            }

            //PrivateLog.d(LOG_TAG, "play progress = " + percentage + "/" + album_percentage);
            updateProgressView(mCurrTrackNo, percentage, album_percentage);
        }
    }


    /**************************************************************************
     *
     * update list with selection colour etc
     *
     *************************************************************************/
    private void updateList(int newTrackNo)
    {
        if (mCurrTrackNo != newTrackNo)
        {
            if (mTracksRecyclerAdapter != null)
            {
                // change selected row
                mTracksRecyclerAdapter.setCurrTrack(newTrackNo);
                // redraw new and previous selected row
                mTracksRecyclerAdapter.notifyItemChanged(mCurrTrackNo);
                mTracksRecyclerAdapter.notifyItemChanged(newTrackNo);

                boolean bIsCompletelyVisible = isListItemVisible(newTrackNo);
                if (!bIsCompletelyVisible)
                {
                    if ((mTracksRecyclerView != null) && (newTrackNo >= 0))
                    {
                        if (newTrackNo > 2)
                        {
                            // necessary hack, because scrollTo() is buggy with expanded bar
                            collapseToolbar();
                        }
                        mTracksRecyclerView.scrollToPosition(newTrackNo);
                    }
                }
            }/*
            else
            if (mTracksAdapter != null)
            {
                mTracksAdapter.setCurrTrack(newTrackNo);
                // prev row back to normal
                setBgColourForPos(mCurrTrackNo, UserSettings.getThemedColourFromResId(this, R.attr.track_list_background_normal));
                // new row to selected
                setBgColourForPos(newTrackNo, UserSettings.getThemedColourFromResId(this, R.attr.track_list_background_selected));
                if ((mTracksView != null) && (newTrackNo >= 0))
                {
                    mTracksView.smoothScrollToPosition(newTrackNo);
                }
            }*/
        }

        mCurrTrackNo = newTrackNo;
    }


    /**************************************************************************
     *
     * update list and player controls
     *
     *************************************************************************/
    private void updateControllerAndList()
    {
        //PrivateLog.d(LOG_TAG, "updateControllerAndList()");
        if (mMediaPlayService != null)
        {
            int newTrackNo = mMediaPlayService.getAudioTrackArrayNumber();
            if ((mCurrTrackNo >= 0) && (newTrackNo < 0) && (mMediaController != null))
            {
                // hide the controller overlay view
                mMediaController.reallyHide();
            }
            if (mCurrTrackNo != newTrackNo)
            {
                updateList(newTrackNo);
            }
        }
    }

    // for skip forward and backward
    public void gotoTrack(int newTrackNo, long pos)
    {
        PrivateLog.d(LOG_TAG, "gotoTrack(" + newTrackNo + ") -- mCurrTrackNo == " + mCurrTrackNo);
        if ((mCurrTrackNo >= 0) && (newTrackNo < 0))
        {
            if (mMediaController != null)
            {
                // hide the controller overlay view
                mMediaController.reallyHide();
            }
            if (mMediaPlayService != null)
            {
                mMediaPlayService.stopPlayer();
            }
        }
        updateList(newTrackNo);

        // now mCurrTrackNo has been updated with newTrackNo
        if (mCurrTrackNo >= 0)
        {
            mBufferedPlayState = true;
            PrivateLog.d(LOG_TAG, "gotoTrack() : buffered_play_state set to true");
            if (mMediaPlayService != null)
            {
                mMediaPlayService.playAudioTrack(mCurrTrackNo, pos);
            }
            if ((mMediaController != null) && (!mMediaController.isShowing()))
            {
                mMediaController.show(mAutohideModeMs);
            }
        }
    }

    // for skip forward (deltaTrack = 1) and backward (deltaTrack = -1)
    public void gotoTrackRel(int deltaTrack)
    {
        mBufferedPosition = 0;
        mBufferedDuration = 0;
        int newTrackNo;
        if (mMediaPlayService != null)
        {
            newTrackNo = mMediaPlayService.getNextTrackArrayNumber(deltaTrack, false /* do not wrap */);
        }
        else
        {
            newTrackNo = -1;
        }
        gotoTrack(newTrackNo, 0);
    }

    // called when BACK key was pressed for STOP
    public void stopPlaying()
    {
        mBufferedPlayState = false;
        if (mMediaPlayService != null)
        {
            saveStateForBookmark();     // also update buffered position
            mMediaPlayService.stopPlayer();
        }
        if (mMediaController != null)
        {
            mMediaController.reallyHide();
        }

        //stopService(mPlayIntent); ???
        updateList(-1); // new active track number is invalid
        updateFloatingButton();
    }


    /* *************************************************************************
     *
     * callback for custom ListView that reports clicks to empty space
     *
     *************************************************************************/
    /*
    @Override
    public void onNoItemClicked()
    {
        PrivateLog.d(LOG_TAG, "onNoItemClicked()");
        if (mAutohideModeMs != 0)
        {
            if (mCurrTrackNo >= 0)
            {
                // currently playing
                if (mMediaController != null)
                {
                    mMediaController.show(mAutohideModeMs);
                }
            }
        }
    }
    */


    /* *************************************************************************
     *
     * callback for user track select
     *
     * -> "activity_tracks.xml"
     *
     * note that this will never be called once we call
     * setMovementMethod() for the album title view
     *
     *************************************************************************/
    /*
    public void onSpaceClicked(View view)
    {
        PrivateLog.d(LOG_TAG, "onSpaceClicked()");
        if (mAutohideModeMs != 0)
        {
            if (mCurrTrackNo >= 0)
            {
                // currently playing
                if (mMediaController != null)
                {
                    mMediaController.show(mAutohideModeMs);
                }
            }
        }
    }
    */


    /* *************************************************************************
     *
     * callback for user album image select
     *
     * -> "activity_tracks.xml"
     *
     *************************************************************************/
    /*
    public void onAlbumImageClicked(View v)
    {
        PrivateLog.d(LOG_TAG, "onAlbumImageClicked()");

//        TextView textAlbumTitle = (TextView) findViewById(R.id.tracks_album_title);
//        PrivateLog.d(LOG_TAG, "onStart() : line count = " + textAlbumTitle.getLineCount());

        RelativeLayout albumHeader = findViewById(R.id.tracks_album_header);
        TextView albumTitle = findViewById(R.id.tracks_album_title);
        ImageView albumImage = findViewById(R.id.tracks_album_image);
        if (headerClicked == 0)
        {
            // shrink
            origHeaderHeight = changeViewGroupHeightAbsOrRelative(albumHeader, -1, -40);
            origTitleHeight = changeViewHeightAbsOrRelative(albumTitle, -1, -40);
            origImageWidth = changeViewWidthAbsOrRelative(albumImage, -1, -40);
            headerClicked++;
        }
        else
        if (headerClicked == 3)
        {
            // sizes back to normal
            changeViewGroupHeightAbsOrRelative(albumHeader, origHeaderHeight, 0);
            changeViewHeightAbsOrRelative(albumTitle, origTitleHeight, 0);
            changeViewWidthAbsOrRelative(albumImage, origImageWidth, 0);
            headerClicked = 0;
        }
        else
        if (headerClicked == 2)
        {
            // shrink
            changeViewGroupHeightAbsOrRelative(albumHeader, -1, -30);
            changeViewHeightAbsOrRelative(albumTitle, -1, -30);
            changeViewWidthAbsOrRelative(albumImage, -1, -30);
            headerClicked++;
        }
        else
        {
            // shrink
            changeViewGroupHeightAbsOrRelative(albumHeader, -1, -40);
            changeViewHeightAbsOrRelative(albumTitle, -1, -40);
            changeViewWidthAbsOrRelative(albumImage, -1, -40);
            headerClicked++;
        }
    }
    */


    /**************************************************************************
     *
     * track was clicked or the floating play button was clicked
     *
     *************************************************************************/
    private void playTrack(int newTrackNo, long pos)
    {
        PrivateLog.d(LOG_TAG, "playTrack(" + newTrackNo + ", pos=" + pos + ")");
        long noOfTracks = (AppGlobals.sCurrAlbum == null) ? 0 : AppGlobals.sCurrAlbum.album_no_of_tracks;
        boolean trackValid = (newTrackNo >= 0) && (newTrackNo < noOfTracks);

        if (trackValid && ((mCurrTrackNo < 0) || (mAutohideModeMs == 0)))
        {
            // if no track is being played and we chose one, just play it
            // if a track is being played, and the controller is visible, and we chose a new track, play it
            gotoTrack(newTrackNo, pos);
        }
        else
        if (mAutohideModeMs != 0)
        {
            // media controller is automatically hidden after some time

            if (mCurrTrackNo >= 0)
            {
                // currently playing
                if (mMediaController != null)
                {
                    mMediaController.show(mAutohideModeMs);
                }
            }
        }

        updateFloatingButton();
    }


    /**************************************************************************
     *
     * Start Classical Music Tagger with the provided path list
     *
     *************************************************************************/
    private void startTagger(final ArrayList<String> audioPathList)
    {
        final String packageName = "de.kromke.andreas.musictagger";

        if (!audioPathList.isEmpty())
        {
            Intent theIntent = getPackageManager().getLaunchIntentForPackage(packageName);
            if (theIntent != null)
            {
                theIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                theIntent.putExtra("pathTable", audioPathList);
                startActivity(theIntent);
            } else
            {
                //
                // tagger or scanner is not installed, ask if to proceed to play store
                //

                MainActivity.dialogOpenPlayStore(this, packageName);
            }
        }
    }


    /**************************************************************************
     *
     * Show alert asking for tagger to start
     *
     * path == null: handle all tracks
     *
     *************************************************************************/
    private void dialogStartTagger(final String path)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppDialogTheme);
        builder.setTitle(R.string.str_edit_metadata);
        builder.setMessage(R.string.str_start_classical_music_tagger);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                ArrayList<String> audioPathList = new ArrayList<>();
                if (path != null)
                {
                    audioPathList.add(path);
                }
                else
                {
                    for (AudioTrack track:AppGlobals.sCurrAlbumTrackList)
                    {
                        final String path = track.path;
                        if (path != null)
                        {
                            audioPathList.add(path);
                        }
                    }
                }
                startTagger(audioPathList);
                mPendingDialog = null;
            }
        });
        builder.setNegativeButton(R.string.str_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                Toast.makeText(getApplicationContext(), R.string.str_no_tagger, Toast.LENGTH_LONG).show();
                mPendingDialog = null;
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * Show alert asking for using a bookmark
     *
     *************************************************************************/
    private void dialogUseBookmark()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppDialogTheme);
        builder.setTitle(R.string.str_execute_bookmark);
        String msg = getString(R.string.str_continue_at_bookmark_explanation);
        builder.setMessage(msg + " " + getBookmarkInfo() + ".");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                PrivateLog.d(LOG_TAG, "OK");
                // repeat the last seconds when continuing the track
                long pos = Math.max(mPendingBookmarkPos - bookmarkRepeatMs, 0);
                //mBufferedPosition = (int) pos;
                playTrack(mPendingBookmarkTrackNo, pos);
                mPendingBookmarkTrackNo = -1;
                mPendingDialog = null;
            }
        });
        builder.setNegativeButton(R.string.str_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                PrivateLog.d(LOG_TAG, "Cancel");
                mPendingBookmarkTrackNo = -1;
                mPendingDialog = null;
                playTrack(0, 0);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /* *************************************************************************
     *
     * Listview callback: user list item clicked
     *
     *************************************************************************/
    /*
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        onTrackClicked(position);
    }
    */


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    public int getPositionFromView(View view)
    {
        Object theTag = view.getTag();
        if (theTag == null)
        {
            PrivateLog.e(LOG_TAG, "onTrackClicked() : no tag");
            return -1;
        }
        String theTagString = theTag.toString();
        int position;
        try
        {
            position = Integer.parseInt(theTagString);
        }
        catch(NumberFormatException e)
        {
            position = 0;
        }
        return position;
    }


    /**************************************************************************
     *
     * callback for user track select
     *
     * -> "audio_track.xml"
     *
     *************************************************************************/
    public void onTrackClicked(View view)
    {
        int position = getPositionFromView(view);
        playTrack(position, 0);
    }


    /* *************************************************************************
     *
     * Listview callback: user list item long clicked
     *
     *************************************************************************/
    /*
    @Override
    public boolean onItemLongClick(AdapterView<?> l, View v,
                                   final int position, long id)
    {
        PrivateLog.d(LOG_TAG, "onItemLongClick(pos = " + position + ", id = " + id + ")");
        onItemLongClick(position);
        return true;
    }
    */


    /**************************************************************************
     *
     * callback for header click
     *
     * -> "activity_tracks_collapsing.xml"
     *
     *************************************************************************/
    public void onHeaderImageClicked(View view)
    {
        collapseToolbar();
    }


    /**************************************************************************
     *
     * callback for header click
     *
     * -> "activity_tracks_collapsing.xml"
     *
     *************************************************************************/
    public void onHeaderTextClicked(View view)
    {
        collapseToolbar();
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void onHeaderImageLongClick()
    {
        PrivateLog.d(LOG_TAG, "onHeaderImageLongClick()");
        if (!AppGlobals.sCurrAlbumTrackList.isEmpty())
        {
            dialogStartTagger(null);
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void onHeaderTextLongClick()
    {
        PrivateLog.d(LOG_TAG, "onHeaderTextLongClick()");
        if (!AppGlobals.sCurrAlbumTrackList.isEmpty())
        {
            dialogStartTagger(null);
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private AudioTrack getTrackFromIndex(int index)
    {
        if ((AppGlobals.sCurrAlbumTrackList != null) && (index >= 0) && (index < AppGlobals.sCurrAlbumTrackList.size()))
        {
            return AppGlobals.sCurrAlbumTrackList.get(index);
        }
        else
        {
            return null;
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void onItemLongClick(final int position)
    {
        PrivateLog.d(LOG_TAG, "onItemLongClick(pos = " + position + ")");
        AudioTrack theTrack = getTrackFromIndex(position);
        if (theTrack != null)
        {
            if (theTrack.path != null)
            {
                dialogStartTagger(theTrack.path);
            }
        }
    }


    /**************************************************************************
     *
     * dummy callbacks to avoid this Android bug:
     *
     * https://stackoverflow.com/questions/44073861/play-store-crash-report-illegalstateexception-on-android-view-viewdeclaredoncl/46435279#46435279
     *
     *************************************************************************/
    public void onAlbumClicked(View view)
    {
        PrivateLog.e(LOG_TAG, "onAlbumClicked() -- misdirected callback (Android bug)");
    }
    @SuppressWarnings({"unused", "RedundantSuppression"})
    public void onClick(View v)
    {
        PrivateLog.e(LOG_TAG, "onClick() -- misdirected callback (Android bug)");
    }


    /************************************************************************************
     *
     * compose header text
     *
     ***********************************************************************************/
    private String getHeaderHtmlText(AudioAlbum theAlbum)
    {
        int no_of_tracks;
        final String common_composer;
        final String album_name;
        boolean bHasGroups;
        String albumPerformerText;
        long album_duration;

        if (theAlbum != null)
        {
            no_of_tracks = (int) theAlbum.album_no_of_tracks;
            common_composer = theAlbum.getComposer();
            bHasGroups = theAlbum.bHasGroups;
            album_name = theAlbum.album_name;
            albumPerformerText = theAlbum.getArtistAndYear();
            album_duration = theAlbum.album_duration;
        }
        else
        {
            no_of_tracks = 0;
            common_composer = "";
            bHasGroups = false;
            album_name = "";
            albumPerformerText = "";
            album_duration = 0;
        }

        boolean showAlbumDuration = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_DURATION, false);

        // compose composer and performer colour string, make sure to remove alpha value
        int colour = UserSettings.getThemedColourFromResId(this, R.attr.track_list_header_text2_colour);
        String colourString = "<font color='#" + String.format("%X", colour).substring(2) + "'>";

        if (no_of_tracks != AppGlobals.getAudioAlbumTrackListSize())
        {
            PrivateLog.w(LOG_TAG, "onCreate() : number of album tracks wrong for album" + AppGlobals.sCurrAlbum.album_name);
            if (theAlbum != null)
            {
                theAlbum.album_no_of_tracks = AppGlobals.getAudioAlbumTrackListSize();
            }
        }

        //
        // constants
        //

        final String composerStyle1 = "<small>" + colourString;
        final String composerStyle2 = "</font></small>";
        final String performerStyle1 = "<small>" + colourString;
        final String performerStyle2 = "</font></small>";
        final String trackStyle1 = "<small>";
        final String trackStyle2 = "</small>";

        //
        // composer is either empty or a small line with ":" at top
        //

        String albumComposerText;
        if ((!bHasGroups) && (common_composer != null) && !common_composer.isEmpty())
        {
            albumComposerText = composerStyle1 + common_composer + ":<br>" + composerStyle2;
        }
        else
        {
            albumComposerText = "";
        }

        //
        // album title text
        //

        String albumTitleText = "<big>" + album_name + "</big>";

        //
        // performer is either empty or a small line
        //

        if (!albumPerformerText.isEmpty())
        {
            albumPerformerText = performerStyle1 + albumPerformerText + performerStyle2;
        }

        //
        // track and duration information
        //

        String trackText = (no_of_tracks == 1) ? getString(R.string.str_track) : getString(R.string.str_tracks);
        trackText = "(" + no_of_tracks + " " + trackText;
        if ((showAlbumDuration) && (album_duration != 0))
        {
            trackText += ", " + convertMsToHMmSs(theAlbum.album_duration);
        }
        trackText += ")";
        trackText = trackStyle1 + trackText + trackStyle2;


        @SuppressWarnings("UnnecessaryLocalVariable") final String theHtmlString = "<body>" +
                albumComposerText +
                albumTitleText + "<br>" +
                albumPerformerText + "<br>" +
                trackText +
                "</body>";
        return theHtmlString;
    }


    /* ***********************************************************************************
     *
     * set icon and informational text for album header
     *
     ***********************************************************************************/
    /*
    private void initAlbumHeaderViews(AudioAlbum theAlbum, Drawable theAlbumImageDrawable)
    {
        //
        // Set size and colour of album header.
        // Note that only image has fixed size, and header and text are arranged and sized automatically
        //

        // set image size, treating 120 as standard size and 0 as minimum
        int imageAlbumSize = UserSettings.getVal(UserSettings.PREF_DELTA_SIZE_OF_ALBUM_HEADER, 120);
        ViewGroup.LayoutParams params;
        ImageView imageAlbumView = findViewById(R.id.tracks_album_image);
        params = imageAlbumView.getLayoutParams();
        if ((params.width > 150) && (params.height > 150))
        {
            params.width += imageAlbumSize - 120;
            params.height += imageAlbumSize - 120;
            imageAlbumView.setLayoutParams(params);
        }
        if (theAlbumImageDrawable != null)
        {
            imageAlbumView.setImageDrawable(theAlbumImageDrawable);
        }

        //
        // set text
        //

        final String htmlText = getHeaderHtmlText(theAlbum);
        TextView textAlbumView = findViewById(R.id.tracks_album_title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            textAlbumView.setText(Html.fromHtml(htmlText, android.text.Html.FROM_HTML_MODE_LEGACY));
        } else
        {
            textAlbumView.setText(Html.fromHtml(htmlText));
        }
        PrivateLog.d(LOG_TAG, "onCreate() : line count = " + textAlbumView.getLineCount());
    }
    */


    /* ***********************************************************************************
     *
     * init user interface (classic)
     *
     ***********************************************************************************/
    /*
    private void initUi()
    {
        setContentView(R.layout.activity_tracks);

        initAlbumHeaderViews(AppGlobals.currAlbum, AppGlobals.currAlbumImageDrawable);

        //
        // get user settings
        //

        boolean showTrackNumbers = UserSettings.getBool(UserSettings.PREF_SHOW_TRACK_NUMBER, false);
        int showConductorMode = UserSettings.getVal(UserSettings.PREF_SHOW_CONDUCTOR, 0);
        boolean showSubtitle = UserSettings.getBool(UserSettings.PREF_SHOW_SUBTITLE, false);
        AppGlobals.genderismMode = UserSettings.getVal(UserSettings.PREF_GENDERISM_NONSENSE, 0);

        //retrieve list view
        mTracksView = findViewById(R.id.audioTrack_list);
        // special handling for a custom ListView that passes clicks to non-elements
        mTracksView.setOnNoItemClickListener(this);
        mTracksView.setOnItemLongClickListener(this);
        mTracksView.setOnItemClickListener(this);

        //create and set adapter
        mTracksAdapter = new TracksAdapter(
                this,
                AppGlobals.currAlbumTrackList,
                mAutohideModeMs == 0,
                showTrackNumbers,
                showConductorMode, showSubtitle);
        mTracksAdapter.setCurrTrack(mCurrTrackNo);      // currTrackNo is static, preserved during destroy/create
        if (mMediaControllerHeight > 0)
        {
            mTracksAdapter.setDummyTrackHeight(mMediaControllerHeight);
        }
        mTracksView.setAdapter(mTracksAdapter);
    }
    */


    /**************************************************************************
     *
     * init user interface (collapsing variant)
     *
     *************************************************************************/
    @SuppressLint("ClickableViewAccessibility")
    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    private void initUiCollapsing()
    {
        setContentView(R.layout.activity_tracks_collapsing);

        //
        // header image
        //

        // set image size, treating 150 as standard size and 0 as minimum
        int imageAlbumDeltaSize = UserSettings.getVal(UserSettings.PREF_DELTA_SIZE_OF_ALBUM_HEADER, 0);
        ImageView imageAlbumView = findViewById(R.id.header_image);

        if (!AppGlobals.sbAlbumHeaderPictureSizeKnown)
        {
            // hack to pass the real image size in pixels to the thumbnail loader
            ViewGroup.LayoutParams params;
            params = imageAlbumView.getLayoutParams();
            //PrivateLog.d(LOG_TAG, "initUiCollapsing() : header image size is " + params.width + " x " + params.height);
            AppGlobals.sAlbumHeaderPictureSize = params.width + imageAlbumDeltaSize;
            AppGlobals.sbAlbumHeaderPictureSizeKnown = true;
        }

        if (imageAlbumDeltaSize != 0)
        {
            ViewGroup.LayoutParams params;
            params = imageAlbumView.getLayoutParams();
            params.width += imageAlbumDeltaSize;
            params.height += imageAlbumDeltaSize;
            imageAlbumView.setLayoutParams(params);
        }
        if (AppGlobals.sCurrAlbumImageDrawable != null)
        {
            imageAlbumView.setImageDrawable(AppGlobals.sCurrAlbumImageDrawable);
        }
        imageAlbumView.setOnLongClickListener(
            new View.OnLongClickListener()
            {
                public boolean onLongClick (View v)
                {
                    onHeaderImageLongClick();
                    return true;
                }
            });

        //
        // header text
        //

        final String htmlText = getHeaderHtmlText(AppGlobals.sCurrAlbum);
        TextView textAlbumView = findViewById(R.id.header_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            textAlbumView.setText(Html.fromHtml(htmlText, android.text.Html.FROM_HTML_MODE_LEGACY));
        } else
        {
            textAlbumView.setText(Html.fromHtml(htmlText));
        }
        textAlbumView.setOnLongClickListener(
            new View.OnLongClickListener()
            {
                public boolean onLongClick (View v)
                {
                    onHeaderTextLongClick();
                    return true;
                }
            });

        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        String albumName = (AppGlobals.sCurrAlbum != null) ? AppGlobals.sCurrAlbum.album_name : "UNKNOWN";
        collapsingToolbar.setTitle(albumName);

        mTracksRecyclerView = findViewById(R.id.recycler);
        mTracksRecyclerView.setHasFixedSize(true);     // ???
        LinearLayoutManager l = new LinearLayoutManager(this);
        mTracksRecyclerView.setLayoutManager(l);

        // trying to set divider lines
        // note that divider drawable will automatically be a PNG for Android 4.4, otherwise: crash
        DividerItemDecoration divider = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        /*
        if (Build.VERSION.SDK_INT > 19)
        {
            // this line is not necessary if the theme contains an entry for: item name="android:listDivider"
            divider.setDrawable(getResources().getDrawable(R.drawable.divider));
        }
        else
        {
            // Android 4.4 will crash with xml drawable
            PrivateLog.w(LOG_TAG, "initUiCollapsing() -- Android 4.4: use PNG for divider");
            divider.setDrawable(getResources().getDrawable(R.drawable.divider_f0f0f0));
        }
        */

        mTracksRecyclerView.addItemDecoration(divider);

        // get settings
        boolean showTrackNumbers = UserSettings.getBool(UserSettings.PREF_SHOW_TRACK_NUMBER, false);
        int showConductorMode = UserSettings.getVal(UserSettings.PREF_SHOW_CONDUCTOR, 0);
        boolean showSubtitle = UserSettings.getBool(UserSettings.PREF_SHOW_SUBTITLE, false);
        AppGlobals.sGenderismMode = UserSettings.getVal(UserSettings.PREF_GENDERISM_NONSENSE, 0);

        //create and set adapter
        mTracksRecyclerAdapter = new TracksRecyclerAdapter(
                this,
                AppGlobals.sCurrAlbumTrackList,
                mAutohideModeMs == 0,
                showTrackNumbers,
                showConductorMode, showSubtitle);
        mTracksRecyclerAdapter.setOnLongClickListener(new TracksRecyclerAdapter.OnLongClickListener()
        {
            @Override
            public void onLongClick(View view)
            {
                int position = getPositionFromView(view);
                onItemLongClick(position);
            }
        });
        mTracksRecyclerAdapter.setCurrTrack(mCurrTrackNo);      // currTrackNo is static, preserved during destroy/create
        if (mMediaControllerHeight > 0)
        {
            mTracksRecyclerAdapter.setDummyTrackHeight(mMediaControllerHeight);
        }
        mTracksRecyclerView.setAdapter(mTracksRecyclerAdapter);

        // add touch listener to adapter to detect clicks outside the items
        mTracksRecyclerView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN
                        && mTracksRecyclerView.findChildViewUnder(event.getX(), event.getY()) == null)
                {
                    if (mAutohideModeMs != 0)
                    {
                        if (mCurrTrackNo >= 0)
                        {
                            // currently playing
                            if (mMediaController != null)
                            {
                                mMediaController.show(mAutohideModeMs);
                            }
                        }
                    }
                }
                return false;
            }
        });

        //
        // floating button
        //

        mFloatingButton = findViewById(R.id.play_button);
        mFloatingButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (audioIsPlaying())
                {
                    pause();
                    updateMediaController();
                }
                else
                if (audioIsPaused())
                {
                    start();
                    updateMediaController();
                }
                else
                {
                    if (mPendingBookmarkTrackNo >= 0)
                    {
                        dialogUseBookmark();
                    }
                    else
                    {
                        playTrack(0, 0);
                    }
                }
            }
        }
        );

        mFloatingButton.setOnLongClickListener(new View.OnLongClickListener()
        {
           @Override
           public boolean onLongClick(View view)
           {
               // brute stop without remembering position
               stopPlaying();
               mSavedTrackNo = -1;
               mSavedTrackPosition = 0;
               updateMediaController();
               return true;
           }
        }
        );
    }


    /**************************************************************************
     *
     * set icon and colour of floating button #1 according to
     * mFloatingButton1Action
     *
     *************************************************************************/
    private void updateFloatingButton()
    {
        //PrivateLog.d(LOG_TAG, "updateFloatingButton()");
        boolean shallBePlaying = audioIsPlaying();

        if ((shallBePlaying != mbFloatingButtonIsPlaying) && (mFloatingButton != null))
        {
            // BUG: https://stackoverflow.com/questions/49587945/setimageresourceint-doesnt-work-after-setbackgroundtintcolorlistcolorstateli
            mFloatingButton.hide();
            if (shallBePlaying)
            {
                //PrivateLog.d(LOG_TAG, "updateFloatingButton() -- PAUSE icon");
                mFloatingButton.setImageResource(android.R.drawable.ic_media_pause);
                mFloatingButton.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.action_button_pause_colour));
            } else
            {
                //PrivateLog.d(LOG_TAG, "updateFloatingButton() -- PLAY icon");
                mFloatingButton.setImageResource(android.R.drawable.ic_media_play);
                mFloatingButton.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.action_button_play_colour));
            }
            mFloatingButton.show();

            mbFloatingButtonIsPlaying = shallBePlaying;
        }
    }


    /**************************************************************************
     *
     * update colour theme etc., if necessary
     *
     *************************************************************************/
    @SuppressWarnings("UnusedReturnValue")
    private boolean updateSettings()
    {
        int colourTheme = getVal(PREF_THEME, 0);
        if ((mColourTheme < 0) || (mColourTheme != colourTheme))
        {
            mColourTheme = colourTheme;
            switch (mColourTheme)
            {
                //noinspection DefaultNotLastCaseInSwitch
                default:
                case 0:
                    setTheme(R.style.TracksOfAlbumTheme_BlueLight);
                    break;
                case 1:
                    setTheme(R.style.TracksOfAlbumTheme_OrangeLight);
                    break;
                case 2:
                    setTheme(R.style.TracksOfAlbumTheme_GreenLight);
                    break;
                case 3:
                    setTheme(R.style.TracksOfAlbumTheme_Blue);
                    break;
                case 4:
                    setTheme(R.style.TracksOfAlbumTheme_Gray);
                    break;
            }

            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                Window window = getWindow();
                window.setStatusBarColor(UserSettings.getThemedColourFromResId(this, R.attr.colorPrimaryDark));
                window.setNavigationBarColor(UserSettings.getThemedColourFromResId(this, R.attr.colourNavigationBarPlaying));
            }
            return true;
        }
        return false;
    }


    /**************************************************************************
     *
     * bookmark for this album, also update buffered position
     *
     *************************************************************************/
    private void rememberStateForBookmark()
    {
        // remember state in order to be able to continue later
        if ((AppGlobals.sCurrAlbum != null) && (mCurrTrackNo >= 0) && (!isCurrAlbumPseudo()))
        {
            if ((mMediaPlayService != null) && mMediaPlayService.isPlaying())
            {
                mBufferedPosition = mMediaPlayService.getPosition();
            }

            // Note: alternatively we could store path instead of album/title
            mSavedTrackNo = mCurrTrackNo;
            mSavedTrackPosition = mBufferedPosition;
        }
        else
        {
            mSavedTrackNo = -1;
            mSavedTrackPosition = 0;
        }
    }


    /**************************************************************************
     *
     * save bookmark for this album
     *
     *************************************************************************/
    private void saveStateForBookmark()
    {
        if (!isCurrAlbumPseudo())
        {
            rememberStateForBookmark();     // also update buffered position
            // remember state in order to be able to continue later
            PrivateLog.d(LOG_TAG, "saveStateForBookmark() : album=" + getCurrAlbumName() + "/track=" + mSavedTrackNo + "/pos=" + mSavedTrackPosition);
            UserSettings.putBookmark(getCurrAlbumName(), mSavedTrackNo, mSavedTrackPosition);
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    static String getCurrAlbumName()
    {
        return (AppGlobals.sCurrAlbum != null) ? AppGlobals.sCurrAlbum.album_name : "UNKNOWN";
    }


    /**************************************************************************
     *
     * Pseudo albums are temporarily created for "share" and "open with"
     * handling. They do not have album names or bookmarks.
     *
     *************************************************************************/
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    static boolean isCurrAlbumPseudo()
    {
        return (AppGlobals.sCurrAlbumId < 0);
    }


    /**************************************************************************
     *
     * timer task for progress animation
     *
     * note that the timer is disabled while the activity is not visible
     *
     *************************************************************************/
    class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(TracksOfAlbumActivity.this::TimerCallback);
        }
    }
}
