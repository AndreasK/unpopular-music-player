/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
/*
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.CollapsingToolbarLayout;
import java.util.ArrayList;
import android.view.MenuInflater;
*/
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowMetrics;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.GridView;
import android.view.View;
import android.content.Intent;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

import static android.os.Environment.getExternalStorageState;
import static android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;
import static de.kromke.andreas.unpopmusicplayerfree.UserSettings.PREF_THEME;
import static de.kromke.andreas.unpopmusicplayerfree.UserSettings.getVal;
import static de.kromke.andreas.utilities.UriToPath.getPathFromIntentUri;


/**
 * The album list, which is also the initial activity
 * @noinspection JavadocBlankLines, RedundantSuppression, JavadocLinkAsPlainText
 */

@SuppressWarnings({"Convert2Lambda", "SpellCheckingInspection", "CommentedOutCode"})
public class MainActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "UMPL : MainAct";
    public static final String sDbPath = "ClassicalMusicDb";
    public static final String sDbName = "musicmetadata.db";
    public static final String sDbSafName = "safmetadata.db";
    private static final String STATE_CURR_ALBUM_ID = "currAlbumId";
    public static final String EXTRA_MESSAGE_ALBUM_ID = "com.kromke.unpopmusicplayerfree.ALBUM";
    private static final int MY_PERMISSIONS_REQUEST = 11;
    //protected static final int REQUEST_DIRECTORY_SELECT = 5;
    //protected static final int REQUEST_FILE_SELECT = 6;
    //private static final int RESULT_SETTINGS = 1;
    private static final String sIntentKeyDatabasePath = "databasePath";
    private static boolean sbReadMediaFileAccessGranted = false;
    private int mColourTheme = -1;
    private boolean mbUseOwnDatabase;
    private String mOwnDatabasePath;
    private static String sSafDatabasePath = null;     // if set, use this one and not the shared one
    private boolean mbShowAlbumDuration;
    private int mAlbumImageSize;
    private int mAlbumImageGridSize;
    private AbsListView m_audioAlbumView;
    //private RecyclerView m_audioAlbumRecyclerView;
    private int m_currAlbumNo;
    private long m_currAlbumId;
    AudioAlbumAdapter m_audioAlbumAdt;
    private MenuItem mMenuItemGridView;
    private MenuItem mMenuItemManageFiles;
    //AudioAlbumRecyclerAdapter m_audioAlbumRecyclerAdt;
    //static boolean mbUseCollapsingLayout = false;

    ActivityResultLauncher<Intent> mPreferencesActivityLauncher;
    ActivityResultLauncher<Intent> mRequestDirectorySelectActivityLauncher;
    boolean mbDirectorySelectForSaf = false;    // otherwise: path
    ActivityResultLauncher<Intent> mRequestFileSelectActivityLauncher;
    ActivityResultLauncher<Intent> mStorageAccessPermissionActivityLauncher;


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(LOG_TAG, "onCreate()");
        super.onCreate(savedInstanceState);

        // registering for callback must be done here, not later
        registerPreferencesCallback();
        registerDirectorySelectCallback();
        registerFileSelectCallback();
        registerStorageAccessPermissionCallback();

        // restore state
        if (savedInstanceState != null)
        {
            m_currAlbumId = savedInstanceState.getLong(STATE_CURR_ALBUM_ID);
            Log.d(LOG_TAG, "onCreate() : restore album id " + m_currAlbumId);
        }
        else
        {
            m_currAlbumId = -1;
        }

        Intent intent = getIntent();
        if (intent != null)
        {
            sSafDatabasePath = intent.getStringExtra(sIntentKeyDatabasePath);
            Log.d(LOG_TAG, "onCreate() : restore SAF db path " + sSafDatabasePath);
        }

        // all needed preference settings
        UserSettings.init(this);
        writeDefaultSettings();
        updateSettings();

        AudioAlbum.SetContext(this);

        // We need the old (pre-Android-11) file read permissions at once, otherwise
        // we cannot run on older Android versions. Later we may ask for full
        // file access on Android 11 and above.
        // Note that full file access includes media file access, so that we
        // may skip the request.

        boolean bManageAllFiles = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            bManageAllFiles = Environment.isExternalStorageManager();
        }
        if (bManageAllFiles)
        {
            sbReadMediaFileAccessGranted = true;
            onPermissionResult(true);
        }
        else
        {
            requestForPermissions();
        }

        // make sure Volume up/down keys change music volume, not ringer volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // Check if app has been updated. If yes, show list of changes.
        // If preference value does not exist, yet, also show the dialogue

        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);
        int newVersionCode = info.versionCode;
        int oldVersionCode = UserSettings.updateVal(UserSettings.PREF_INSTALLED_APP_VERSION, newVersionCode, -1);
        if (oldVersionCode != newVersionCode)
        {
            DialogChanges();
        }
    }


    /* *************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    /*
    @Override
    protected void onResume()
    {
        Log.d(LOG_TAG, "onResume()");
        super.onResume();
    }
    */


    /* *************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    /*
    @Override
    protected void onPause()
    {
        Log.d(LOG_TAG, "onPause()");
        super.onPause();
    }
    */


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState)
    {
        Log.d(LOG_TAG, "onSaveInstanceState()");
        if (m_currAlbumId != -1)
        {
            savedInstanceState.putLong(STATE_CURR_ALBUM_ID, m_currAlbumId);
            Log.d(LOG_TAG, "onSaveInstanceState() : save album id " + m_currAlbumId);
        }

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    /**************************************************************************
     *
     * permission was granted or denied, immediately or later
     *
     *************************************************************************/
    private void onPermissionResult(boolean granted)
    {
        sbReadMediaFileAccessGranted = granted;
        if (!granted)
        {
            Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
        }
        initAlbumList();
        handleIntent();
        /*
        if (mbUseCollapsingLayout)
            initUiCollapsing();
        else*/
        initUi();
    }


    /**************************************************************************
     *
     * Check if media files may be accessed
     *
     *************************************************************************/
    private boolean checkForMediaFilePermissions()
    {
        String[] requests;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            // Android 13 needs different requests than 4..12
            requests = new String[]{
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_IMAGES
            };
        }
        else
        {
            // this one is invalid in Android 13
            requests =  new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        }

        //
        // Count number of already granted permissions
        //

        int granted = 0;
        for (String request : requests)
        {
            int permissionCheck = ContextCompat.checkSelfPermission(this, request);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED)
            {
                granted++;
            }
        }

        // check if all permissions have been granted
        return (granted == requests.length);
    }


    /**************************************************************************
     *
     * Ask for read access to media files. If this is denied, the app is
     * limited to SAF mode. However, if "manage all files" is granted,
     * the read media files permission is redundant. But note that
     * "manage all files" is not allowed for the Play Store version.
     *
     * Also ask for permission to send notifications.
     *
     *************************************************************************/
    private void requestForPermissions()
    {
        String[] requests;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            // Android 13 needs different requests than 4..12
            requests = new String[]{
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_IMAGES,
                    Manifest.permission.POST_NOTIFICATIONS
            };
        }
        else
        {
            // this one is invalid in Android 13
            requests =  new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        }

        //
        // Count number of already granted permissions
        //

        int granted = 0;
        for (String request : requests)
        {
            int permissionCheck = ContextCompat.checkSelfPermission(this, request);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED)
            {
                granted++;
            }
        }

        //
        // if not all have been granted, request them all
        //

        if (granted == requests.length)
        {
            Log.d(LOG_TAG, "permission(s) immediately granted");
            onPermissionResult(true);
        } else
        {
            Log.d(LOG_TAG, "request permission(s)");
            ActivityCompat.requestPermissions(this, requests, MY_PERMISSIONS_REQUEST);
        }
    }


    /**************************************************************************
     *
     * Permission request for Android 11 (API 30)
     *
     * Note that we open the permission dialogue of the system, regardless of
     * the current permission state, so that the user can not only grant, but
     * also revoke the permission.
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.R)
    private void requestForManageAllFiles()
    {
        Intent intent = new Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        mStorageAccessPermissionActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * Activity method
     *
     * File read request granted or denied, only used for Android 10 or older
     *
     *************************************************************************/
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST:
            {
                // If request is cancelled, the result arrays are empty.
                //noinspection StatementWithEmptyBody,RedundantIfStatement
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    onPermissionResult(true);
                } else
                {
                    onPermissionResult(false);
                }
            }
        }
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * called everytime the menu opens
     * Enable and disable menu items according to state and context
     *
     *************************************************************************/
    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        mMenuItemGridView.setChecked(UserSettings.getBool(UserSettings.PREF_GRIDVIEW, false));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            mMenuItemManageFiles.setCheckable(true);
            mMenuItemManageFiles.setEnabled(true);
            //noinspection RedundantIfStatement
            if (Environment.isExternalStorageManager())
            {
                // full file access already granted
                mMenuItemManageFiles.setChecked(true);
            }
            else
            {
                mMenuItemManageFiles.setChecked(false);
            }
        }
        else
        {
            // The "manage files" permission is available for Android 10 and above
            mMenuItemManageFiles.setEnabled(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        Log.d(LOG_TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        mMenuItemGridView = menu.findItem(R.id.check_gridview);
        mMenuItemManageFiles = menu.findItem(R.id.action_manage_external_files);
        return true;
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    protected void onDestroy()
    {
        Log.d(LOG_TAG, "onDestroy()");
        super.onDestroy();
    }





    /**************************************************************************
     *
     * init and restore album and track lists
     *
     * priority:
     *  1. SAF path, if selected manually
     *  2. own database path, if configured
     *  3. Android database
     *
     *************************************************************************/
    private void initAlbumList()
    {
        mOwnDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);
        boolean bIsSafDatabase = (sSafDatabasePath != null);
        String ownDatabasePath;

        if (bIsSafDatabase)
        {
            ownDatabasePath = sSafDatabasePath;
            Log.d(LOG_TAG, "initAlbumList() -- using SAF path " + sSafDatabasePath);
        }
        else
        {
            // no SAF path, maybe use own database
            mbUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
            if (mbUseOwnDatabase && (mOwnDatabasePath != null))
            {
                Log.d(LOG_TAG, "initAlbumList() -- trying to use own database from preferences");
                ownDatabasePath = mOwnDatabasePath;
            }
            else
            {
                Log.d(LOG_TAG, "initAlbumList() -- using OS database");
                ownDatabasePath = null;
            }
        }

        boolean bUseJaudioTaglib = UserSettings.getBool(UserSettings.PREF_USE_JAUDIOTAGGER_LIB, false);
        m_currAlbumNo = AppGlobals.initAppGlobals(this, m_currAlbumId, ownDatabasePath, bIsSafDatabase, bUseJaudioTaglib);
        if (mbUseOwnDatabase && AppGlobals.sbAndroidDbIsActive)
        {
            Toast.makeText(getApplicationContext(), R.string.str_open_db_failure, Toast.LENGTH_LONG).show();
        }
    }


    /**************************************************************************
     *
     * init user interface (classic)
     *
     *************************************************************************/
    private void initUi()
    {
        Log.d(LOG_TAG, "initUi()");
        boolean bUseFastScroll = UserSettings.getBool(UserSettings.PREF_ALBUM_LIST_FAST_SCROLL, true);
        //create and set adapter
        // at least 80 pixels, at least 8 mm
        mAlbumImageSize = UserSettings.getAndPutVal(
                UserSettings.PREF_SIZE_ALBUM_LIST_ALBUM_ART,
                UserSettings.pixelsOrMillimeters(this, 80, 12));
        mAlbumImageGridSize = getVal(UserSettings.PREF_SIZE_ALBUM_GRID_ALBUM_ART, 512);
        mbShowAlbumDuration = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_DURATION, false);
        boolean useSectionIndexer = bUseFastScroll && UserSettings.getBool(UserSettings.PREF_ALBUM_LIST_SECTION_INDEXER, true);

        ActionBar ab = getSupportActionBar();
        if (ab != null)
        {
            String text;
            if (!sbReadMediaFileAccessGranted && (sSafDatabasePath == null))
            {
                text = getString(R.string.media_file_access_denied_only_saf_possible);
            }
            else
            {
                int nAlbums = (AppGlobals.sAudioAlbumList != null) ? AppGlobals.sAudioAlbumList.size() : 0;
                ab.setDisplayShowHomeEnabled(true);
                //ab.setLogo(R.mipmap.ic_launcher);     rather ugly
                text = "(" + nAlbums + " " + ((nAlbums == 1) ? getString(R.string.str_album) : getString(R.string.str_albums)) + ")";
                if (!AppGlobals.sbAndroidDbIsActive)
                {
                    if (AppGlobals.sbOwnDatabaseIsSaf)
                    {
                        text += "   --   SAF";
                    } else
                    {
                        text += "   --   DB";
                    }
                }
            }

            ab.setSubtitle(text);
            //ab.setDisplayUseLogoEnabled(true);
            ab.setBackgroundDrawable(new ColorDrawable(UserSettings.getThemedColourFromResId(this, R.attr.album_list_header_background)));
        }

        boolean bGridView = UserSettings.getBool(UserSettings.PREF_GRIDVIEW, false);
        int albumImageSize;
        if (bGridView)
        {
            setContentView(R.layout.activity_albums_grid);
            GridView audioAlbumView = findViewById(R.id.audioAlbum_grid);

            // get screen width and derive number of columns for grid view
            Rect r = getWindowRect();
            int width = r.width();
            int height = r.height();

            int dim = Math.min(width, height);    // get the smaller one of width and height
            int gridw;
            if (dim >= 2 * mAlbumImageGridSize)
            {
                gridw = mAlbumImageGridSize;
            }
            else
            {
                gridw = dim / 2;
            }

            audioAlbumView.setNumColumns(width / gridw);
            m_audioAlbumView = audioAlbumView;
            albumImageSize = mAlbumImageGridSize;
        }
        else
        {
            setContentView(R.layout.activity_albums);
            m_audioAlbumView = findViewById(R.id.audioAlbum_list);
            albumImageSize = mAlbumImageSize;
        }
        //m_audioAlbumRecyclerView = null;
        //m_audioAlbumRecyclerAdt = null;
        m_audioAlbumView.setBackgroundColor(UserSettings.getThemedColourFromResId(this, R.attr.album_list_background_normal));
        m_audioAlbumView.setFastScrollEnabled(bUseFastScroll);
        m_audioAlbumAdt = new AudioAlbumAdapter(this, AppGlobals.sAudioAlbumList, albumImageSize, mbShowAlbumDuration, useSectionIndexer, bGridView);
        m_audioAlbumView.setAdapter(m_audioAlbumAdt);
    }


    /* *************************************************************************
     *
     * init user interface (collapsing variant)
     *
     *************************************************************************/
    /*
    private void initUiCollapsing()
    {
        boolean bUseFastScroll = UserSettings.getBool(UserSettings.PREF_ALBUM_LIST_FAST_SCROLL, true);
        //create and set adapter
        // at least 80 pixels, at least 8 mm
        mAlbumImageSize = UserSettings.getAndPutVal(
                UserSettings.PREF_SIZE_ALBUM_LIST_ALBUM_ART,
                UserSettings.pixelsOrMillimeters(this, 80, 12));
        mbShowAlbumDuration = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_DURATION, false);
        boolean useSectionIndexer = bUseFastScroll && UserSettings.getBool(UserSettings.PREF_ALBUM_LIST_SECTION_INDEXER, true);
        int nAlbums = (AppGlobals.audioAlbumList != null) ? AppGlobals.audioAlbumList.size() : 0;

        // THIS SEEMS TO HAVE NO EFFECT:
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.inflateMenu(R.menu.main_menu);
        //setSupportActionBar(toolbar);

        setContentView(R.layout.activity_albums_collapsing);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.inflateMenu(R.menu.main_menu);    // calling this without setSupportActionBar() will never call onOptionsItemSelected()
        setSupportActionBar(toolbar);       // this is necessary to make the options menu work
        ActionBar ab = getSupportActionBar();
        if (ab != null)
        {
            ab.setDisplayHomeAsUpEnabled(false);       // this is the main activity, we do not want to have an arrow
            ab.setSubtitle("(" + nAlbums + " " + ((nAlbums == 1) ? getString(R.string.str_album) : getString(R.string.str_albums)) + ")");
        }

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle(getString(R.string.app_name));
//        collapsingToolbar.setSubtitle("(" + nAlbums + " " + ((nAlbums == 1) ? getString(R.string.str_album) : getString(R.string.str_albums)) + ")");

        m_audioAlbumRecyclerView = findViewById(R.id.audioAlbum_recycler);
        m_audioAlbumView = null;
        m_audioAlbumAdt = null;
        m_audioAlbumRecyclerView.setBackgroundColor(UserSettings.getThemedColourFromResId(this, R.attr.album_list_background_normal));
        m_audioAlbumRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        m_audioAlbumRecyclerView.setLayoutManager(layoutManager);
        //m_audioAlbumRecyclerView.setFastScrollEnabled(bUseFastScroll);
        bUseFastScroll = false;     // TODO: add later

        m_audioAlbumRecyclerAdt = new AudioAlbumRecyclerAdapter(this,  AppGlobals.audioAlbumList, mAlbumImageSize, mbShowAlbumDuration, useSectionIndexer);
        m_audioAlbumRecyclerView.setAdapter(m_audioAlbumRecyclerAdt);
    }
    */


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    private Rect getWindowRect()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            WindowMetrics windowMetrics = getWindowManager().getCurrentWindowMetrics();
            return windowMetrics.getBounds();
        } else
        {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // make sure that in portrait mode there are at least two columns
            return new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    public View getViewByPosition(int pos, AbsListView listView)
    {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition)
        {
            return listView.getAdapter().getView(pos, null, listView);
        } else
        {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerPreferencesCallback()
    {
        mPreferencesActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    // check for changed settings, and apply them
                    // Note that the result code is always -1, so we ignore it.
                    boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
                    String ownDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);
                    boolean showAlbumDuration = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_DURATION, false);
                    boolean bUseFastScroll = UserSettings.getBool(UserSettings.PREF_ALBUM_LIST_FAST_SCROLL, true);
                    boolean bUseSectionIndexer = bUseFastScroll && UserSettings.getBool(UserSettings.PREF_ALBUM_LIST_SECTION_INDEXER, true);
                    Context context = getApplicationContext();
                    int albumImageSize = UserSettings.getAndPutVal(
                            UserSettings.PREF_SIZE_ALBUM_LIST_ALBUM_ART,
                            UserSettings.pixelsOrMillimeters(context, 80, 12));
                    int albumImageGridSize = getVal(UserSettings.PREF_SIZE_ALBUM_GRID_ALBUM_ART, 512);
                    boolean bGridView = UserSettings.getBool(UserSettings.PREF_GRIDVIEW, false);

                    boolean bUsingSafDb = !AppGlobals.sbAndroidDbIsActive && AppGlobals.sbOwnDatabaseIsSaf;

                    if ((bUseOwnDatabase != mbUseOwnDatabase) && !bUsingSafDb)
                    {
                        // db mode changed, while SAF is not active, restart app
                        restart();
                    }
                    else
                    if (AppGlobals.stringsDiffer(ownDatabasePath, mOwnDatabasePath) && bUseOwnDatabase && !bUsingSafDb)
                    {
                        // db path changed, we want db, while SAF is not active, restart app
                        restart();
                    }
                    else
                    if (updateSettings())
                    {
                        // db mode or colour theme has changed, recreate activity
                        recreate();
                    }
                    else
                    if ((m_audioAlbumView != null) && bGridView && (mAlbumImageGridSize != albumImageGridSize))
                    {
                        // grid size changed
                        recreate();
                    }
                    else
                    if ((m_audioAlbumView != null) && ((mbShowAlbumDuration != showAlbumDuration) || (mAlbumImageSize != albumImageSize)))
                    {
                        /*
                        mbShowAlbumDuration = showAlbumDuration;
                        mAlbumImageSize = albumImageSize;
                        mAlbumImageGridSize = albumImageGridSize;
                        // this leads to a crash:
                        m_audioAlbumAdt = new AudioAlbumAdapter(context, AppGlobals.audioAlbumList, mAlbumImageSize, mbShowAlbumDuration, bUseSectionIndexer, bGridView);
                        m_audioAlbumView.setAdapter(m_audioAlbumAdt);
                        */
                        recreate();
                    }
                    else
                    if (m_audioAlbumView != null)
                    {
                        m_audioAlbumAdt.createSections(bUseSectionIndexer);
                        m_audioAlbumView.setFastScrollEnabled(bUseFastScroll);
                    }
                }
            });
    }


    /**************************************************************************
     *
     * helper to get path from URI
     *
     * /tree/primary:Music/bla
     * /tree/12F0-3703:Music/bla
     *
     *************************************************************************/
    static String pathFromTreeUri(Uri uri)
    {
        String uriPath = uri.getPath();
        if (uriPath != null)
        {
            if (uriPath.startsWith("/tree/primary:"))
            {
                return "/storage/emulated/0/" + uriPath.substring(14);
            }

            if ((uriPath.length() >= 17) &&
                    (uriPath.substring(0, 16).matches("^/tree/....-....:$")))
            {
                return "/storage/" + uriPath.substring(6, 15) + "/" + uriPath.substring(16);
            }
        }

        return null;
    }


    /************************************************************************************
     *
     * Helper to change the db path
     * Return true when path was successfully changed and is active
     *
     ***********************************************************************************/
    private boolean changeDbPath(String newMusicBasePath)
    {
        boolean ret = false;
        String newOwnDatabasePath;
        if (newMusicBasePath.endsWith(sDbPath))
        {
            newOwnDatabasePath = newMusicBasePath + "/" + sDbName;
        }
        else
        {
            newOwnDatabasePath = newMusicBasePath + "/"+ sDbPath + "/" + sDbName;
        }
        File dbf = new File(newOwnDatabasePath);
        if (dbf.exists())
        {
            // get current values
            boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
            String currOwnDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);

            // check if there are changes
            if (AppGlobals.stringsDiffer(currOwnDatabasePath, newOwnDatabasePath))
            {
                Log.d(LOG_TAG, " new DB path = " + newOwnDatabasePath);
                UserSettings.putStringAndCommit(UserSettings.PREF_OWN_DATABASE_PATH, newOwnDatabasePath);
                ret = bUseOwnDatabase;
            }
        }

        return ret;
    }


    /**************************************************************************
     *
     * Callback for file tree selector
     *
     *************************************************************************/
    private void registerDirectorySelectCallback()
    {
        mRequestDirectorySelectActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri treeUri = data.getData();
                        if (treeUri != null)
                        {
                            Log.d(LOG_TAG, " Tree Uri = " + treeUri.getPath());

                            if (mbDirectorySelectForSaf)
                            {
                                grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                // copy and open database
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                {
                                    // remember URI for the next time the selector is opened
                                    UserSettings.putString(UserSettings.PREF_LATEST_SAF_URI, treeUri.toString());
                                    openSafTree(treeUri);
                                } else
                                {
                                    Log.e(LOG_TAG, "This function needs Android 5 or higher");
                                }
                            }
                            else
                            {
                                // /tree/primary:Music/bla
                                // /tree/12F0-3703:Music/bla
                                String newMusicBasePath = pathFromTreeUri(treeUri);
                                Log.d(LOG_TAG, " real Path = " + newMusicBasePath);
                                if (newMusicBasePath != null)
                                {
                                    File f = new File(newMusicBasePath);
                                    if (f.exists())
                                    {
                                        if (changeDbPath(newMusicBasePath))
                                        {
                                            // db path has been changed
                                            restart();
                                            return;
                                        }
                                        // db path unchanged
                                        return;
                                    }
                                }
                                DialogNoDatabaseFile();
                            }
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * SAF helper
     *
     *************************************************************************/
    private void registerFileSelectCallback()
    {
        mRequestFileSelectActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri fileUri = data.getData();
                        if (fileUri != null)
                        {
                            Log.d(LOG_TAG, " File Uri = " + fileUri.getPath());
                            // open database
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            {
                                openDatabaseFile(fileUri);
                            }
                            else
                            {
                                Log.e(LOG_TAG, "This function needs Android 5 or higher");
                            }
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * Callback for "manage all files" access permission
     *
     * Note that this function is not called when then the permission is
     * revoked! Instead, the process ends and is restarted immediately
     * at the moment the user disables the permission in the system dialogue.
     *
     * Otherwise the function is called, i.e. when the user did not change
     * the setting (either denied or permitted) or when the user granted
     * permission.
     *
     *************************************************************************/
    private void registerStorageAccessPermissionCallback()
    {
        mStorageAccessPermissionActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>()
                {
                    @Override
                    @RequiresApi(api = Build.VERSION_CODES.R)
                    public void onActivityResult(ActivityResult result)
                    {
                        Log.d(LOG_TAG, "registerStorageAccessPermissionCallback()::onActivityResult()");
                        // Note that the resultCode is not helpful here, fwr.
                        if (Environment.isExternalStorageManager())
                        {
                            boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
                            if (bUseOwnDatabase && AppGlobals.sbAndroidDbIsActive)
                            {
                                Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): restart to activate own db");
                                restart();
                            }

                            // If media file access was already granted, we do not have to do anything here,
                            // because it is only needed for accessing the music database created by
                            // Classical Music Scanner.
                            if (!sbReadMediaFileAccessGranted)
                            {
                                // permission has changed: now permitted
                                Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): full file access permission granted");
                                onPermissionResult(true);
                            }
                        }
                        else
                        {
                            if (!sbReadMediaFileAccessGranted)
                            {
                                // permission has changed: now denied
                                // most probably we will never get here
                                Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): full file access permission denied");
                                Toast.makeText(getApplicationContext(), R.string.str_manage_permission_denied, Toast.LENGTH_LONG).show();
                                if (!checkForMediaFilePermissions())
                                {
                                    Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): media file access permission also denied");
                                    onPermissionResult(false);
                                }
                            }
                        }
                    }
                });
    }


    /************************************************************************************
     *
     * Activity method: got new intent
     *
     * This function may be called in case of a "open with" or "share" operation.
     *
     ***********************************************************************************/
    @Override
    protected void onNewIntent(Intent theIntent)
    {
        Log.d(LOG_TAG, "onNewIntent(): intent action is " + theIntent.getAction());
        setIntent(theIntent);

        if (!handleIntent())
        {
            super.onNewIntent(theIntent);
        }
    }


    /************************************************************************************
     *
     * helper to handle API differences (Android 13 or older)
     *
     ***********************************************************************************/
    @SuppressWarnings("deprecation")
    private Uri getUriFromIntent(Intent theIntent)
    {
        Uri theUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            theUri = theIntent.getParcelableExtra(Intent.EXTRA_STREAM, Uri.class);
        }
        else
        {
            theUri = theIntent.getParcelableExtra(Intent.EXTRA_STREAM);
        }
        return theUri;
    }


    /************************************************************************************
     *
     * helper to handle API differences (Android 13 or older)
     *
     ***********************************************************************************/
    @SuppressWarnings("deprecation")
    private ArrayList<Uri> getUriListFromIntent(Intent theIntent)
    {
        ArrayList<Uri> theUriList;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            theUriList = theIntent.getParcelableArrayListExtra(Intent.EXTRA_STREAM, Uri.class);
        }
        else
        {
            theUriList = theIntent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        }
        return theUriList;
    }


    /************************************************************************************
     *
     * open passed files (tracks) in selection view
     *
     ***********************************************************************************/
    private boolean handleIntent()
    {
        Intent theIntent = getIntent();
        if (theIntent == null)
        {
            return true;
        }

        Log.d(LOG_TAG, "handleIntent(): intent action is " + theIntent.getAction());

        String action = theIntent.getAction();
        ArrayList<Uri> newObjectList = new ArrayList<>();

        //
        // official messages like SHARE and OPEN_WITH
        //

        if ((Intent.ACTION_VIEW.equals(action)) ||
                (Intent.ACTION_EDIT.equals(action)))
        {
            Uri theUri = theIntent.getData();
            if (theUri != null)
            {
                newObjectList.add(theUri);
            }
        }
        else
        if ((Intent.ACTION_SEND.equals(action)))
        {
            Uri theUri = getUriFromIntent(theIntent);
            if (theUri != null)
            {
                newObjectList.add(theUri);
            }
        }
        else
        if ((Intent.ACTION_SEND_MULTIPLE.equals(action)))
        {
            ArrayList<Uri> uriList = getUriListFromIntent(theIntent);
            if (uriList != null)
            {
                newObjectList.addAll(uriList);
            }
        }
        else
        {
            return false;
        }

        ArrayList<AudioTrack> addList = new ArrayList<>();
        int i = 1;
        String commonAlbumName = null;
        boolean bUseJaudioTaglib = UserSettings.getBool(UserSettings.PREF_USE_JAUDIOTAGGER_LIB, false);
        AudioTagReaderInterface theTagsFromFile = (bUseJaudioTaglib) ? new JaudioTagReader() : new MyTagReader();

        for (Uri uri : newObjectList)
        {
            String uriOrPath;
            String intentScheme = uri.getScheme();
            if ((bUseJaudioTaglib) || !"content".equals(intentScheme))
            {
                // Own tagger needs "content" scheme for Uri.
                // JaudioTagger does not deal with Uri at all
                uriOrPath = getPathFromIntentUri(this, uri);
                Log.d(LOG_TAG, "handleIntent(): Uri " + uri + " converted to path " + uriOrPath);
            }
            else
            {
                uriOrPath = uri.toString();
                Log.d(LOG_TAG, "handleIntent(): Uri: " + uri);
            }

            DocumentFile df = DocumentFile.fromSingleUri(this, uri);
            AudioTrack theTrack = new AudioTrack(
                    -1,
                    null,
                    "" + i,
                    "",     // will be set later
                    null,
                    null,
                    uriOrPath,
                    -1,
                    null,
                    null,
                    null,
                    0);
            theTrack.albumName = "";

            theTagsFromFile.get(uriOrPath, uri.getPath(), -1, getContentResolver());
            if (theTagsFromFile.isValid())
            {
                theTrack.addTagInfo(theTagsFromFile.tagAlbum, theTagsFromFile.tagTitle, theTagsFromFile.tagTrack, theTagsFromFile.tagDiscNo);
                theTrack.addTagInfo(
                        theTagsFromFile.tagMovementName,
                        theTagsFromFile.tagGrouping,
                        theTagsFromFile.tagSubtitle,
                        theTagsFromFile.tagComposer,
                        theTagsFromFile.tagConductor,
                        theTagsFromFile.tagYear);
            }
            else
            {
                Log.w(LOG_TAG, "handleIntent(): could not get tags from " + uriOrPath);
            }

            // fallback for missing title
            if (theTrack.title.isEmpty() && (df != null))
            {
                String title = df.getName();
                if (title != null)
                {
                    int index = title.lastIndexOf('.');
                    if (index > 0)
                    {
                        title = title.substring(0, index);        // remove file name extension
                    }
                    Log.w(LOG_TAG, "handleIntent() : derived title \"" + title + "\" from file name");
                    theTrack.title = title;
                }
                else
                {
                    Log.w(LOG_TAG, "handleIntent() : could not derive title from file name");
                }
            }

            // fallback for missing album name
            if (theTrack.albumName.isEmpty() && (uriOrPath != null))
            {
                String albumName = null;
                int index = uriOrPath.lastIndexOf('/');
                if (index > 0)
                {
                    albumName = uriOrPath.substring(0, index);        // remove file name
                    index = albumName.lastIndexOf('/');
                }

                if ((albumName != null) && (index >= 0))
                {
                    albumName = albumName.substring(index + 1);        // remove left part of path
                    Log.w(LOG_TAG, "handleIntent() : derived album name \"" + albumName + "\" from path");
                    theTrack.albumName = albumName;
                }
                else
                {
                    Log.w(LOG_TAG, "handleIntent() : could not derive album name from path");
                }
            }

            if (i == 1)
            {
                commonAlbumName = theTrack.albumName;
            }
            else
            if (commonAlbumName != null)
            {
                if (!commonAlbumName.equals(theTrack.albumName))
                {
                    commonAlbumName = null;
                }
            }

            addList.add(theTrack);
            i++;
        }

        m_currAlbumId = -1;
        Collections.sort(addList);  // sort by track number
        if (commonAlbumName == null)
        {
            commonAlbumName = "SHARED";
        }
        AppGlobals.openAudioAlbumPseudo(this, addList, commonAlbumName);

        Intent intent = new Intent(this, TracksOfAlbumActivity.class);
        intent.putExtra(EXTRA_MESSAGE_ALBUM_ID, m_currAlbumId);
        startActivity(intent);

        return true;
    }


    /**************************************************************************
     *
     * start SAF tree selector
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void startSafTreePicker(boolean bIsSaF)
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        // specify initial directory tree position

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String uriString = UserSettings.getString(UserSettings.PREF_LATEST_SAF_URI);
            if (uriString != null)
            {
                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uriString);
            }
        }

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

        // callback: see registerDirectorySelectCallback()
        mbDirectorySelectForSaf = bIsSaF;
        mRequestDirectorySelectActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * start SAF file selector
     *
     * Note that dumb API cannot select file name extension, but only
     * MIME types, and Android does not know about "application/vnd.sqlite3".
     * Further, any usable documentation about supported MIME types is missing.
     *
     * Note that EXTRA_INITIAL_URI does not accept file URIs, but only those
     * generated from SAF. Thus this function uses a hack.
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void startSafFilePicker()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");

        /*
        // THIS DOES NOT WORK DUE TO POOR API DESIGN
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String bla = map.getMimeTypeFromExtension("db");
        String bla2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension("sqlite");
        String bla3 = MimeTypeMap.getSingleton().getMimeTypeFromExtension("sqlite3");
        String blu = MimeTypeMap.getSingleton().getExtensionFromMimeType("application/vnd.sqlite3");
        intent.setType("application/x-sqlite3");
        */

        // specify initial directory tree position
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            // This is a hack ...
            Uri uri = Uri.parse("content://com.android.externalstorage.documents/document/primary%3A" + sDbPath);
            // ... because that one does not work:
            // Uri uri = Uri.fromFile(new File(getSharedDbFilePath(this)));
            if (uri != null)
            {
                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
            }
        }

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        /*
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
         */
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        // DEPRECATED startActivityForResult(intent, REQUEST_FILE_SELECT);
        mRequestFileSelectActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * copy database from here and open it
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openSafTree(Uri treeUri)
    {
        DocumentFile treeDf = DocumentFile.fromTreeUri(this, treeUri);
        if (treeDf == null)
        {
            Log.e(LOG_TAG, "cannot open treeUri: " + treeUri);
            Toast.makeText(getApplicationContext(), R.string.str_nonAccessiblePath, Toast.LENGTH_LONG).show();
            return;
        }

        DocumentFile ddf = treeDf.findFile(sDbPath);
        if (ddf == null)
        {
            Log.e(LOG_TAG, "cannot find directory " + sDbPath + " in treeUri: " + treeUri);
            DialogPickDatabaseFile();
            return;
        }

        DocumentFile df = ddf.findFile(sDbSafName);
        if (df == null)
        {
            Log.e(LOG_TAG, "cannot find file " + sDbSafName + " in directory " + sDbPath + " in treeUri: " + treeUri);
            DialogPickDatabaseFile();
            return;
        }

        InputStream is;
        try
        {
            is = getContentResolver().openInputStream(df.getUri());
            if (is == null)
            {
                Log.e(LOG_TAG, "cannot open file for input " + sDbSafName + " in directory " + sDbPath + " in treeUri: " + treeUri);
                return;
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "cannot create output stream");
            return;
        }

        boolean bResult = false;
        File tempDbFile = new File(getFilesDir(), "tempSaf.db");
        //noinspection ResultOfMethodCallIgnored
        tempDbFile.delete();
        FileOutputStream os;
        try
        {
            if (!tempDbFile.createNewFile())
            {
                Log.e(LOG_TAG, "cannot create temporary file");
                SafUtilities.closeStream(is);
                return;
            }
            os = new FileOutputStream(tempDbFile);
            bResult = SafUtilities.copyFileFromTo(is, os);
        } catch (IOException e)
        {
            SafUtilities.closeStream(is);
            Log.e(LOG_TAG, "cannot create temporary file: " + e);
        }

        if (bResult)
        {
            Toast.makeText(getApplicationContext(), R.string.str_db_retrieved, Toast.LENGTH_SHORT).show();
            sSafDatabasePath = tempDbFile.getPath();
            restart();
        }
        else
        {
            Toast.makeText(getApplicationContext(), R.string.str_db_not_retrieved, Toast.LENGTH_LONG).show();
        }
    }


    /**************************************************************************
     *
     * For SAF mode: open database from internal memory
     *
     * The file Uri looks like this:
     *  content://com.android.externalstorage.documents/document/primary%3AClassicalMusicDb%2F6D9B-B0E6%20Music.db
     *
     * The file generated from it looks like
     *  /document/primary:ClassicalMusicDb/6D9B-B0E6 Music.db
     *
     * There is no conversion so that we have to use a hack here.
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openDatabaseFile(@NonNull Uri fileUri)
    {
        String dbPath = getDefaultSharedDbFilePath();
        if (dbPath != null)
        {
            String path = fileUri.getPath();
            if (path != null)
            {
                File f = new File(path);
                f = new File(dbPath, f.getName());
                if (f.canRead())
                {
                    sSafDatabasePath = f.getPath();
                    restart();
                    return;
                }
            }
        }

        Toast.makeText(getApplicationContext(),"ERROR: Database not retrieved", Toast.LENGTH_LONG).show();
    }


    /**************************************************************************
     *
     * "About" dialogue
     *
     *************************************************************************/
    private void DialogAbout()
    {
        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);

        final String strTitle = "Unpopular Music Player";
        final String strDescription = getString(R.string.str_app_description);
        final String strAuthor = getString(R.string.str_author);
        @SuppressWarnings("ConstantConditions") boolean bIsPlayStoreVersion = ((BuildConfig.BUILD_TYPE.equals("release_play")) || (BuildConfig.BUILD_TYPE.equals("debug_play")));
        @SuppressWarnings("ConstantConditions") final String tStore = (bIsPlayStoreVersion) ? "   [Play Store]" : "   [free]";
        final String strVersion = "Version " + info.versionName + (((info.isDebug) ? " DEBUG" : "") + tStore);

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setIcon(R.drawable.app_icon_noborder);
        alertDialog.setMessage(
                        strDescription + "\n\n" +
                        strAuthor + "Andreas Kromke" + "\n\n" +
                        strVersion + "\n" +
                        "(" + info.strCreationTime + ")");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * "Not Available in Play Store Version" dialogue
     *
     *************************************************************************/
    private void DialogNotAvailableInPlayStoreVersion()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.str_NeedManageFilesPermission));
        alertDialog.setMessage(getString(R.string.str_NotAvailableInPlayStoreVersion));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * "Select Database File" dialogue
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void DialogPickDatabaseFile()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.str_NeedDatabaseFile));
        alertDialog.setMessage(getString(R.string.str_NeedDatabaseFileDescription));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        startSafFilePicker();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * "There is no Database File" dialogue
     *
     *************************************************************************/
    private void DialogNoDatabaseFile()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getString(R.string.str_NeedDatabaseFile));
        alertDialog.setMessage(getString(R.string.str_NoDatabaseFileDescription));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * html based dialogue
     *
     *************************************************************************/
    private void DialogHtml(final String filename)
    {
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/html-" + getString(R.string.locale_prefix) + "/" + filename);
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setView(webView);
        alertDialog.show();
    }


    /**************************************************************************
     *
     * wrapper: Some mysterious crashes occurred caused by Android to have been
     *          corrupted by failed or incomplete updatesof the WebView component.
     *
     *************************************************************************/
    private void DialogHtmlSafe(final String filename)
    {
        try
        {
            DialogHtml(filename);
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "HTML system component WebView is corrupt");
        }
    }


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogHelp()
    {
        DialogHtmlSafe("help.html");
    }


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogChanges()
    {
        DialogHtmlSafe("changes.html");
    }


    /**************************************************************************
     *
     * Ask to proceed to Play Store
     *
     * Note that this function is "static" because it is also called from
     * TracksOfAlbumActivity. This is ugly, should be improved.
     *
     *************************************************************************/
    public static void dialogOpenPlayStore(final AppCompatActivity context, final String packageName)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppDialogTheme);
        builder.setTitle(R.string.str_app_install);
        builder.setMessage(context.getString(R.string.str_app_install_msg) + "\n(" + packageName + ")");

        builder.setPositiveButton(R.string.str_play_store, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                try
                {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                } catch (android.content.ActivityNotFoundException anfe)
                {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
                }
            }
        });

        builder.setNeutralButton(R.string.str_fdroid, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://f-droid.org/packages/" + packageName)));
            }
        });

        builder.setNegativeButton(R.string.str_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Log.d(LOG_TAG, "cancelled");
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    /**************************************************************************
     *
     * start scanner or tagger or play store app
     *
     *************************************************************************/
    private void startOtherApp(final String packageName)
    {
        Intent theIntent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (theIntent != null)
        {
            //
            // tagger or scanner is installed, start it
            //

            theIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            //theIntent.putExtra("pathTable", audioPathList);   // currently not supported
            startActivity(theIntent);
        } else
        {
            //
            // tagger or scanner is not installed, ask if to proceed to play store
            //

            dialogOpenPlayStore(this, packageName);
        }
    }


    /**************************************************************************
     *
     * restart the app
     *
     *************************************************************************/
    private void restart()
    {
        Log.d(LOG_TAG, "restart()");
        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
        if (intent != null)
        {
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = Intent.makeRestartActivityTask(componentName);
            mainIntent.putExtra(sIntentKeyDatabasePath, sSafDatabasePath);
            startActivity(mainIntent);
            System.exit(0);
        }
    }


    /**************************************************************************
     *
     * menu handling
     *
     *************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.d(LOG_TAG, "onOptionsItemSelected()");
        int id = item.getItemId();

        if (id == R.id.action_about)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_about");
            DialogAbout();
        }
        else
        if (id == R.id.action_changes)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_changes");
            DialogChanges();
        }
        else
        if (id == R.id.action_help)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_help");
            DialogHelp();
        }
        else
        if (id == R.id.action_settings)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_settings");
            // User chose the "Settings" item, show the app settings UI...
            Intent intent = new Intent(this, UserSettingsActivity.class);
            // DEPRECATED startActivityForResult(i, RESULT_SETTINGS);
            mPreferencesActivityLauncher.launch(intent);
        }
        else
        if (id == R.id.action_tag)
        {
            startOtherApp("de.kromke.andreas.musictagger");
            return true;
        }
        else
        if (id == R.id.action_scan)
        {
            startOtherApp("de.kromke.andreas.mediascanner");
            return true;
        }
        else
        if (id == R.id.action_manage_external_files)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            {
                // Android 10 and newer: MANAGE EXTERNAL FILES
                //noinspection ConstantConditions
                if ((BuildConfig.BUILD_TYPE.equals("release_play")) || (BuildConfig.BUILD_TYPE.equals("debug_play")))
                {
                    DialogNotAvailableInPlayStoreVersion();
                } else
                {
                    requestForManageAllFiles();
                }
            }
            return true;
        }
        else
        if (id == R.id.action_saf_select)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                startSafTreePicker(true);
            }
            return true;
        }
        else
        if (id == R.id.action_path_select)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                startSafTreePicker(false);
            }
            return true;
        }
        else
        if (id == R.id.action_reload_db)
        {
            restart();
            return true;
        }
        else
        if (id == R.id.check_gridview)
        {
            boolean b = mMenuItemGridView.isChecked();
            b = !b;     // invert
            mMenuItemGridView.setChecked(b);
            // The commit() operation is essential before restart(). Otherwise nothing is written.
            UserSettings.putValAndCommit(UserSettings.PREF_GRIDVIEW, b);
            restart();
            return true;
        }
        else
        if (id == R.id.action_exit)
        {
            System.exit(0);
            return true;
        }
        else
        {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            return super.onOptionsItemSelected(item);
        }

        return true;
    }


    /**************************************************************************
     *
     * callback for user album select
     *
     * -> "audio_album.xml"
     *
     *************************************************************************/
    public void onAlbumClicked(View view)
    {
        //
        // get tag from view and convert to string and this string to number
        //

        Object theTag = view.getTag();
        if (theTag == null)
        {
            Log.e(LOG_TAG, "onAlbumClicked() : no tag");
            return;
        }
        String theTagString = theTag.toString();
        int newAlbumNo;
        try
        {
            newAlbumNo = Integer.parseInt(theTagString);
        }
        catch(NumberFormatException e)
        {
            newAlbumNo = 0;
        }

        //
        // use this number to open an album
        //

        if (newAlbumNo != m_currAlbumNo)
        {
            /*
            if (mbUseCollapsingLayout)
            {
                m_audioAlbumRecyclerAdt.setCurrAlbum(newAlbumNo);
            }
            else*/
            {
                m_audioAlbumAdt.setCurrAlbum(newAlbumNo);
            }

            if (m_currAlbumNo >= 0)
            {
                View v;
                if (m_audioAlbumView != null)
                {
                    v = getViewByPosition(m_currAlbumNo, m_audioAlbumView);
                }
                else
                {
                    v = view;
                }
                if (v != null)
                {
                    // back to normal
                    v.setBackgroundColor(UserSettings.getThemedColourFromResId(this, R.attr.album_list_background_normal));
                }
            }
            if (newAlbumNo >= 0)
            {
                View v;
                if (m_audioAlbumView != null)
                {
                    v = getViewByPosition(newAlbumNo, m_audioAlbumView);
                }
                else
                {
                    v = view;
                }
                if (v != null)
                {
                    // selected
                    v.setBackgroundColor(UserSettings.getThemedColourFromResId(this, R.attr.album_list_background_selected));
                }
            }
            m_currAlbumNo = newAlbumNo;
        }

        AudioAlbum currAlbum = (AppGlobals.sAudioAlbumList != null) ? AppGlobals.sAudioAlbumList.get(m_currAlbumNo) : null;
        if (currAlbum != null)
        {
            m_currAlbumId = currAlbum.album_id;
            Log.v(LOG_TAG, "onAlbumClicked() : theTag = " + theTagString);
            Intent intent = new Intent(this, TracksOfAlbumActivity.class);
            intent.putExtra(EXTRA_MESSAGE_ALBUM_ID, m_currAlbumId);
            AppGlobals.openAudioAlbum(this, currAlbum);
            startActivity(intent);
        }
        else
        {
            m_currAlbumId = -1;
            Log.w(LOG_TAG, "onAlbumClicked() : invalid album");
        }
    }


    /**************************************************************************
     *
     * dummy callbacks to avoid this Android bug:
     *
     * https://stackoverflow.com/questions/44073861/play-store-crash-report-illegalstateexception-on-android-view-viewdeclaredoncl/46435279#46435279
     *
     *************************************************************************/
    @SuppressWarnings({"unused", "RedundantSuppression"})
    public void onNoItemClicked()
    {
        Log.e(LOG_TAG, "onNoItemClicked() -- misdirected callback (Android bug)");
    }
    public void onTrackClicked(View view)
    {
        Log.e(LOG_TAG, "onTrackClicked() -- misdirected callback (Android bug)");
    }
    public void onSpaceClicked(View view)
    {
        Log.e(LOG_TAG, "onSpaceClicked() -- misdirected callback (Android bug)");
    }
    public void onAlbumImageClicked(View v)
    {
        Log.e(LOG_TAG, "onAlbumImageClicked() -- misdirected callback (Android bug)");
    }
    @SuppressWarnings({"unused", "RedundantSuppression"})
    public void onClick(View v)
    {
        Log.e(LOG_TAG, "onClick() -- misdirected callback (Android bug)");
    }


    /************************************************************************************
     *
     * get default path of database file from environment or null on error
     *
     ***********************************************************************************/
    private static String getDefaultSharedDbFilePath()
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStorageDirectory();
        } else
        {
            Log.e(LOG_TAG, "getSharedDbFilePath() -- media not mounted, no db path");
            return null;
        }

        return basePath.getPath() + "/" + sDbPath;
    }


    /************************************************************************************
     *
     * get default path of database file from environment or null on error
     *
     ***********************************************************************************/
    private static String getDefaultSharedDbFile()
    {
        String dbPath = getDefaultSharedDbFilePath();
        if (dbPath != null)
        {
            return dbPath + "/" + sDbName;
        }
        else
        {
            return null;
        }
    }


    /**************************************************************************
     *
     * write default settings, if necessary (for first start of app)
     *
     *************************************************************************/
    private void writeDefaultSettings()
    {
        String defaultPath = getDefaultSharedDbFile();
        if (defaultPath != null)
        {
            UserSettings.initString(UserSettings.PREF_OWN_DATABASE_PATH, defaultPath);
        }
    }


    /**************************************************************************
     *
     * update colour theme etc., if necessary
     *
     *************************************************************************/
    private boolean updateSettings()
    {
        int colourTheme = getVal(PREF_THEME, 0);
        if ((mColourTheme < 0) || (mColourTheme != colourTheme))
        {
            mColourTheme = colourTheme;
            switch (mColourTheme)
            {
                default:
                case 0:
                    setTheme(R.style.AlbumListTheme_BlueLight);
                    break;
                case 1:
                    setTheme(R.style.AlbumListTheme_OrangeLight);
                    break;
                case 2:
                    setTheme(R.style.AlbumListTheme_GreenLight);
                    break;
                case 3:
                    setTheme(R.style.AlbumListTheme_Blue);
                    break;
                case 4:
                    setTheme(R.style.AlbumListTheme_Gray);
                    break;
            }

            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                Window window = getWindow();
                window.setStatusBarColor(UserSettings.getThemedColourFromResId(this, R.attr.colorPrimaryDark));
            }
            return true;
        }
        return false;
    }
}
