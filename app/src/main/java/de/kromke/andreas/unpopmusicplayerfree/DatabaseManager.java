/*
 * Copyright (C) 2018-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;

/**
 * SQLite database access to shared database created by "Classical Music Scanner" application
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
public class DatabaseManager
{
    private static final String LOG_TAG = "UMPL : DatabaseManager";

    private SQLiteDatabase mDb = null;

    /* this block has to be kept in sync with corresponding definitions in Music Scanner */
    private final static String FILES_TABLE_NAME  = "MUSICFILES";
    private final static String ALBUMS_TABLE_NAME = "ALBUMS";
    private final static String INFO_TABLE_NAME   = "INFORMATION";

    private final static String ALBUMS_TABLE_TCOL_ID         = "_id";
    private final static String ALBUMS_TABLE_TCOL_NAME       = "album";
    private final static String ALBUMS_TABLE_TCOL_ARTIST     = "artist";
    private final static String ALBUMS_TABLE_TCOL_COMPOSER   = "composer";
    private final static String ALBUMS_TABLE_TCOL_PERFORMER  = "performer";
    private final static String ALBUMS_TABLE_TCOL_CONDUCTOR  = "conductor";
    private final static String ALBUMS_TABLE_TCOL_GENRE      = "genre";
    private final static String ALBUMS_TABLE_TCOL_FIRST_YEAR = "minyear";
    private final static String ALBUMS_TABLE_TCOL_LAST_YEAR  = "maxyear";
    private final static String ALBUMS_TABLE_TCOL_NO_TRACKS  = "numsongs";
    private final static String ALBUMS_TABLE_TCOL_DURATION   = "duration";
    private final static String ALBUMS_TABLE_TCOL_PATH       = "path";
    private final static String ALBUMS_TABLE_TCOL_PICTURE    = "album_art";

    private final static String FILES_TABLE_TCOL_ID           = "_id";
    private final static String FILES_TABLE_TCOL_TRACK_NO     = "track";
    private final static String FILES_TABLE_TCOL_TITLE        = "title";
    private final static String FILES_TABLE_TCOL_ALBUM        = "album";
    private final static String FILES_TABLE_TCOL_ALBUM_ID     = "album_id";
    private final static String FILES_TABLE_TCOL_DURATION     = "duration";
    private final static String FILES_TABLE_TCOL_GROUPING     = "grouping";
    private final static String FILES_TABLE_TCOL_SUBTITLE     = "subtitle";
    private final static String FILES_TABLE_TCOL_COMPOSER     = "composer";
    private final static String FILES_TABLE_TCOL_PERFORMER    = "artist";
    private final static String FILES_TABLE_TCOL_ALBUM_ARTIST = "album_artist";
    private final static String FILES_TABLE_TCOL_CONDUCTOR    = "conductor";
    private final static String FILES_TABLE_TCOL_GENRE        = "genre";
    private final static String FILES_TABLE_TCOL_YEAR         = "year";
    private final static String FILES_TABLE_TCOL_PATH         = "path";
    private final static String FILES_TABLE_TCOL_PIC_TYPE     = "pic_type";      // 0: none, 1: jpeg, 2: png
    private final static String FILES_TABLE_TCOL_PIC_SIZE     = "pic_size";
    private final static String FILES_TABLE_TCOL_TAG_TYPE     = "tag_type";      // tagType

    public int open(final String dbPath)
    {
        Log.v(LOG_TAG, "open database \"" + dbPath + "\"");
        mDb = null;

        // to avoid exceptions and lots of log file entries, check this before:
        if (!AppGlobals.pathIsReadable(dbPath))
        {
            Log.e(LOG_TAG, "cannot open database \"" + dbPath + "\"");
            return -1;
        }

        try
        {
            mDb = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READONLY);
        }
        catch(Exception e)
        {
            Log.e(LOG_TAG, "cannot open database \"" + dbPath + "\"");
            return -1;
        }

        return 0;
    }


    @SuppressWarnings("WeakerAccess")
    public ArrayList<AudioAlbum> queryAlbums()
    {
        if (mDb == null)
        {
            return null;
        }

        final String[] columns =
        {
            ALBUMS_TABLE_TCOL_ID,               // column 0 of result
            ALBUMS_TABLE_TCOL_NAME,
            ALBUMS_TABLE_TCOL_ARTIST,
            ALBUMS_TABLE_TCOL_COMPOSER,
            ALBUMS_TABLE_TCOL_PERFORMER,
            ALBUMS_TABLE_TCOL_NO_TRACKS,
            ALBUMS_TABLE_TCOL_DURATION,
            ALBUMS_TABLE_TCOL_FIRST_YEAR,
            ALBUMS_TABLE_TCOL_LAST_YEAR,
            ALBUMS_TABLE_TCOL_PICTURE           // column 7 of result
        };

        final String orderBy = ALBUMS_TABLE_TCOL_NAME + " COLLATE NOCASE ASC";
        Cursor theCursor;
        try
        {
            theCursor = mDb.query(
                    ALBUMS_TABLE_NAME,      // table
                    columns,                // columns to return, null returns all rows
                    null,       // rows to return, as "WHERE ...", null returns all rows
                    null,           // selection arguments, replacing question marks in previous argument
                    null,                    // "GROUP BY" as string
                    null,
                    orderBy     // sort order, null: unordered
            );
        }
        catch (SQLiteException e)
        {
            Log.e(LOG_TAG, "incompatible database");
            return null;
        }

        // handle error cases
        if (theCursor == null)
        {
            Log.e(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no album found");
            theCursor.close();
            return null;
        }

        int numAlbums = 0;

        //
        // loop to add all album data to the global album list
        //
        ArrayList<AudioAlbum> audioAlbumList = new ArrayList</*AudioAlbum*/>();
        do
        {
            int idx = 0;
            long thisAlbumId         = theCursor.getLong  (idx++);
            String thisAlbumName     = theCursor.getString(idx++);
            String theAlbumArtist    = theCursor.getString(idx++);
            String theAlbumComposer  = theCursor.getString(idx++);
            String theAlbumPerformer = theCursor.getString(idx++);
            long thisNoOfTracks      = theCursor.getInt   (idx++);
            long thisDuration        = theCursor.getInt   (idx++);
            long thisFirstYear       = theCursor.getInt   (idx++);
            long thisLastYear        = theCursor.getInt   (idx++);
            String thisAlbumArt      = theCursor.getString(idx);

            /*
            Log.v(LOG_TAG, "Album Id (_ID)     = " + thisAlbumId);
            Log.v(LOG_TAG, "Album Name         = " + thisAlbumName);
            Log.v(LOG_TAG, "Album Composer     = " + theAlbumComposer);
            Log.v(LOG_TAG, "Album Performer    = " + theAlbumPerformer);
            Log.v(LOG_TAG, "Album No of Tracks = " + thisNoOfTracks);
            Log.v(LOG_TAG, "Album Duration     = " + thisDuration);
            Log.v(LOG_TAG, "Album First Year   = " + thisFirstYear);
            Log.v(LOG_TAG, "Album Last Year    = " + thisLastYear);
            Log.v(LOG_TAG, "Album Art          = " + thisAlbumArt);
            Log.v(LOG_TAG, "==============================================\n");
            */

            numAlbums++;

            if (thisAlbumName != null)
            {
                AudioAlbum theAlbum = new AudioAlbum(
                        thisAlbumId,
                        thisAlbumName,
                        theAlbumPerformer,
                        thisNoOfTracks,
                        thisFirstYear,
                        thisLastYear,
                        thisAlbumArt);
                theAlbum.album_duration = thisDuration;
                theAlbum.setAlbumComposer(theAlbumComposer);
                theAlbum.setAlbumArtist(theAlbumArtist);
                theAlbum.setAlbumPerformer(theAlbumPerformer);
                if (thisFirstYear == thisLastYear)
                {
                    theAlbum.setAlbumYear((int) thisFirstYear);
                }
                audioAlbumList.add(theAlbum);
            }
        }
        while (theCursor.moveToNext());

        theCursor.close();

        Log.d(LOG_TAG, "found " + numAlbums + " albums");
        return audioAlbumList;
    }


    @SuppressWarnings("WeakerAccess")
    public ArrayList<AudioTrack> queryTracksOfAlbum(AudioAlbum theAlbum)
    {
        if (mDb == null)
        {
            return null;
        }

        final String[] columns =
        {
            FILES_TABLE_TCOL_ID,                // column 0 of result
            FILES_TABLE_TCOL_TRACK_NO,
            FILES_TABLE_TCOL_TITLE,
            FILES_TABLE_TCOL_DURATION,
            FILES_TABLE_TCOL_GROUPING,
            FILES_TABLE_TCOL_SUBTITLE,
            FILES_TABLE_TCOL_COMPOSER,
            FILES_TABLE_TCOL_PERFORMER,
            FILES_TABLE_TCOL_CONDUCTOR,
            FILES_TABLE_TCOL_GENRE,
            FILES_TABLE_TCOL_YEAR,
            FILES_TABLE_TCOL_PATH               // column 11
        };

        long album_id = theAlbum.album_id;

        final String where = FILES_TABLE_TCOL_ALBUM_ID + "=?";
        final String[] whereVal = { "" + album_id };
        final String orderBy = FILES_TABLE_TCOL_TRACK_NO + ", " + FILES_TABLE_TCOL_TITLE;

        Cursor theCursor;
        try
        {
            theCursor = mDb.query(
                    FILES_TABLE_NAME,   // table
                    columns,            // columns to return, null returns all rows
                    where,              // rows to return, as "WHERE ...", null returns all rows
                    whereVal,           // selection arguments, replacing question marks in previous argument
                    null,      // "GROUP BY" as string
                    null,
                    orderBy     // sort order, null: unordered
            );
        }
        catch (SQLiteException e)
        {
            Log.e(LOG_TAG, "incompatible database");
            return null;
        }

        // handle error cases
        if (theCursor == null)
        {
            Log.e(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no track found for album id " + album_id);
            theCursor.close();
            return null;
        }

        int numTracks = 0;

        //
        // loop to add all track data to the album track list
        //
        ArrayList<AudioTrack> currAlbumTrackList = new ArrayList</*AudioTrack*/>();
        do
        {
            int idx = 0;
            long thisId          = theCursor.getLong  (idx++);
            String thisTrack     = theCursor.getString(idx++);
            String thisTitle     = theCursor.getString(idx++);
            long thisDuration    = theCursor.getInt   (idx++);
            String thisGrouping  = theCursor.getString(idx++);
            String thisSubtitle  = theCursor.getString(idx++);
            String thisComposer  = theCursor.getString(idx++);
            String thisPerformer = theCursor.getString(idx++);
            String thisConductor = theCursor.getString(idx++);
            String thisGenre     = theCursor.getString(idx++);
            long thisYear        = theCursor.getInt   (idx++);
            String thisPath      = theCursor.getString(idx);

            /*
            Log.v(LOG_TAG, "thisId        = " + thisId);
            Log.v(LOG_TAG, "thisTrack     = " + thisTrack);
            Log.v(LOG_TAG, "thisTitle     = " + thisTitle);
            Log.v(LOG_TAG, "thisDuration  = " + thisDuration);
            Log.v(LOG_TAG, "thisGrouping  = " + thisGrouping);
            Log.v(LOG_TAG, "thisSubtitle  = " + thisSubtitle);
            Log.v(LOG_TAG, "thisComposer  = " + thisComposer);
            Log.v(LOG_TAG, "thisPerformer = " + thisPerformer);
            Log.v(LOG_TAG, "thisConductor = " + thisConductor);
            Log.v(LOG_TAG, "thisGenre     = " + thisGenre);
            Log.v(LOG_TAG, "thisYear      = " + thisYear);
            Log.v(LOG_TAG, "thisPath      = " + thisPath);
            Log.v(LOG_TAG, "==============================================\n");
            */

            numTracks++;
            AudioTrack theTrack = new AudioTrack(
                    thisId,
                    theAlbum,
                    thisTrack,
                    thisTitle,
                    thisGrouping,
                    thisSubtitle,
                    thisPath,
                    thisDuration,
                    thisComposer,
                    thisPerformer,
                    thisConductor,
                    (int) thisYear);

            currAlbumTrackList.add(theTrack);
        }
        while (theCursor.moveToNext());

        theCursor.close();

        Log.d(LOG_TAG, "found " + numTracks + " tracks");
        return currAlbumTrackList;
    }


    public void close()
    {
        if (mDb != null)
        {
            mDb.close();
            mDb = null;
        }

    }
}
