/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.MediaController;

/*
 * Slightly extend MediaController
 * Forwards key presses and implements no-autohide option
 */

@SuppressWarnings("SpellCheckingInspection")
public class MyMediaController extends MediaController
{
    private static final String LOG_TAG = "UMPL : MyMedController";
    private final boolean mAutohideMode;

	public MyMediaController(Context c, boolean autohideMode)
    {
		super(c);
        mAutohideMode = autohideMode;
	}

	// this function is defined only to avoid IDE warnings
    public MyMediaController(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mAutohideMode = false;
    }

    public void reallyHide()
    {
        //Log.d(LOG_TAG, "reallyHide()");
        if (isShowing())
        {
            //Log.d(LOG_TAG, "reallyHide() -- call super.hide()");
            super.hide();
        }
    }

    // pass keys for back, menu and search to activity, all others to base class
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        int keyCode = event.getKeyCode();
        //Log.d(LOG_TAG, "dispatchKeyEvent() : keyCode = " + keyCode);
        //final boolean uniqueDown = (event.getRepeatCount() == 0) && (event.getAction() == KeyEvent.ACTION_DOWN);
        if ((keyCode == KeyEvent.KEYCODE_BACK) ||
            (keyCode == KeyEvent.KEYCODE_MENU) ||
            (keyCode == KeyEvent.KEYCODE_SEARCH))
        {
//            if (uniqueDown)
            {
                // pass key event to activity
                return ((Activity) getContext()).dispatchKeyEvent(event);
            }
//            return false;
        }
        else
        {
            if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY)
            {
                Log.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_PLAY");
            }
            else
            if (keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE)
            {
                Log.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_PAUSE");
            }
            else
            if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP)
            {
                Log.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_STOP");
            }
            return super.dispatchKeyEvent(event);
        }
    }

    public void hide()
	{
        //Log.d(LOG_TAG, "hide()");
        //suppress automatic hiding the controller, instead use explicit function above
        if (mAutohideMode)
        {
            super.hide();
        }
	}

    public void show(int timeout)
    {
        try
        {
            super.show(timeout);
        } catch (Exception e)
        {
            Log.w(LOG_TAG, "Error on show()", e);
        }
    }

/*
    public void show()
    {
        Log.d(LOG_TAG, "show()");
        super.show();
    }
    */
}
