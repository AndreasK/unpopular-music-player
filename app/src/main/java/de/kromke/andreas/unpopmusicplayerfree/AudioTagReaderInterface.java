/*
 * Copyright (C) 2016-17 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.content.ContentResolver;

/**
 * Reads tags from audio file
 */

@SuppressWarnings("WeakerAccess")
public abstract class AudioTagReaderInterface
{
    public String tagTitle        = null;
    public String tagComposer     = null;
    public String tagConductor    = null;
    public String tagGrouping     = null;       // mp4: may hold ©wrk or ©grp, mp3: TIT2 or GRP1
    public String tagMovementName = null;       // mp4: ©mvi and ©mvn, mp3: MVNM and MVIN (up to "/")
    public String tagSubtitle     = null;
    public String tagAlbum        = null;
    public String tagAlbumArtist  = null;
    public String tagDiscNo       = null;
    public String tagDiscTotal    = null;
    public String tagTrack        = null;
    public String tagTrackTotal   = null;
    public int tagYear = 0;                     // information from Android DB is unreliable, so get it here

    /*
    * get data from file or Uri
    */
    abstract public void get(String thisPathOrUri, String name, long size, ContentResolver resolver);

    /*
     * validation
     */
    boolean isValid()
    {
        return tagTitle != null;
    }
}
