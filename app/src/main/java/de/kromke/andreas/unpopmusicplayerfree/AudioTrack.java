/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;


import androidx.annotation.NonNull;

/**
 * all data for a single audio file
 */
class AudioTrack implements Comparable<AudioTrack>
{
    static boolean sbUsingAndroidDb = true;
    long id;
    public String title;            // the title of the "track" resp. movement, e.g. "I. Allegro"
    public long duration;
    public String grouping;         // the name of the complete piece, i.e. "Symphony No. 5"
    public String subtitle;         //  maybe like "op. 42"?
    public String composer;         // the composer of the complete piece, i.e. Wolfgang Mozart
    String performer;               // performer(s) (called "artist" in subculture), i.e. NDR Radiosinfonie
    public String conductor;
    public int trackno;
    public int discno;
    public int year;
    int group_no;
    boolean group_last;             // last in group
    public String path;             // needed to later read the audio tags from file
    public AudioAlbum album;        // album the track belongs to
    public String albumName;        // in case there is no album

    // complete constructor
    AudioTrack(
            long audioTrackID,
            AudioAlbum theAlbum,
            String theTrackNo,
            String theTrackTitle,
            String theGrouping,
            String theSubtitle,
            String theTrackPath,
            long theDuration,
            String theComposer,
            String theTrackArtist,
            String theConductor,
            int theYear)
    {
        id         = audioTrackID;
        album      = theAlbum;
        albumName  = null;
        initTrackNo(theTrackNo);        // disc and track number (disc number 1000, 2000, ...)
        title      = (theTrackTitle != null) ? theTrackTitle : "";
        grouping   = (theGrouping != null) ? theGrouping : "";
        subtitle   = (theSubtitle != null) ? theSubtitle : "";
        path       = theTrackPath;
        duration   = theDuration;
        composer   = AppGlobals.beautifyName(theComposer);
        performer  = AppGlobals.beautifyName(theTrackArtist);
        conductor  = AppGlobals.beautifyName(theConductor);
        year       = theYear;

        group_no   = 0;
        group_last = true;
    }

    // partial constructor, remaining information will be passed with addTagInfo()
    AudioTrack(
            long audioTrackID,
            AudioAlbum theAlbum,
            String theTrackNo,
            String theTrackTitle,
            String theTrackPath,
            long theDuration,
            String theTrackArtist)
    {
        id         = audioTrackID;
        album      = theAlbum;
        albumName  = null;
        initTrackNo(theTrackNo);        // disc and track number (disc number 1000, 2000, ...)
        title      = (theTrackTitle != null) ? theTrackTitle : "";
        path       = theTrackPath;
        duration   = theDuration;
        composer   = "";
        performer  = AppGlobals.beautifyName(theTrackArtist);
        conductor  = "";

        year       = 0;
        grouping   = "";
        subtitle   = "";
        group_no   = 0;
        group_last = true;
    }

    void addTagInfo(
            String theAlbumName,
            String theTitle,
            String theTrackno,
            String theDiscno)
    {
        albumName = theAlbumName;
        title = theTitle;
        trackno = getNum(theTrackno);
        discno = getNum(theDiscno);
    }


    void addTagInfo(
        String audioTrackMovementName,
        String audioTrackGrouping,
        String audioTrackSubtitle,
        String theComposer,
        String theConductor,
        int audioTrackYear)
    {
        if ((audioTrackMovementName != null) && (!audioTrackMovementName.isEmpty()))
        {
            // if there is a movement name, replace title with this one
            title = audioTrackMovementName;
        }

        grouping  = (audioTrackGrouping != null) ? audioTrackGrouping : "";
        subtitle  = (audioTrackSubtitle != null) ? audioTrackSubtitle : "";
        composer  = AppGlobals.beautifyName(theComposer);
        conductor = AppGlobals.beautifyName(theConductor);
        year      = audioTrackYear;   // ignore unreliable value from Android
    }

	// The composer matches if both are not set or if they are identical.
    @SuppressWarnings("SimplifiableIfStatement")
	static private boolean composerMatch(final String composer1, final String composer2)
	{
		if (composer1 == null)
		{
			// only match, if both are null
			return (composer2 == null);
		}

		if (composer2 == null)
		{
			// only match, if both are null, and composer1 is not (see above)
			return false;
		}

		return (composer1.equals(composer2));
	}


    // convert text to number
    static private int getNum(String trackInfo)
    {
        int n;
        try
        {
            n = Integer.parseInt(trackInfo);
        } catch (NumberFormatException e)
        {
            n = 0;
        }

        return n;
    }


	// convert text to numeric track number
	private void initTrackNo(String trackInfo)
    {
        // convert track number from string to number
        int n = getNum(trackInfo);
        // divide by 1000 to get CD number from track number
        discno = n / 1000;
        trackno = n % 1000;
    }


	// Two tracks belong to the same group if grouping is valid, and
	// both grouping and composer match.
    @SuppressWarnings("SimplifiableIfStatement")
	boolean isSameGroup(final AudioTrack anotherTrack)
	{
		if ((anotherTrack != null) &&
		    (anotherTrack.grouping != null) &&
		    !(anotherTrack.grouping.isEmpty()) &&
		    (grouping != null) &&
		    (grouping.equals(anotherTrack.grouping)))
		{
			return composerMatch(composer, anotherTrack.composer);
		}
		else
		{
			// no grouping or different group
			return false;
		}
	}

    // for sorting
    @Override
    public int compareTo(@NonNull AudioTrack another)
    {
        if (albumName != null && albumName.equals(another.albumName))
        {
            return Integer.compare(trackno, another.trackno);
        }
        else
        if (albumName != null)
        {
            return albumName.compareTo(another.albumName);
        }
        else
        {
            return title.compareTo(another.title);
        }
    }
}
