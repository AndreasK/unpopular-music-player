/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.app.Service;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
//import android.media.session.MediaSession;
import android.support.v4.media.session.MediaSessionCompat;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import de.kromke.andreas.utilities.MediaPlayBtInfo;


/*
 * A service keeps running while our application is
 * suspended. Handles MediaPlayer and Notifications.
 *
 * Note that activity may be recreated and that even
 * process may be restarted while notifications are
 * being displayed.
 */

/** @noinspection JavadocBlankLines*/
public class MediaPlayService extends Service implements
        MyMediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MyMediaPlayer.OnCompletionListener,
        MediaPlayBtInfo.MediaPlayBtRemoteController
{
    private static final String LOG_TAG = "UMPL : MediaPlayService";

    private enum MediaPlayerState
    {
        STOPPED, PLAYING_PREPARED, PLAYING, PAUSED
    }
    MediaPlayerState mMediaPlayerState = MediaPlayerState.STOPPED;

    // interface (in fact kind a set of callback functions)
    MediaPlayServiceControl mMediaPlayServiceController = null;
    private MyMediaPlayer mMediaPlayer;         // Android media player, extended to catch exceptions
    private MyMediaPlayer mMediaPlayerNext;     // second player for "gapless" playback
    private boolean mbMediaPlayerNextPrepared;
    //current position
    private int mCurrTrackArrayNumber = -1;
    private int mNextTrackArrayNumber = -1;
    //binder
    private final IBinder mMediaPlayerBind = new MediaPlayServiceBinder();
    //title of current audio file
    private String mAudioInformationFirstLine = "";
    private String mAudioInformationSecondLine = "";
    private String mAudioInformationThirdLine = "";
    // BT sink management and remote control, needs Android 6 and higher (API 23)
    MediaPlayBtInfo mBtInfo = null;
    MediaPlayNotification mNotification = null;

    //timer for play position updates
    private Timer mTimer = null;

    /*
    * callback mechanism to activity
     */
    interface MediaPlayServiceControl
    {
        void onNotificationPlayPauseChanged();
        void onNotificationTrackChanged(int newTrackNo);    // prev, next and stop
    }


    /* #######################
     * ### service methods ###
     * #######################
     */

    /**************************************************************************
     *
     * Service function
     *
     * run by the system if someone calls Context.startService()
     *
     *************************************************************************/
    public void onCreate()
    {
        Log.d(LOG_TAG, "onCreate()");

        //create the service
        super.onCreate();
        //initialize position
        mCurrTrackArrayNumber = 0;
        //create mMediaPlayer
        mMediaPlayer = createMediaPlayer();
        mMediaPlayerNext = createMediaPlayer();

        mNotification = new MediaPlayNotification(this, TracksOfAlbumActivity.class);
    }


    /**************************************************************************
     *
     * helper function for play/pause
     *
     *************************************************************************/
    private void onPlayPause(boolean isplay)
    {
        if (isplay)
        {
            continuePlayer();
        }
        else
        {
            pausePlayer();
        }

        if (mMediaPlayServiceController != null)
        {
            // just tell activity, if listening
            mMediaPlayServiceController.onNotificationPlayPauseChanged();
        }
    }


    /**************************************************************************
     *
     * helper function for seek
     *
     *************************************************************************/
    private void onSeek(long position)
    {
        seek((int) position);
        // TODO: notification to activity?
    }


    /**************************************************************************
     *
     * helper function for stop
     *
     *************************************************************************/
    private void onStop()
    {
        mCurrTrackArrayNumber = -1;
        stopPlayer();

        if (mMediaPlayServiceController != null)
        {
            mMediaPlayServiceController.onNotificationTrackChanged(mCurrTrackArrayNumber);
        }
    }


    /**************************************************************************
     *
     * helper function for next/prev/advance
     *
     * delta = -1: prev
     * delta = 1: next and advance (automatic next)
     *
     *************************************************************************/
    private void onChgTrack(int delta)
    {
        Log.d(LOG_TAG, "onChgTrack(delta = " + delta + ")");
        int nextTrackNo = getNextTrackArrayNumber(delta, false);
        if ((mCurrTrackArrayNumber >= 0) && (nextTrackNo < 0) && (isPlaying()))
        {
            stopPlayer();
        }
        if (nextTrackNo >= 0)
        {
            playAudioTrack(nextTrackNo, 0);
        }
        else
        {
            mCurrTrackArrayNumber = -1;
        }

        if (mMediaPlayServiceController != null)
        {
            mMediaPlayServiceController.onNotificationTrackChanged(mCurrTrackArrayNumber);
        }
    }


    /**************************************************************************
     *
     * Service function, e.g. called by control elements on the notification
     *
     * Note that this function may be called after restart of the process,
     * and in this case there is no activity that controls this service, and
     * the global variables including the playlist etc. has not been
     * initialised.
     *
     *************************************************************************/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.d(LOG_TAG, "onStartCommand(startId = " + startId + ")");

        /*
         * handle error cases like null intent or illegal action
         */

        if (intent == null)
        {
            Log.w(LOG_TAG, "onStartCommand() : intent is null");
            return Service.START_STICKY;
        }
        final String theAction = intent.getAction();
        if (theAction == null)
        {
            Log.w(LOG_TAG, "onStartCommand() : action is null");
            return Service.START_STICKY;
        }

        /*
         * repair user settings, if our process had been restarted in the meantime
         */

        UserSettings.init(getApplicationContext());

        /*
         * get extra information to re-initialise the track list
         * if necessary
         */

        long albumId = -1;

        if (intent.hasExtra(MediaPlayNotification.NOTIFICATION_TRACK_ARRAY_NO))
        {
            int trackArrayNo = intent.getIntExtra(MediaPlayNotification.NOTIFICATION_TRACK_ARRAY_NO, -1);
            Log.i(LOG_TAG, "onStartCommand() : NOTIFICATION_TRACK_ARRAY_NO is " + trackArrayNo);
            if (trackArrayNo != mCurrTrackArrayNumber)
            {
                Log.i(LOG_TAG, "onStartCommand() : track array no changed from " + mCurrTrackArrayNumber + " to " + trackArrayNo);
                mCurrTrackArrayNumber = trackArrayNo;
            }
        }
        if (intent.hasExtra(MediaPlayNotification.NOTIFICATION_ALBUM_ID))
        {
            albumId = intent.getLongExtra(MediaPlayNotification.NOTIFICATION_ALBUM_ID, -1);
            Log.i(LOG_TAG, "onStartCommand() : NOTIFICATION_ALBUM_ID is " + albumId);
            if (albumId != AppGlobals.sCurrAlbumId)
            {
                Log.i(LOG_TAG, "onStartCommand() : audio album id changed from " + AppGlobals.sCurrAlbumId + " to " + albumId);
            }
        }

        /*
         * check if onCreate() has been called or if we lost all our variables
         */

        if ((mMediaPlayer == null ) || (mNotification == null))
        {
            Log.e(LOG_TAG, "onStartCommand() : onCreate has not been called?!?");
            removeNotificationAndStopBt();
            return Service.START_STICKY;
        }

        /*
         * in case the process has been restarted, re-initialise globals
         */

        if (albumId >= 0)
        {
            // init and restore album and track lists
            Context context = getApplicationContext();
            boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
            String ownDatabasePath;
            if (bUseOwnDatabase)
            {
                ownDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);  // might be null
            }
            else
            {
                ownDatabasePath = null;
            }
            boolean bUseJaudioTaglib = UserSettings.getBool(UserSettings.PREF_USE_JAUDIOTAGGER_LIB, false);
            int albumNo = AppGlobals.initAppGlobals(context, albumId, ownDatabasePath, false, bUseJaudioTaglib);
            if (albumNo < 0)
            {
                Log.e(LOG_TAG, "onStartCommand() : album id not found");
            }
        }
        if (AppGlobals.sCurrAlbumId < 0)
        {
            Log.e(LOG_TAG, "onStartCommand() : illegal album id");
            removeNotificationAndStopBt();
            return Service.START_STICKY;
        }

        switch (theAction)
        {
            case MediaPlayNotification.NOTIFICATION_PREV_ACTION:
                Log.i(LOG_TAG, "Clicked Previous");
                onChgTrack(-1);
                break;

            case MediaPlayNotification.NOTIFICATION_PLAY_ACTION:
                Log.i(LOG_TAG, "Clicked Play resp. Pause");
                onPlayPause(!isPlaying());
                break;

            case MediaPlayNotification.NOTIFICATION_NEXT_ACTION:
                Log.i(LOG_TAG, "Clicked Next");
                onChgTrack(1);
                break;

            case MediaPlayNotification.NOTIFICATION_STOP_ACTION:
                Log.i(LOG_TAG, "Clicked Stop");
                onStop();
                break;

            default:
                Log.i(LOG_TAG, "Unknown intent");
                break;
        }

        return START_STICKY;
    }


    /**************************************************************************
     *
     * Service function: Pendant to onCreate()
     *
     * Note that this function is not always called
     *
     *************************************************************************/
    @Override
    public void onDestroy()
    {
        Log.d(LOG_TAG, "onDestroy()");
        TimerStop();

        if (mMediaPlayer.isPlayingEx())
        {
            mMediaPlayer.stop();
        }
        mMediaPlayer.release();

        if (mMediaPlayerNext.isPlayingEx())
        {
            mMediaPlayerNext.stop();
        }
        mMediaPlayerNext.release();

        removeNotificationAndStopBt();
    }


    /**************************************************************************
     *
     * Service function: will bind to service
     *
     *************************************************************************/
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.d(LOG_TAG, "onBind()");
        return mMediaPlayerBind;
    }


    /**************************************************************************
     *
     * Service function: release resources
     *
     *************************************************************************/
    @Override
    public boolean onUnbind(Intent intent)
    {
        Log.d(LOG_TAG, "onUnbind()");
        mMediaPlayServiceController = null;
        return false;
    }


    /**************************************************************************
     *
     * called once from onCreate()
     *
     *************************************************************************/
    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    public MyMediaPlayer createMediaPlayer()
    {
        MyMediaPlayer theMediaPlayer = new MyMediaPlayer();
        //set mMediaPlayer properties
        theMediaPlayer.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
        {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            theMediaPlayer.setAudioAttributes(attributes);
        }
        else
        {
            theMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }

        //set listeners
        theMediaPlayer.setOnPreparedListener(this);
        theMediaPlayer.setOnCompletionListener(this);
        theMediaPlayer.setOnErrorListener(this);
        if (theMediaPlayer.isPlayingEx())
        {
            mMediaPlayerState = MediaPlayerState.PLAYING;
        }

        return theMediaPlayer;
    }


    /**************************************************************************
     *
     * binder
     *
     *************************************************************************/
    class MediaPlayServiceBinder extends Binder
    {
        MediaPlayService getService()
        {
            return MediaPlayService.this;
        }
    }


    /**************************************************************************
     *
     * set Bluetooth information (supported with Android 5 and later)
     *
     *************************************************************************/
    private void setBtInfo(AudioTrack currAudioTrack, int size)
    {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            String btAlbum = AppGlobals.sCurrAlbum.album_name;
            String btTitle = currAudioTrack.title;
            String btInterpreter = currAudioTrack.performer;

            String composer = currAudioTrack.composer;
            if (!composer.isEmpty())
            {
                composer += ": ";
            }
            String grouping = currAudioTrack.grouping;
            String composer_and_work = composer + ((!grouping.isEmpty()) ? grouping : btTitle);

            int btMetadataStyle = UserSettings.getVal(UserSettings.PREF_BLUETOOTH_METADATA_STYLE, 0);
            switch (btMetadataStyle)
            {
                case 1:
                    // Berlingo 2014: no album, just title and performer

                    if (grouping.isEmpty())
                    {
                        // no movements: just precede title with composer

                        btTitle = composer + btTitle;
                    } else
                    {
                        // movements: show work instead of title and movement instead of performer

                        btInterpreter = btTitle;
                        btTitle = composer + grouping;
                    }
                    break;

                case 2:
                    // use album for work and composer
                    if (!composer.isEmpty() || !grouping.isEmpty())
                    {
                        // composer/work -> album
                        btAlbum = composer + grouping;
                    }
                    break;

                case 0:
                default:
                    break;
            }

            mBtInfo = new MediaPlayBtInfo(
                    this,
                    getApplicationContext(),
                    currAudioTrack.id,
                    btInterpreter,
                    btAlbum,
                    composer_and_work,
                    btTitle,
                    currAudioTrack.composer,
                    AppGlobals.sCurrAlbumImageDrawable,
                    currAudioTrack.duration,
                    mCurrTrackArrayNumber,          // 0..n-1
                    size);
        }
    }

    /**************************************************************************
     *
     * set data source for player
     *
     *************************************************************************/
    private boolean setDataSource(MyMediaPlayer player, AudioTrack currAudioTrack)
    {
        if ((AudioTrack.sbUsingAndroidDb) && (currAudioTrack.id >= 0))
        {
            // create URI from Android's track id
            Uri trackUri = ContentUris.withAppendedId(
                    android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    currAudioTrack.id);
            player.setDataSource(getApplicationContext(), trackUri);
        }
        else
        if (SafUtilities.isSafPath(currAudioTrack.path))
        {
            // this is no path, but an Uri
            //noinspection RedundantIfStatement
            if (player.setDataSourceWithUri(this, currAudioTrack.path) != 0)
            {
                return false;
            }
        }
        else
        {
            // not using Android DB, must use path instead
            //noinspection RedundantIfStatement
            if (player.setDataSourceWithPath(currAudioTrack.path) != 0)
            {
                return false;
            }
        }

        return true;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void prepareNextTrack()
    {
        mNextTrackArrayNumber = getNextTrackArrayNumber(1, false);
        AudioTrack nextAudioTrack = (mNextTrackArrayNumber >= 0) ? AppGlobals.sCurrAlbumTrackList.get(mNextTrackArrayNumber) : null;
        mbMediaPlayerNextPrepared = false;

        if (nextAudioTrack != null)
        {
            if (setDataSource(mMediaPlayerNext, nextAudioTrack))
            {
                mMediaPlayerNext.prepareAsyncEx(0);
            }
            else
            {
                Log.e(LOG_TAG, "prepareNextTrack() -- data source of next track is bad");
                mMediaPlayer.setNextMediaPlayerEx(null);
            }
        }
        else
        {
            mMediaPlayer.setNextMediaPlayerEx(null);
        }
    }


    /**************************************************************************
     *
     * play the current audio track
     *
     * Note that the media player starts asynchronously
     *
     *************************************************************************/
    public void playAudioTrack(int trackArrayNumber, long pos)
    {
        Log.d(LOG_TAG, "playAudioTrack(" + trackArrayNumber + ", pos=" + pos + ")");

        mMediaPlayerState = MediaPlayerState.STOPPED;
        mMediaPlayer.reset();
        mMediaPlayerNext.reset();

        if (trackArrayNumber < 0)
        {
            mCurrTrackArrayNumber = mNextTrackArrayNumber = -1;
            Log.e(LOG_TAG, "playAudioTrack() : invalid track number");
            return;
        }

        if (AppGlobals.sCurrAlbum == null)
        {
            mCurrTrackArrayNumber = mNextTrackArrayNumber = -1;
            Log.e(LOG_TAG, "playAudioTrack() : invalid album");
            return;
        }

        int size = AppGlobals.getAudioAlbumTrackListSize();
        if (trackArrayNumber >= size)
        {
            if (AppGlobals.sCurrAlbumTrackList == null)
            {
                Log.e(LOG_TAG, "playAudioTrack() : empty track list");
            }
            else
            {
                Log.e(LOG_TAG, "playAudioTrack() : track number overflow");
            }
            mCurrTrackArrayNumber = mNextTrackArrayNumber = -1;
            return;
        }

        //get audio track information
        mCurrTrackArrayNumber = trackArrayNumber;
        AudioTrack currAudioTrack = AppGlobals.sCurrAlbumTrackList.get(mCurrTrackArrayNumber);
        if (!setDataSource(mMediaPlayer, currAudioTrack))
        {
            return;     // error
        }

        setBtInfo(currAudioTrack, size);
        setNotificationInfoLines(currAudioTrack);
        TimerStart();

        // start asynchronously, later onPrepared() will be called
        // mMediaPlayerNext will be prepared in callback
        if (mMediaPlayer.prepareAsyncEx(pos))
        {
            mMediaPlayerState = MediaPlayerState.PLAYING_PREPARED;
        }
    }


    /**************************************************************************
     *
     * called from activity to register for callback notifications
     *
     *************************************************************************/
    public void setController(MediaPlayServiceControl controller)
    {
        mMediaPlayServiceController = controller;
    }


    /**************************************************************************
     *
     * playback methods
     *
     *************************************************************************/
    public int getPosition()
    {
        if (mMediaPlayer != null)
        {
            return mMediaPlayer.getCurrentPosition();
        }
        else
        {
            return 0;
        }
    }

    public int getDuration()
    {
        if (mMediaPlayer != null)
        {
            return mMediaPlayer.getDuration();
        }
        else
        {
            return 0;
        }
    }

    public boolean isPlaying()
    {
        if (mMediaPlayerState == MediaPlayerState.PLAYING_PREPARED)
        {
            // note that the media play tells "false" here which takes the activity's play state out of sync
            return true;
        }

        boolean is = false;
        if (mMediaPlayer != null)
        {
            is = mMediaPlayer.isPlayingEx();
        }
        return is;
    }

    public boolean isPaused()
    {
        return (mMediaPlayerState == MediaPlayerState.PAUSED);
    }

    public int getAudioTrackArrayNumber() { return mCurrTrackArrayNumber; }

    public void pausePlayer()
    {
        mMediaPlayerState = MediaPlayerState.PAUSED;
        TimerStop();

        // change notification to show PAUSE instead of PLAY icon
        if (mBtInfo != null)
        {
            mBtInfo.sendPlayState(getApplicationContext(), false /* pause */, getDuration(), getPosition());
            setNotification(false /* pause */, mBtInfo.getMediaSession());
        }
        else
        {
            setNotification(false /* pause */, null);
        }

        mMediaPlayer.pause();
    }

    public void continuePlayer()
    {
        TimerStart();

        // change notification to show PLAY instead of PAUSE icon
        if (mBtInfo != null)
        {
            mBtInfo.sendPlayState(getApplicationContext(), true /* play */, getDuration(), getPosition());
            setNotification(true /* play */, mBtInfo.getMediaSession());
        }
        else
        {
            setNotification(true /* play */, null);
        }

        if (mMediaPlayer.startEx())
        {
            mMediaPlayerState = MediaPlayerState.PLAYING;
        }
    }

    private void removeNotificationAndStopBt()
    {
        if (mNotification != null)
        {
            mNotification.removeNotification();
        }
        if (mBtInfo != null)
        {
            mBtInfo.stop(getApplicationContext());
        }
    }

    public void stopPlayer()
    {
        mMediaPlayerState = MediaPlayerState.STOPPED;
        TimerStop();

        removeNotificationAndStopBt();
        mMediaPlayer.pause();
    }

    public void seek(int posn)
    {
        if (mMediaPlayer != null)
        {
            mMediaPlayer.seekTo(posn);
        }
    }


    /**************************************************************************
     *
     * skip to next (delta = 1) or previous (delta = -1) track, but do not play, yet
     *
     *************************************************************************/
    public int getNextTrackArrayNumber(int delta, boolean wrap)
    {
        int nextTrackArrayNumber = mCurrTrackArrayNumber + delta;
        Log.d(LOG_TAG, "getNextTrackArrayNumber() -- " + mCurrTrackArrayNumber + " -> " + nextTrackArrayNumber);
        int size = AppGlobals.getAudioAlbumTrackListSize();

        if (wrap)
        {
            // keep track number in valid range
            if (nextTrackArrayNumber < 0)
            {
                nextTrackArrayNumber = size - 1;
            }
            if (nextTrackArrayNumber >= size)
            {
                nextTrackArrayNumber = 0;
            }
        }

        if ((nextTrackArrayNumber < 0) || (nextTrackArrayNumber >= size))
        {
            // invalid
            nextTrackArrayNumber = -1;
        }

        Log.d(LOG_TAG, "getNextTrackArrayNumber() -- next track is " + nextTrackArrayNumber);
        return nextTrackArrayNumber;
    }


    /* ###########################################
     * ### callback functions from MediaPlayer ###
     * ###########################################
     */


    /**************************************************************************
     *
     * media player callback for asynchronous start
     *
     *************************************************************************/
    @Override
    public void onPrepared(MyMediaPlayer mp)
    {
        Log.d(LOG_TAG, "onPrepared()");

        if (mp == mMediaPlayer)
        {
            Log.d(LOG_TAG, "onPrepared() -- this is the current player");
            prepareNextTrack();

            //start playback
            if (mp.startEx())
            {
                mMediaPlayerState = MediaPlayerState.PLAYING;
            }

            if (mBtInfo != null)
            {
                mBtInfo.sendPlayState(getApplicationContext(), true /* play */, getDuration(), getPosition());
                setNotification(true /* play */, mBtInfo.getMediaSession());
            } else
            {
                setNotification(true /* play */, null);
            }
        }
        else
        {
            if (mMediaPlayer.setNextMediaPlayerEx(mMediaPlayerNext))
            {
                mbMediaPlayerNextPrepared = true;
                Log.d(LOG_TAG, "onPrepared() -- this is the next player");
            }
            else
            {
                mbMediaPlayerNextPrepared = false;
                Log.e(LOG_TAG, "onPrepared() -- we have a problem");
            }
        }
    }


    /**************************************************************************
     *
     * media player callback when playback has ended normally
     *
     *************************************************************************/
    @Override
    public void onCompletion(MyMediaPlayer mp)
    {
        Log.d(LOG_TAG, "onCompletion()");
        if (mp == mMediaPlayer)
        {
            Log.d(LOG_TAG, "onCompletion() -- this is the current player");
            mp.reset();
            mMediaPlayerState = MediaPlayerState.STOPPED;
            //removeNotification();       // TODO: update notification, do not remove, if there is a next track to play

            if (mbMediaPlayerNextPrepared)
            {
                // the next player is now the current one, i.e. we exchange the player objects
                Log.d(LOG_TAG, "onCompletion() -- exchange player objects");
                MyMediaPlayer temp = mMediaPlayer;
                mMediaPlayer = mMediaPlayerNext;
                mMediaPlayerNext = temp;
                mCurrTrackArrayNumber = mNextTrackArrayNumber;
                mMediaPlayerState = MediaPlayerState.PLAYING;
                prepareNextTrack();
            }
            else
            {
                Log.d(LOG_TAG, "onCompletion() -- no next player. Stop.");
                mCurrTrackArrayNumber = -1;
                stopPlayer();
            }

            if (mMediaPlayServiceController != null)
            {
                mMediaPlayServiceController.onNotificationTrackChanged(mCurrTrackArrayNumber);
            }

            if (mCurrTrackArrayNumber >= 0)
            {
                AudioTrack currAudioTrack = AppGlobals.sCurrAlbumTrackList.get(mCurrTrackArrayNumber);
                setBtInfo(currAudioTrack, AppGlobals.getAudioAlbumTrackListSize());
                setNotificationInfoLines(currAudioTrack);

                if (mBtInfo != null)
                {
                    mBtInfo.sendPlayState(getApplicationContext(), true /* play */, getDuration(), getPosition());
                    setNotification(true /* play */, mBtInfo.getMediaSession());
                } else
                {
                    setNotification(true /* play */, null);
                }
            }
        }
        else
        {
            Log.e(LOG_TAG, "onCompletion() -- this is the next player");
        }
    }


    /**************************************************************************
     *
     * media player callback when playback has been ended with error
     *
     *************************************************************************/
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        Log.v(LOG_TAG, "onError()");
        mMediaPlayerState = MediaPlayerState.STOPPED;
        mp.reset();
        return false;
    }


    /* ###########################################################
     * ### callback functions from MediaPlayBtRemoteController ###
     * ###########################################################
     */

    public void onMediaControlPlayState(boolean isplay)
    {
        Log.d(LOG_TAG, "media control play state " + isplay);
        onPlayPause(isplay);
    }

    public void onMediaControlSkip(int direction)
    {
        Log.d(LOG_TAG, "media control skip " + direction);
        onChgTrack(direction);
    }

    public void onMediaControlSeek(long position)
    {
        Log.d(LOG_TAG, "media control seek " + position);
        onSeek(position);
    }

    public void onMediaControlStop()
    {
        Log.d(LOG_TAG, "media control stop");
        onStop();
    }


    /**************************************************************************
     *
     * timer for position updates
     *
     *************************************************************************/
    private class PositionTimerTask extends TimerTask
    {
        public void run()
        {
            if (mBtInfo != null)
            {
                if (mMediaPlayerState == MediaPlayerState.STOPPED)
                {
                    mBtInfo.sendPlayState(getApplicationContext(), false, 0, 0);
                }
                else
                {
                    //Log.v(LOG_TAG, "PositionTimerTask() : position = " + getPosition());
                    mBtInfo.sendPlayState(getApplicationContext(), isPlaying(), getDuration(), getPosition());
                }
            }
        }
    }

    private void TimerStart()
    {
        // delete old timer, if any
        TimerStop();
        //create new timer
        mTimer = new Timer();
        // start position timer
        mTimer.scheduleAtFixedRate(new PositionTimerTask(), 1000 /* 1s delay */, 1000 /* every 1s */);
    }

    private void TimerStop()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
        }
    }


    /**************************************************************************
     *
     * set the information lines for the notification
     *
     *************************************************************************/
    private void setNotificationInfoLines(AudioTrack currAudioTrack)
    {
        if (!currAudioTrack.composer.isEmpty())
        {
            mAudioInformationFirstLine = currAudioTrack.composer + ":";
        } else
        {
            mAudioInformationFirstLine = AppGlobals.getGenderedUnknownComposer(this);
        }

        if (!currAudioTrack.grouping.isEmpty())
        {
            mAudioInformationSecondLine = currAudioTrack.grouping;
            mAudioInformationThirdLine = currAudioTrack.title;
        } else
        {
            mAudioInformationSecondLine = currAudioTrack.title;
            mAudioInformationThirdLine = "";
        }
    }


    /**************************************************************************
     *
     * set notification or update current one, if any
     *
     * Old implementation used
     *    startForeground(NOTIFY_ID, theNotification, FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK);
     * and
     *    stopForeground()
     *
     * This does no longer work with Android 12 and newer when existing notifications must
     * be replaced due to track change when playing in background. Instead, we
     * now call
     *    mNotificationManager.notify(MY_NOTIFICATION_ID, theNotification);
     * and
     *    mNotificationManager.cancel(MY_NOTIFICATION_ID);
     * which basically seem to do the same.
     *
     *************************************************************************/
    private void setNotification(boolean isPlay, MediaSessionCompat mediaSession)
    {
        //Log.d(LOG_TAG, "setNotification() : trk = " + mCurrTrackArrayNumber);
        mNotification.createAndShowNotification(
                                                isPlay,
                                                mAudioInformationFirstLine,
                                                mAudioInformationSecondLine,
                                                mAudioInformationThirdLine,
                                                mCurrTrackArrayNumber,
                                                mediaSession,
                                                this);
    }

}
