/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;


import static androidx.core.app.NotificationCompat.CATEGORY_TRANSPORT;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import androidx.core.app.NotificationCompat;
import androidx.media.app.NotificationCompat.MediaStyle;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Icon;
import android.media.session.MediaSession;
import android.os.Build;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.widget.RemoteViews;


/** @noinspection JavadocBlankLines, JavadocLinkAsPlainText */
@SuppressWarnings("WeakerAccess")
public class MediaPlayNotification
{
    private static final String LOG_TAG = "UMPL : MedPlayNotif";

    public final static String NOTIFICATION_MAIN_ACTION = "de.kromke.andreas.unpopmusicplayerfree.customnotification.action.main";
    public final static String NOTIFICATION_PREV_ACTION = "de.kromke.andreas.unpopmusicplayerfree.customnotification.action.prev";
    public final static String NOTIFICATION_PLAY_ACTION = "de.kromke.andreas.unpopmusicplayerfree.customnotification.action.play";
    public final static String NOTIFICATION_NEXT_ACTION = "de.kromke.andreas.unpopmusicplayerfree.customnotification.action.next";
    public final static String NOTIFICATION_STOP_ACTION = "de.kromke.andreas.unpopmusicplayerfree.customnotification.action.stop";

    public final static String NOTIFICATION_ALBUM_ID       = "de.kromke.andreas.unpopmusicplayerfree.customnotification.album_id";
    public final static String NOTIFICATION_TRACK_ARRAY_NO = "de.kromke.andreas.unpopmusicplayerfree.customnotification.track_array_no";

    private final PendingIntent mPendingNotificationIntent;
    private final static String NOTIFICATION_CHANNEL_ID = "de.kromke.andreas.unpopmusicplayerfree";
    private final static String channelName = "Playback Service";
    private final static int MY_NOTIFICATION_ID = 520;      // arbitrary number, as we only have on at a time
    private final NotificationManager mNotificationManager;
    private RemoteViews mCustomBigViewForAndroid6;          // only needed for Android 6 and older



    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    static int getPendingFlags()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            return PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_IMMUTABLE;
        }
        else
        {
            return PendingIntent.FLAG_UPDATE_CURRENT;
        }
    }


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    @SuppressLint("UnspecifiedImmutableFlag")
    MediaPlayNotification
    (
        Context context,
        Class theClass
    )
    {
        //
        // Create pendingIntent for later use.
        // Note that this intent will be sent to the activity, not to us
        //
        Intent notificationIntent = new Intent(context, theClass);
        notificationIntent.setAction(NOTIFICATION_MAIN_ACTION);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mPendingNotificationIntent = PendingIntent.getActivity(
                context, 0, notificationIntent, getPendingFlags());
        //
        // The Notification Manager is needed to create a Notification Channel and
        // to update existing notifications.
        //
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    /**************************************************************************
     *
     * adds essential necessary information to intent, so that the
     * handler can get any needed information to recover from
     * process restart, ie. loss of any global or static variables.
     *
     *************************************************************************/
    private PendingIntent createMediaPlayIntent
    (
        final String action,
        int currTrackArrayNumber,
        Context context
    )
    {
        Intent theIntent = new Intent(context, MediaPlayService.class);
        theIntent.setAction(action);
        theIntent.putExtra(NOTIFICATION_TRACK_ARRAY_NO, currTrackArrayNumber);
        theIntent.putExtra(NOTIFICATION_ALBUM_ID, AppGlobals.sCurrAlbumId);
        // for whatever reason PendingIntent.FLAG_UPDATE_CURRENT is needed to pass extra parameters, otherwise they are ignored
        return PendingIntent.getService(context, 0, theIntent, getPendingFlags());
    }


    /**************************************************************************
     *
     * add action to notification builder
     *
     *************************************************************************/
    @TargetApi(23)
    private void addAction
    (
        NotificationCompat.Builder builder,
        int resId,
        final String title,     // no idea what this is used for
        final String action,
        int currTrackArrayNumber,
        Context context
    )
    {
        Icon icon = Icon.createWithResource(context, resId);
        PendingIntent theIntent = createMediaPlayIntent(action, currTrackArrayNumber, context);
        builder.addAction(resId, title, theIntent);
    }


    /**************************************************************************
     *
     * add Media Style to notification builder
     *
     *************************************************************************/
    private void addMediaStyle
    (
        NotificationCompat.Builder builder,
        boolean isPlay,
        int currTrackArrayNumber,
        MediaSessionCompat mediaSession,
        Context context
    )
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            //builder.setSmallIcon(R.drawable.app_icon_noborder);
            if (AppGlobals.sCurrAlbumImageDrawable != null)
            {
                builder.setLargeIcon(((BitmapDrawable) AppGlobals.sCurrAlbumImageDrawable).getBitmap());
            }
            // On Android 6 the notification will be tinted in an ugly dark gray.
            // Does not happen with Android 7
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M)
            {
                builder.setColor(0x9e9e9e);
            }
            MediaStyle mediaStyle = new MediaStyle();
            mediaStyle.setShowActionsInCompactView(1);

            builder.setStyle(mediaStyle);
            if (mediaSession != null)
            {
                MediaSessionCompat.Token token = mediaSession.getSessionToken();
                builder.setStyle(new MediaStyle().setMediaSession(token));
            }

            int playPauseId = (!isPlay) ?
                    R.drawable.notification_play :
                    R.drawable.notification_pause;
            addAction(builder, R.drawable.notification_prev,  "PREV", NOTIFICATION_PREV_ACTION, currTrackArrayNumber, context);
            addAction(builder, playPauseId,                   "PLAY", NOTIFICATION_PLAY_ACTION, currTrackArrayNumber, context);
            addAction(builder, R.drawable.notification_next,  "NEXT", NOTIFICATION_NEXT_ACTION, currTrackArrayNumber, context);
            addAction(builder, R.drawable.notification_close, "STOP", NOTIFICATION_STOP_ACTION, currTrackArrayNumber, context);
        }
    }


    /**************************************************************************
     *
     * Android >= 8.1 needs notification channels
     *
     * @noinspection RedundantSuppression
     *************************************************************************/
    @SuppressWarnings("deprecation")
    private NotificationCompat.Builder createNotificationBuilder(Context context)
    {
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
            // note that IMPORTANCE_NONE works fine on any device except Samsung. Damn!
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            if (mNotificationManager != null)
            {
                mNotificationManager.createNotificationChannel(chan);
            }
            else
            {
                Log.e(LOG_TAG, "no Notification Manager");
            }
        }
        else
        {
            // deprecated, but needed for older APIs
            builder = new NotificationCompat.Builder(context);
        }

        return builder;
    }


    /**************************************************************************
     *
     * create a generic (non custom) Notification Builder
     *
     * Note that with API 33 (Android 13) for Media Notifications:
     *
     *  setSmallIcon() shows the icon at the upper left corner, but not in status bar
     *  setTicker(), setContentTitle(), setContent(Sub)Text() are ignored
     *
     * For API 33 Media Style Notifications some information is instead taken
     * from Media Session Metadata:
     *
     *  METADATA_KEY_DISPLAY_TITLE is preferred to
     *  METADATA_KEY_TITLE for the track name
     *  METADATA_KEY_ARTIST is shown
     *  METADATA_KEY_ALBUM is ignored
     *  METADATA_KEY_COMPOSER is ignored
     *  everything else is ignored
     *
     *
     *************************************************************************/
    private NotificationCompat.Builder createGenericNotificationBuilder
    (
        boolean isPlay,
        boolean isMediaStyle,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        MediaSessionCompat mediaSession,
        Context context
    )
    {
        NotificationCompat.Builder builder = createNotificationBuilder(context);
        int playPauseIcon = (isPlay) ?
                R.drawable.play :
                R.drawable.pause;
        if (Build.VERSION.SDK_INT >= 33)
        {
            if (isMediaStyle)
            {
                playPauseIcon = R.drawable.app_icon_noborder_white;
            }
        }

        builder.setContentIntent(mPendingNotificationIntent)    // intent to be sent when the notification is clicked on
                .setSmallIcon(playPauseIcon)                    // MANDATORY: used for menu bar and, with Android N, inside notification
                .setTicker(audioInformationSecondLine)          // (for accessibility services or ignored)
                .setOngoing(true)                               // notification cannot be dismissed
                .setShowWhen(false)                             // do not show time of the day when notification was sent
                .setContentTitle(audioInformationFirstLine)     // "composer:", ignored in Android 13
                .setContentText(audioInformationSecondLine)     // work, ignored in Android 13
                .setSubText(audioInformationThirdLine);         // movement, ignored in Android 13
        if (Build.VERSION.SDK_INT > 20)
        {
            // show notification on lock screen
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        }

        if (isMediaStyle)
        {
            // Media Style (extended generic)
            addMediaStyle(builder, isPlay, currTrackArrayNumber, mediaSession, context);
        }

        return builder;
    }


    /************************************************************************************
     *
     * helper to handle API differences (Android 7 or older)
     *
     * @noinspection RedundantSuppression
     ***********************************************************************************/
    @SuppressWarnings("deprecation")
    private NotificationCompat.Builder createNotificationBuilderWithCustomViews
    (
        Context context,
        RemoteViews customSmallView,
        RemoteViews customBigView
    )
    {
        NotificationCompat.Builder builder = createNotificationBuilder(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            builder.setCustomContentView(customSmallView);
            if (customBigView != null)
            {
                builder.setCustomBigContentView(customBigView);
            }
        }
        else
        {
            // Deprecated, but needed for Android 6 and older.
            // Ignore big view here, will be added later to notification.
            builder.setContent(customSmallView);
        }

        return builder;
    }


    /**************************************************************************
     *
     * helper for custom notifications
     *
     *************************************************************************/
    private RemoteViews createCustomSmallView
    (
        boolean isPlay,
        boolean bUseCompactNotification,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        Context context,
        Bitmap theAlbumBitmap,
        PendingIntent ppreviousIntent,
        PendingIntent pplayIntent,
        PendingIntent pnextIntent,
        PendingIntent pcloseIntent
    )
    {
        int resid = (bUseCompactNotification) ? R.layout.notification_compact : R.layout.notification;
        RemoteViews customSmallView = new RemoteViews(context.getPackageName(), resid);

        // showing album image or default album image
        if (theAlbumBitmap != null)
        {
            customSmallView.setImageViewBitmap(R.id.album_art, theAlbumBitmap);
        }

        // showing play or pause, Note that !isPlay means status pause, so show play icon and vice versa
        int playPauseId = (isPlay) ? R.drawable.notification_pause : R.drawable.notification_play;
        if (bUseCompactNotification)
        {
            int playPauseIdCompact = (isPlay) ? R.drawable.notification_ic_pause : R.drawable.notification_ic_play;
            customSmallView.setImageViewResource(R.id.button_play_pause, playPauseIdCompact);
        } else
        {
            customSmallView.setImageViewResource(R.id.button_play_pause, playPauseId);
        }

        // three text lines

        customSmallView.setTextViewText(R.id.composer_name, audioInformationFirstLine);
        customSmallView.setTextViewText(R.id.work_name, audioInformationSecondLine);
        customSmallView.setTextViewText(R.id.track_name, audioInformationThirdLine);

        // HACK. No idea why text colour is white by default
        if (Build.VERSION.SDK_INT > 20)
        {
            customSmallView.setTextColor(R.id.composer_name, Color.BLACK);
            customSmallView.setTextColor(R.id.work_name, Color.DKGRAY);
            customSmallView.setTextColor(R.id.track_name, Color.GRAY);
        }

        customSmallView.setOnClickPendingIntent(R.id.button_play_pause, pplayIntent);
        if (!bUseCompactNotification)
        {
            customSmallView.setOnClickPendingIntent(R.id.button_next, pnextIntent);
            customSmallView.setOnClickPendingIntent(R.id.button_prev, ppreviousIntent);
            customSmallView.setOnClickPendingIntent(R.id.button_close, pcloseIntent);
        }

        return customSmallView;
    }


    /**************************************************************************
     *
     * helper for custom notifications
     *
     *************************************************************************/
    private RemoteViews createCustomBigView
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        Context context,
        Bitmap theAlbumBitmap,
        PendingIntent ppreviousIntent,
        PendingIntent pplayIntent,
        PendingIntent pnextIntent,
        PendingIntent pcloseIntent
    )
    {
        RemoteViews customBigView = new RemoteViews(context.getPackageName(), R.layout.notification_expanded);
        if (theAlbumBitmap != null)
        {
            customBigView.setImageViewBitmap(R.id.album_art, theAlbumBitmap);
        }
        // showing play or pause, Note that !isPlay means status pause, so show play icon and vice versa
        int playPauseId = (isPlay) ? R.drawable.notification_pause : R.drawable.notification_play;
        customBigView.setImageViewResource(R.id.button_play_pause, playPauseId);
        customBigView.setTextViewText(R.id.composer_name, audioInformationFirstLine);
        customBigView.setTextViewText(R.id.work_name, audioInformationSecondLine);
        customBigView.setTextViewText(R.id.track_name, audioInformationThirdLine);

        // HACK. No idea why text colour is white by default
        if (Build.VERSION.SDK_INT > 20)
        {
            customBigView.setTextColor(R.id.composer_name, Color.BLACK);
            customBigView.setTextColor(R.id.work_name, Color.BLACK);
            customBigView.setTextColor(R.id.track_name, Color.BLACK);
        }

        customBigView.setOnClickPendingIntent(R.id.button_play_pause, pplayIntent);
        customBigView.setOnClickPendingIntent(R.id.button_next, pnextIntent);
        customBigView.setOnClickPendingIntent(R.id.button_prev, ppreviousIntent);
        customBigView.setOnClickPendingIntent(R.id.button_close, pcloseIntent);

        return customBigView;
    }


    /**************************************************************************
     *
     * create a compact or verbose notification
     *
     * As side effect sets mCustomBigViewForAndroid6 if appropriate
     *
     * read about colours of material design icons:
     * http://stackoverflow.com/questions/30544657/material-design-icon-colors
     *
     *************************************************************************/
    private NotificationCompat.Builder createCustomNotificationBuilder
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        boolean bUseCompactNotification,
        boolean bNoBigView,
        Context context
    )
    {
        // intents sent to service, not to activity

        PendingIntent ppreviousIntent = createMediaPlayIntent(NOTIFICATION_PREV_ACTION, currTrackArrayNumber, context);
        PendingIntent pplayIntent     = createMediaPlayIntent(NOTIFICATION_PLAY_ACTION, currTrackArrayNumber, context);
        PendingIntent pnextIntent     = createMediaPlayIntent(NOTIFICATION_NEXT_ACTION, currTrackArrayNumber, context);
        PendingIntent pcloseIntent    = createMediaPlayIntent(NOTIFICATION_STOP_ACTION, currTrackArrayNumber, context);

        Bitmap theAlbumBitmap;
        if ((AppGlobals.sCurrAlbumImageDrawable != null) && !(bUseCompactNotification && bNoBigView))
        {
            theAlbumBitmap = ((BitmapDrawable) AppGlobals.sCurrAlbumImageDrawable).getBitmap();
        }
        else
        {
            theAlbumBitmap = null;
        }

        // Using RemoteViews to bind custom layouts into Notification

        //
        // create small view
        //

        RemoteViews customSmallView = createCustomSmallView(
                    isPlay,
                    bUseCompactNotification,
                    audioInformationFirstLine,
                    audioInformationSecondLine,
                    audioInformationThirdLine,
                    context,
                    theAlbumBitmap,
                    ppreviousIntent,
                    pplayIntent,
                    pnextIntent,
                    pcloseIntent);

        //
        // create big view
        //

        RemoteViews customBigView;
        if (bNoBigView)
        {
            customBigView = null;
        }
        else
        {
            customBigView = createCustomBigView(
                                    isPlay,
                                    audioInformationFirstLine,
                                    audioInformationSecondLine,
                                    audioInformationThirdLine,
                                    context,
                                    theAlbumBitmap,
                                    ppreviousIntent,
                                    pplayIntent,
                                    pnextIntent,
                                    pcloseIntent);
        }

        // Note that big view is ignored here on Android 6 and older, but will be added
        // later, see below.
        NotificationCompat.Builder builder = createNotificationBuilderWithCustomViews(
                                                context, customSmallView, customBigView);

        builder.setContentIntent(mPendingNotificationIntent);
        // note that beginning with Android 5 this icon should be nothing but white:
        builder.setSmallIcon((isPlay) ? R.drawable.play : R.drawable.pause);

        if (Build.VERSION.SDK_INT > 20)
        {
            // show notification on lock screen
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
        {
            // For Android N this had been done in builder.
            // Deprecated, but needed for Android 6 and older
            mCustomBigViewForAndroid6 = customBigView;     // may be null
        }

        return builder;
    }


    /**************************************************************************
     *
     * Create generic or custom Notification Builder that can be used
     * to create or update notifications.
     *
     * notification style:
     *
     *  0 = compact
     *  1 = system
     *  2 = verbose
     *  3 = compact small (developer)
     *  4 = verbose small (developer)
     *  5 = system (media)
     *
     *************************************************************************/
    private NotificationCompat.Builder createMyNotificationBuilder
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        MediaSessionCompat mediaSession,
        Context context
    )
    {
        NotificationCompat.Builder theNotificationBuilder;      // to create and update notifications
        int notificationStyle = UserSettings.getVal(UserSettings.PREF_NOTIFICATION_STYLE, 0);

        if ((notificationStyle == 1) || (notificationStyle == 5))
        {
            // generic notification
            theNotificationBuilder = createGenericNotificationBuilder(
                    isPlay,
                    (notificationStyle == 5),       // Media Style (extended generic)
                    audioInformationFirstLine,
                    audioInformationSecondLine,
                    audioInformationThirdLine,
                    currTrackArrayNumber,
                    mediaSession,
                    context);
        }
        else
        {
            boolean bNoBigView = false;     // for debugging
            boolean bCompact = (notificationStyle == 0);

            if (notificationStyle == 3)
            {
                bCompact = true;
                bNoBigView = true;
            }
            else
            if (notificationStyle == 4)
            {
                //noinspection ConstantConditions
                bCompact = false;
                bNoBigView = true;
            }

            theNotificationBuilder = createCustomNotificationBuilder(
                    isPlay,
                    audioInformationFirstLine,
                    audioInformationSecondLine,
                    audioInformationThirdLine,
                    currTrackArrayNumber,
                    bCompact, bNoBigView, context);
        }

        return theNotificationBuilder;
    }


    /**************************************************************************
     *
     * set notification or update current one, if any
     *
     *************************************************************************/
    public void removeNotification()
    {
        if (mNotificationManager != null)
        {
            mNotificationManager.cancel(MY_NOTIFICATION_ID);
        }
        else
        {
            Log.e(LOG_TAG, "no Notification Manager");
        }
    }


    /**************************************************************************
     *
     * set notification or update current one, if any
     *
     *************************************************************************/
    public void createAndShowNotification
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        MediaSessionCompat mediaSession,
        Context context
    )
    {
        // Theoretically the Notification Builder could be created once and
        // updated accordingly when audio information or play state changes.
        // But this would be highly complicated, thus we rebuild the builder
        // whenever the notification is requested.

        NotificationCompat.Builder theNotificationBuilder = createMyNotificationBuilder(
                            isPlay,
                            audioInformationFirstLine,
                            audioInformationSecondLine,
                            audioInformationThirdLine,
                            currTrackArrayNumber,
                            mediaSession,
                            context);

        android.app.Notification theNotification = theNotificationBuilder.build();

        // register the notification with an id so that we can later update or remove it
        //mNotificationManager.notify(MY_NOTIFICATION_ID, theNotification);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            theNotification.category = CATEGORY_TRANSPORT;
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
        {
            // For Android N this had been done in builder.
            // Deprecated, but needed for Android 6 and older
            theNotification.bigContentView = mCustomBigViewForAndroid6;     // may be null
        }

        theNotification.flags = android.app.Notification.FLAG_ONGOING_EVENT;    // ?

        // register the notification with an id so that we can later update or remove it

        mNotificationManager.notify(MY_NOTIFICATION_ID, theNotification);
    }
}
