/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * SQLite database access to shared database created by "Classical Music Scanner" application
 * @noinspection JavadocBlankLines
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
public class DatabaseManagerAndroid
{
    private static final boolean bLogOn = false;
    private static final String LOG_TAG = "UMPL : DbManagerAndroid";
    private static ContentResolver mResolver;
    @SuppressWarnings("deprecation")
    private static final String[] album_columns29 =
    {
        // pre-Android 11: get album art path
        android.provider.MediaStore.Audio.Albums._ID,
        android.provider.MediaStore.Audio.Albums.ALBUM,
        android.provider.MediaStore.Audio.Albums.ARTIST,
        android.provider.MediaStore.Audio.Albums.NUMBER_OF_SONGS,
        android.provider.MediaStore.Audio.Albums.FIRST_YEAR,
        android.provider.MediaStore.Audio.Albums.LAST_YEAR,
        android.provider.MediaStore.Audio.Albums.ALBUM_ART      // abandoned with Android 11
    };
    private static final String[] album_columns30 =
    {
        // Android 11: derive album art later, from _ID
        android.provider.MediaStore.Audio.Albums._ID,
        android.provider.MediaStore.Audio.Albums.ALBUM,
        android.provider.MediaStore.Audio.Albums.ARTIST,
        android.provider.MediaStore.Audio.Albums.NUMBER_OF_SONGS,
        android.provider.MediaStore.Audio.Albums.FIRST_YEAR,
        android.provider.MediaStore.Audio.Albums.LAST_YEAR
    };
    private static final String[] album_columns = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ? album_columns30 : album_columns29;
    private static final String album_orderBy = MediaStore.Audio.Albums.ALBUM + " COLLATE NOCASE ASC";
    private static final Uri album_content_uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

    private static final String[] media_columns29 =
    {
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.ALBUM_ID,
        MediaStore.Audio.Media.TRACK,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.COMPOSER,
        MediaStore.Audio.Media.ARTIST,
        /* MediaStore.Audio.Media.ALBUM_ARTIST, is hidden via @hide */
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.YEAR,
        // from base class MediaColumns:
        MediaStore.Audio.Media.DATA,  // here: the path, no longer recommended for Android 11
        MediaStore.Audio.Media.SIZE
    };
    private static final String[] media_columns30 =
    {
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.ALBUM_ID,
        MediaStore.Audio.Media.TRACK,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.COMPOSER,
        MediaStore.Audio.Media.ARTIST,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.YEAR,
        // from base class MediaColumns:
        MediaStore.Audio.Media.DISPLAY_NAME,  // needed for file name extension
        MediaStore.Audio.Media.SIZE
    };
    private static final String[] media_columns = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ? media_columns30 : media_columns29;
    private static final String media_orderBy = MediaStore.Audio.Media.TRACK;
    private static final Uri media_content_uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

    private final boolean mbUseJaudioTaglib;



    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    public DatabaseManagerAndroid(boolean bUseJaudioTaglib)
    {
        mbUseJaudioTaglib = bUseJaudioTaglib;
    }


    /**************************************************************************
     *
     * open
     *
     *************************************************************************/
    public void open(ContentResolver musicResolver)
    {
        if (bLogOn) Log.v(LOG_TAG, "open Android database");
        mResolver = musicResolver;
    }


    /**************************************************************************
     *
     * get list of all albums
     *
     *************************************************************************/
    @SuppressWarnings({"WeakerAccess", "deprecation"})
    public ArrayList<AudioAlbum> queryAlbums()
    {
        // this is kind of SQL query:
        Cursor theCursor;
        try
        {
            theCursor = mResolver.query(
                    album_content_uri,
                    album_columns,    // columns to return, null returns all rows
                    null,       // rows to return, as "WHERE ...", null returns all rows
                    null,       // selection arguments, replacing question marks in previous argument
                    album_orderBy     // sort order, null: unordered
            );
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "queryAlbums() : " + e);
            theCursor = null;
        }

        // handle error cases
        if (theCursor == null)
        {
            Log.w(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.w(LOG_TAG, "no album found");
            theCursor.close();
            return null;
        }

        // from class AlbumColumns:
        // this is an Android bug and leads to an exception:
        // -> http://stackoverflow.com/questions/34151038/why-is-androids-mediastore-audio-album-album-id-causing-an-illegalstateexceptio
        //int idColumn         = theCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ID);
        // In fact the "album artist" is nonsense due to an Android bug
        // -> https://stackoverflow.com/questions/17392035/android-getting-album-artist-from-cursor
        int idColumn         = theCursor.getColumnIndex(MediaStore.Audio.Albums._ID);
        int albumColumn      = theCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
        int artistColumn     = theCursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);        // Android-Bug!
        int noOfTracksColumn = theCursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);
        int firstYearColumn  = theCursor.getColumnIndex(MediaStore.Audio.Albums.FIRST_YEAR);
        int lastYearColumn   = theCursor.getColumnIndex(MediaStore.Audio.Albums.LAST_YEAR);
        int albumArtColumn   = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ? -1 : theCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);

        //
        // loop to add all album data to the global album list
        //
        ArrayList<AudioAlbum> audioAlbumList = new ArrayList</*AudioAlbum*/>();
        do
        {
            long thisId = theCursor.getLong(idColumn);
            String thisAlbumName = theCursor.getString(albumColumn);
            String theAlbumArtist = theCursor.getString(artistColumn);        // Android-Bug!
            long thisNoOfTracks = theCursor.getInt(noOfTracksColumn);
            long thisFirstYear  = theCursor.getInt(firstYearColumn);
            long thisLastYear   = theCursor.getInt(lastYearColumn);
            String thisAlbumArt;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            {
                // Android 30 method to the get the album art Uri.
                // These look like ""content://media/external/audio/albums/8204931316969154785".
                // https://stackoverflow.com/questions/66077161/android-loadthumbnail-album-artwork-api-29
                Uri contentUri = ContentUris.withAppendedId(
                        album_content_uri,
                        thisId);
                thisAlbumArt = contentUri.toString();
                // Bitmap = resolver.loadThumbnail(contentUri, new Size(640, 480), null);
            }
            else
            {
                thisAlbumArt = theCursor.getString(albumArtColumn);
            }

            //noinspection ConstantConditions
            /*
            if (BuildConfig.BUILD_TYPE.startsWith("debug"))
            {
                Log.v(LOG_TAG, "Album Id (_ID)     = " + thisId);
                Log.v(LOG_TAG, "Album Name         = " + thisAlbumName);
                Log.v(LOG_TAG, "Album Interpreter  = " + theAlbumArtist);        // Android-Bug!
                Log.v(LOG_TAG, "Album No of Tracks = " + thisNoOfTracks);
                Log.v(LOG_TAG, "Album First Year   = " + thisFirstYear);
                Log.v(LOG_TAG, "Album Last Year    = " + thisLastYear);
                Log.v(LOG_TAG, "Album Art          = " + thisAlbumArt);
                Log.v(LOG_TAG, "==============================================\n");
            }
            */

            if ((thisAlbumName != null) && !isAlbumIllegal(thisAlbumName))
            {
                AudioAlbum theAlbum = new AudioAlbum(
                        thisId,
                        thisAlbumName,
                        theAlbumArtist,        // Android-Bug!
                        thisNoOfTracks,
                        thisFirstYear,
                        thisLastYear,
                        thisAlbumArt);
                audioAlbumList.add(theAlbum);
            }
        }
        while (theCursor.moveToNext());

        theCursor.close();
        Log.d(LOG_TAG, "found " + audioAlbumList.size() + " albums");
        return audioAlbumList;
    }


    /**************************************************************************
     *
     * get list of all tracks for current album
     *
     *************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public ArrayList<AudioTrack> queryTracksOfAlbum(AudioAlbum theAlbum)
    {
        long album_id = theAlbum.album_id;

//        final String where = MediaStore.Audio.Media.ALBUM + "=?";
        final String where = MediaStore.Audio.Media.ALBUM_ID + "=?";
//        final String[] whereVal = { currAlbum.album_name };
        final String[] whereVal = { "" + album_id };

        // this is kind of SQL query:
        Cursor theCursor = mResolver.query(
                media_content_uri,
                (mbUseJaudioTaglib) ? media_columns29 : media_columns,    // note that JaudioTag needs paths
                where,      // rows to return, as "WHERE ...", null returns all rows
                whereVal,   // selection arguments, replacing question marks in previous argument
                media_orderBy     // sort order, null: unordered
        );
        // handle error cases
        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            // note that we will have a track list, but an empty one
            Log.v(LOG_TAG, "no track for album found");
            theCursor.close();
            return null;
        }

        // from class AudioColumns:
        int idColumn          = theCursor.getColumnIndex(MediaStore.Audio.Media._ID);
        int trackColumn       = theCursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
        int titleColumn       = theCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
        int composerColumn    = theCursor.getColumnIndex(MediaStore.Audio.Media.COMPOSER);
        int artistColumn      = theCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int durationColumn    = theCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
        //int yearColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.YEAR);    // unreliable
        // from base class MediaColumns:
        int dataColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.DATA);  // absolute file path
        int nameColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME);  // file name
        int sizeColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.SIZE);
        //int albumColumn       = theCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
        int albumIdColumn       = theCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
        //int albumArtistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ARTIST);  // unreliable
        //int albumKeyColumn    = theCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_KEY);

        //
        // loop to add all music files to the album music list
        //
        ArrayList<AudioTrack> currAlbumTrackList = new ArrayList</*AudioTrack*/>();
        boolean bMustGetAlbumComposer = (theAlbum.getComposer() == null);
        do
        {
            long thisId          = theCursor.getLong(idColumn);
            long albumId         = theCursor.getLong(albumIdColumn);
            String thisTrack     = theCursor.getString(trackColumn);
            String thisTitle     = theCursor.getString(titleColumn);
            String thisComposer  = theCursor.getString(composerColumn);
            String thisPerformer = theCursor.getString(artistColumn);     // performer/performer
            long thisDuration    = theCursor.getInt(durationColumn);
            //long thisYear = theCursor.getInt(yearColumn);
            String thisPath      = (dataColumn >= 0) ? theCursor.getString(dataColumn) : null;
            String thisName      = (nameColumn >= 0) ? theCursor.getString(nameColumn) : thisPath;
            long thisSize        = theCursor.getInt(sizeColumn);
            String thisUri = null;

            if (thisPath == null)
            {
                // Android 30 method to the get the audio file Uri.
                // These look like "content://media/external/audio/media/2718".

                Uri contentUri = ContentUris.withAppendedId(
                        media_content_uri,
                        thisId);
                thisUri = contentUri.toString();
                // This is the recommended method to open the media file:
                // ContentResolver#openFileDescriptor(Uri, String)
            }

            //noinspection ConstantConditions
            if (BuildConfig.BUILD_TYPE.startsWith("debug") && (bLogOn))
            {
                Log.v(LOG_TAG, "thisId        = " + thisId);
                Log.v(LOG_TAG, "albumId       = " + albumId);
                Log.v(LOG_TAG, "thisTrack     = " + thisTrack);
                Log.v(LOG_TAG, "thisTitle     = " + thisTitle);
                Log.v(LOG_TAG, "thisComposer  = " + thisComposer);
                Log.v(LOG_TAG, "thisPerformer = " + thisPerformer);
                Log.v(LOG_TAG, "thisDuration  = " + thisDuration);
                Log.v(LOG_TAG, "thisPath      = " + thisPath);
                Log.v(LOG_TAG, "thisName      = " + thisName);
                Log.v(LOG_TAG, "thisUri       = " + thisUri);
                //Log.v(LOG_TAG, "thisYear        = " + thisYear);
                Log.v(LOG_TAG, "thisSize      = " + thisSize);
                Log.v(LOG_TAG, "==============================================\n");
            }

            // Android 30 and newer: use Uri (though path also would be possible)
            // Android 29 and older: use path
            if (thisPath == null)
            {
                thisPath = thisUri;
            }

            if ((thisTitle == null) || (thisPath == null))
            {
                Log.w(LOG_TAG, "InitAudioTracklist(): corrupt entry skipped");
                continue;
            }
            if ((thisPerformer == null) || (thisPerformer.equals("<unknown>")))
            {
                thisPerformer = "";
            }
            if (thisTrack == null)
            {
                thisTrack = "0";
            }

            AudioTrack theTrack = new AudioTrack(
                    thisId,
                    theAlbum,
                    thisTrack,
                    thisTitle,
                    thisPath,
                    thisDuration,
                    thisPerformer);

            AudioTagReaderInterface theTagsFromFile = (mbUseJaudioTaglib) ? new JaudioTagReader() : new MyTagReader();
            theTagsFromFile.get(thisPath, thisName, thisSize, mResolver);
            theTrack.addTagInfo(
                    theTagsFromFile.tagMovementName,
                    theTagsFromFile.tagGrouping,
                    theTagsFromFile.tagSubtitle,
                    theTagsFromFile.tagComposer,
                    theTagsFromFile.tagConductor,
                    theTagsFromFile.tagYear);
            currAlbumTrackList.add(theTrack);

            if (bMustGetAlbumComposer)
            {
                theAlbum.setAlbumComposer(theTagsFromFile.tagComposer);
                theAlbum.setAlbumPerformer(thisPerformer);
                theAlbum.setAlbumArtist(theTagsFromFile.tagAlbumArtist);
                theAlbum.setAlbumYear(theTagsFromFile.tagYear);
            }
        }
        while (theCursor.moveToNext());

        theCursor.close();
        Log.d(LOG_TAG, "found " + currAlbumTrackList.size() + " tracks");
        return currAlbumTrackList;
    }


    public void close()
    {
        mResolver = null;
    }


    /*
     * helper function to ignore internal "Albums"
     */
    static private boolean isAlbumIllegal(final String thisAlbumName)
    {
        return ((thisAlbumName == null) ||
                (thisAlbumName.isEmpty()) ||
                (thisAlbumName.equals("Notifications")) ||
                (thisAlbumName.equals("Ringtones")));
    }
}
