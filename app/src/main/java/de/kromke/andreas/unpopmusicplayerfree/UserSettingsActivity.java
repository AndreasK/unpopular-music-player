/*
 * Copyright (C) 2016-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.unpopmusicplayerfree;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Keep;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

import android.util.Log;

import static de.kromke.andreas.unpopmusicplayerfree.UserSettings.PREF_THEME;
import static de.kromke.andreas.unpopmusicplayerfree.UserSettings.getVal;

/**
 * combines the preferences pages
 * https://developer.android.com/guide/topics/ui/settings#java
 * https://developer.android.com/guide/topics/ui/settings/organize-your-settings
 * https://developer.android.com/reference/androidx/preference/PreferenceFragmentCompat
 * https://stackoverflow.com/questions/57466675/how-to-use-preferencescreen-of-androidx
 * https://developer.android.com/reference/androidx/preference/package-summary
 * @noinspection JavadocLinkAsPlainText, JavadocBlankLines
 */
public class UserSettingsActivity extends AppCompatActivity /*implements
        PreferenceFragmentCompat.OnPreferenceStartFragmentCallback*/
{
    private static final String LOG_TAG = "UMPL : UserSettingsAct";
    private int mColourTheme = -1;

    static public class MySettingsFragment extends PreferenceFragmentCompat
    {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }
    }

    //
    // Behaviour settings fragment (@keep: otherwise removed by optimiser)
    //

    @Keep
    static public class MyPreferenceFragmentBehaviour extends PreferenceFragmentCompat
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            Log.d(LOG_TAG, "MyPreferenceFragmentBehaviour::onCreate()");
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_behaviour, rootKey);
        }
    }

    //
    // Scaling and themes settings fragment (@keep: otherwise removed by optimiser)
    // Note that we monitor changes directly here to instantly switch the theme
    //

    @Keep
    static public class MyPreferenceFragmentScaleAndThemes extends PreferenceFragmentCompat
            implements SharedPreferences.OnSharedPreferenceChangeListener
    {
        int mFragmentColourTheme = -1;

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            mFragmentColourTheme = getVal(PREF_THEME, 0);
            super.onCreate(savedInstanceState);
            //addPreferencesFromResource(R.xml.settings_scale_and_themes);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_scale_and_themes, rootKey);
        }

        @Override
        public void onResume() {
            super.onResume();
            // Set up a listener whenever a key changes
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
            if (sharedPreferences != null)
            {
                sharedPreferences.registerOnSharedPreferenceChangeListener(this);
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            // free listener
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
            if (sharedPreferences != null)
            {
                sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
            }
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
        {
            UserSettingsActivity theActivity = (UserSettingsActivity) getActivity();
            if (theActivity != null)
            {
                mFragmentColourTheme = theActivity.applyTheme(mFragmentColourTheme);
            }
        }
    }

    //
    // Metadata settings fragment (@keep: otherwise removed by optimiser)
    //

    @Keep
    static public class MyPreferenceFragmentMetadata extends PreferenceFragmentCompat
    {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            Log.d(LOG_TAG, "MyPreferenceFragmentMetadata::onCreatePreferences()");
            setPreferencesFromResource(R.xml.settings_metadata, rootKey);
            Log.d(LOG_TAG, "MyPreferenceFragmentMetadata::onCreatePreferences() : done");
        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            Log.d(LOG_TAG, "MyPreferenceFragmentMetadata::onCreate()");
            super.onCreate(savedInstanceState);
            //addPreferencesFromResource(R.xml.settings_metadata);
        }
    }

    //
    // View settings fragment (@keep: otherwise removed by optimiser)
    //

    @Keep
    static public class MyPreferenceFragmentView extends PreferenceFragmentCompat
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            //addPreferencesFromResource(R.xml.settings_view);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_view, rootKey);
        }
    }

    /*
    @Override
    protected void onPause()
    {
        Log.d("UserSettingsActivity", "onPause()");
        super.onPause();
    }
    */

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        UserSettings.init(this);
        mColourTheme = applyTheme(-1);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MySettingsFragment())
                .commit();
    }

    // sophisticated method to apply theme changed by fragment also to main preferences
    @Override
    protected void onResume()
    {
        Log.d(LOG_TAG, "onResume()");
        int colourTheme = getVal(PREF_THEME, 0);
        if (colourTheme != mColourTheme)
        {
            mColourTheme = applyTheme(mColourTheme);
        }
        super.onResume();
    }

    /*
    @Override
    public boolean onPreferenceStartFragment(PreferenceFragmentCompat caller, Preference pref)
    {
        Log.d(LOG_TAG, "onPreferenceStartFragment()");
        // Instantiate the new Fragment
        final Bundle args = pref.getExtras();
        final Fragment fragment = getSupportFragmentManager().getFragmentFactory().instantiate(
                getClassLoader(),
                pref.getFragment());
        fragment.setArguments(args);
        fragment.setTargetFragment(caller, 0);
        // Replace the existing Fragment with the new Fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit();
        Log.d(LOG_TAG, "onPreferenceStartFragment() : done");
        return true;
    }
    */

    /* THIS IS NEEDED IN CASE THERE ARE TWO LEVELS OF SETTINGS, A HEADER AND SOME FRAGMENTS */
    /*
    @Override
    public void onBuildHeaders(List<Header> target)
    {
        loadHeadersFromResource(R.xml.headers_settings, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName)
    {
        return MyPreferenceFragmentBehaviour.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentView.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentMetadata.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentScaleAndThemes.class.getName().equals(fragmentName);
    }
    */


    /**************************************************************************
     *
     * update colour theme etc., if necessary, and return new value
     *
     *************************************************************************/
    private int applyTheme(int prevTheme)
    {
        int colourTheme = getVal(PREF_THEME, 0);
        if ((prevTheme < 0) || (prevTheme != colourTheme))
        {
            mColourTheme = colourTheme;
            switch (mColourTheme)
            {
                default:
                case 3:
                case 0:
                    setTheme(R.style.UserSettingsTheme_Blue);
                    break;
                case 1:
                    setTheme(R.style.UserSettingsTheme_OrangeLight);
                    break;
                case 2:
                    setTheme(R.style.UserSettingsTheme_GreenLight);
                    break;
                case 4:
                    setTheme(R.style.UserSettingsTheme_Gray);
                    break;
            }

            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                getWindow().setStatusBarColor(UserSettings.getThemedColourFromResId(this, R.attr.colorPrimaryDark));
            }
        }

        return colourTheme;
    }
}
