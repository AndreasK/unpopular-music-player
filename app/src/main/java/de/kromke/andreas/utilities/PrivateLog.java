package de.kromke.andreas.utilities;

import static android.os.Environment.getExternalStorageState;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.kromke.andreas.unpopmusicplayerfree.BuildConfig;

public class PrivateLog
{
    private static final String LOG_TAG = "UMPL : Log";
    private static boolean sbAccessError = false;

    public static void d(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('D', tag, msg);
        }
        Log.d(tag, msg);
    }

    public static void w(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('W', tag, msg);
        }
        Log.w(tag, msg);
    }

    public static void e(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('E', tag, msg);
        }
        Log.e(tag, msg);
    }

    private static void appendLogWithDate(char channel, final String tag, final String msg)
    {
        if (BuildConfig.DEBUG)
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
            String date = df.format(Calendar.getInstance().getTime());
            appendLog(date + " " + tag + channel + " " + msg);
        }
    }

    private static void appendLog(String text)
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStorageDirectory();
        } else
        {
            Log.e(LOG_TAG, "appendLog() -- media not mounted, no private log path");
            return;
        }

        File logFile = new File(basePath + "/umpl.log");
        if (!logFile.exists())
        {
            try
            {
                if (!logFile.createNewFile())
                {
                    Log.e(LOG_TAG, "appendLog() -- cannot create private log file");
                    sbAccessError = true;
                    return;
                }
            }
            catch (IOException e)
            {
                Log.e(LOG_TAG, "appendLog() -- cannot create private log file");
                sbAccessError = true;
                return;
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            Log.e(LOG_TAG, "appendLog() -- cannot write to private log file");
            sbAccessError = true;
        }
    }
}
