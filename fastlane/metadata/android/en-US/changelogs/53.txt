- F-Droid version.
- Bug removed: missing album images were shown as empty, not with default image (only Android 11).
- Handle Uri instead of path, whenever possible, and skip conversion to path, if not necessary.
- As JaudioTagger does not accepts Uris, only paths, it might be removed from the program in near future.
- (v2.72)

