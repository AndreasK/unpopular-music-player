- Play Store version (limited functionality)
- Various problems with Android 11 avoided.
- Error message for database open failure.
- Tile reflects "SAF" mode or "DB" mode, if active.
- API 31.
- (v2.71)

